<?php
if (self::WPORG || ds_icon_expansion_has_license_key()) {
	echo('<div id="agsdi-admin-page" class="wrap"><h1>'.esc_html__(self::PLUGIN_NAME, 'ds-icon-epxansion').'</h1>
			<div id="agsdi-instructions">
				<h2>'.esc_html__('Instructions', 'ds-icon-expansion').'</h2>'
				.'<p>'.sprintf(
							esc_html__('Easily insert one of the 2600+ icons provided by %s when using the WordPress visual editor to create and edit posts, pages, and other content! Simply click on the %s icon in the editor\'s toolbar to open the icon insertion window.', 'ds-icon-expansion'),
							self::PLUGIN_NAME,
							'<span data-icon="agsdix-sao-design" class="agsdi-icon"></span>'
						).'</p>'
				.'<p>'.sprintf(
							esc_html__('If you use the %sDivi or Extra theme%s or the %sDivi Builder%s, you can also use the 2600+ icons provided by %s anywhere that the Divi Builder allows you to specify an icon, such as in Buttons, Blurbs, and much more! Works in both the Divi Builder and the Visual Builder.', 'ds-icon-expansion'),
							'<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=32248_0_1_10" target="_blank">',
							'</a>',
							'<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=32248_0_1_10" target="_blank">',
							'</a>',
							self::PLUGIN_NAME
						).'</p>
			</div>');
?>
<h2><?php echo(esc_html__('Multi-Color Icons', 'ds-icon-expansion')); ?></h2>
<?php
if (!empty($_POST)) {
	$colorSchemes = array();
	if (isset($_POST['agsdi_colors']) && is_array($_POST['agsdi_colors'])) {
		foreach ($_POST['agsdi_colors'] as $colorSchemeId => $colorScheme) {
			if ($colorSchemeId == 'new') {
				if (count($_POST['agsdi_colors']) == 1) {
					$colorSchemeId = 1;
				} else {
					$allColorSchemeIds = array_keys($_POST['agsdi_colors']);
					rsort($allColorSchemeIds);
					foreach ($allColorSchemeIds as $id) {
						if (is_numeric($id) && $id % 1 == 0 && $id > 0) {
							$colorSchemeId = ++$id;
							break;
						}
					}
					
					if ($colorSchemeId == 'new') {
						$colorSchemeValidationError = true;
						break;
					}
				}
			}
			
			$colorScheme = array_map('sanitize_hex_color', $colorScheme);
			
			if (count($colorScheme) != 3 || empty($colorScheme[0]) || empty($colorScheme[1]) || empty($colorScheme[2])) {
				$colorSchemeValidationError = true;
				break;
			} else {
				$colorSchemes[$colorSchemeId] = array(
					substr($colorScheme[0], 1),
					substr($colorScheme[1], 1),
					substr($colorScheme[2], 1)
				);
			}
		}
	}
	
	if (empty($colorSchemeValidationError)) {
		if (empty($colorSchemes)) {
			delete_option('aspengrove_icons_colors');
		} else {
			update_option('aspengrove_icons_colors', $colorSchemes);
		}
		
		foreach ($colorSchemes as $colorSchemeId => $colorScheme) {
			AGS_Divi_Icons_Pro::colorizeSvg($colorScheme, $colorSchemeId);
		}
		
		$colorSchemeIds = array_keys($colorSchemes);
		AGS_Divi_Icons_Pro::generateMultiColorCss($colorSchemeIds);
		
		AGS_Divi_Icons_Pro::cleanupMultiColorSvgs($colorSchemeIds);
	}
} else {
	$colorSchemes = get_option('aspengrove_icons_colors', array());
}

?>
<script>
var agsdi_color_preview_icons = <?php echo(json_encode(AGS_Divi_Icons::getMultiColorIcons())); ?>;
</script>
<form id="agsdi-color-schemes" method="post">
	<?php if (!empty($colorSchemeValidationError)) { ?>
	<p id="agsdi-color-schemes-error"><?php echo(esc_html__('The color schemes were not saved due to validation error(s).', 'ds-icon-expansion')); ?></p>
	<?php } ?>
	
	<p id="agsdi-color-schemes-none"<?php if (!empty($colorSchemes)) { ?> class="hidden"<?php } ?>><?php echo(esc_html__('No color schemes are currently defined.', 'ds-icon-expansion')); ?></p>
	<?php
		foreach ($colorSchemes as $colorSchemeId => $colorScheme) {
	?>
	<div data-colors-id="<?php echo(esc_attr($colorSchemeId)); ?>">
		<div class="agsdi-color-preview"></div>
		<input type="text" name="agsdi_colors[<?php echo(esc_attr($colorSchemeId)); ?>][0]" value="#<?php echo(esc_attr($colorScheme[0])); ?>">
		<input type="text" name="agsdi_colors[<?php echo(esc_attr($colorSchemeId)); ?>][1]" value="#<?php echo(esc_attr($colorScheme[1])); ?>">
		<input type="text" name="agsdi_colors[<?php echo(esc_attr($colorSchemeId)); ?>][2]" value="#<?php echo(esc_attr($colorScheme[2])); ?>">
		<button class="agsdi-colors-remove button-secondary"><?php echo(esc_html__('Remove', 'ds-icon-expansion')); ?></button>
	</div>
	<?php
		}
	?>
	<div id="agsdi-color-schemes-buttons">
		<?php wp_nonce_field('agsdi-color-schemes-save'); ?>
		<button id="agsdi-colors-add" type="button" class="button-secondary"><?php echo(esc_html__('Add Color Scheme', 'ds-icon-expansion')); ?></button>
		<button class="button-primary"><?php echo(esc_html__('Save', 'ds-icon-expansion')); ?></button>
	</div>
</form>
<?php
	if (!self::WPORG) {
		echo('<h2>'.esc_html__('About & License Key', 'ds-icon-expansion').'</h2>');
		ds_icon_expansion_license_key_box();
	}
	
	echo('<h2>'.esc_html__('Check out these products too!', 'ds-icon-expansion').'</h2>
		<ul>
	');
	foreach (self::getCreditPromos('admin-page', true) as $promo) {
		echo('<li>'.$promo.'</li>');
	}
	echo('</ul>');
	
	echo('<p><em>Divi is a registered trademark of Elegant Themes, Inc. This product is not affiliated with nor endorsed by Elegant Themes. Links to the Elegant Themes website on this page are affiliate links.</em></p>');
	
	@include(self::$pluginDir.'build/test.php');
	echo('</div>');
} else {
	ds_icon_expansion_activate_page();
}