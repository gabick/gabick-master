<?php
/* 
This file contains code from the Easy Digital Downloads Software Licensing addon.
Copyright Easy Digital Downloads; licensed under GPLv2 or later (see license.txt file included with plugin).
Modified by Aspen Grove Studios to customize settings, add additional functionality, etc., 2017-2018.
*/

if (!defined('ABSPATH')) exit;

define( 'ds_icon_expansion_STORE_URL', AGS_Divi_Icons::PLUGIN_AUTHOR_URL );
define( 'ds_icon_expansion_ITEM_NAME', AGS_Divi_Icons::PLUGIN_NAME ); // Needs to exactly match the download name in EDD
define( 'ds_icon_expansion_PLUGIN_PAGE', 'admin.php?page=ds-icon-expansion' );

define('ds_icon_expansion_BRAND_NAME', AGS_Divi_Icons::PLUGIN_AUTHOR);

if( !class_exists( 'ds_icon_expansion_Plugin_Updater' ) ) {
	// load our custom updater
	include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
}

// Load translations
load_plugin_textdomain('aspengrove-updater', false, plugin_basename(dirname(__FILE__).'/lang'));

function ds_icon_expansion_updater() {

	// retrieve our license key from the DB
	$license_key = trim( get_option( 'ds_icon_expansion_license_key' ) );

	// setup the updater
	new ds_icon_expansion_Plugin_Updater( ds_icon_expansion_STORE_URL, AGS_Divi_Icons::$pluginFile, array(
			'version' 	=> AGS_Divi_Icons::PLUGIN_VERSION, // current version number
			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
			'item_name' => ds_icon_expansion_ITEM_NAME, 	// name of this plugin
			'author' 	=> ds_icon_expansion_BRAND_NAME,  // author of this plugin
			'beta'		=> false
		)
	);
	
	// creates our settings in the options table
	register_setting('ds_icon_expansion_license', 'ds_icon_expansion_license_key', 'ds_icon_expansion_sanitize_license' );
	
	if (isset($_POST['ds_icon_expansion_license_key_deactivate'])) {
		require_once(dirname(__FILE__).'/license-key-activation.php');
		$result = ds_icon_expansion_deactivate_license();
		if ($result !== true) {
			define('ds_icon_expansion_DEACTIVATE_ERROR', empty($result) ? __('An unknown error has occurred. Please try again.', 'aspengrove-updater') : $result);
		}
		unset($_POST['ds_icon_expansion_license_key_deactivate']);
	}
}
add_action( 'admin_init', 'ds_icon_expansion_updater', 0 );


function ds_icon_expansion_has_license_key() {
	return (get_option('ds_icon_expansion_license_status') === 'valid');
}

function ds_icon_expansion_activate_page() {
	$license = get_option( 'ds_icon_expansion_license_key' );
	$status  = get_option( 'ds_icon_expansion_license_status' );
	?>
		<div class="wrap" id="ds_icon_expansion_license_key_activation_page">
			<form method="post" action="options.php" id="ds_icon_expansion_license_key_form">
				<div id="ds_icon_expansion_license_key_form_logo_container">
					<a href="https://aspengrovestudios.com/?utm_source=<?php echo(AGS_Divi_Icons::PLUGIN_SLUG); ?>&amp;utm_medium=plugin-credit-link&amp;utm_content=license-key-activate" target="_blank">
						<img src="<?php echo(plugins_url('logo.png', __FILE__)); ?>" alt="<?php echo(ds_icon_expansion_BRAND_NAME); ?>" />
					</a>
				</div>
				
				<div id="ds_icon_expansion_license_key_form_body">
					<div id="ds_icon_expansion_license_key_form_title">
						<?php echo(esc_html(ds_icon_expansion_ITEM_NAME)); ?>
						<small>v<?php echo(AGS_Divi_Icons::PLUGIN_VERSION); ?></small>
					</div>
					
					<p>
						Thank you for purchasing <?php echo(htmlspecialchars(ds_icon_expansion_ITEM_NAME)); ?>!<br />
						Please enter your license key below.
					</p>
					
					<?php settings_fields('ds_icon_expansion_license'); ?>
					
					<label>
						<span><?php _e('License Key:', 'aspengrove-updater'); ?></span>
						<input name="ds_icon_expansion_license_key" type="text" class="regular-text"<?php if (!empty($_GET['license_key'])) { ?> value="<?php echo(esc_attr($_GET['license_key'])); ?>"<?php } else if (!empty($license)) { ?> value="<?php echo(esc_attr($license)); ?>"<?php } ?> />
					</label>
					
					<?php
						if (isset($_GET['sl_activation']) && $_GET['sl_activation'] == 'false') {
							echo('<p id="ds_icon_expansion_license_key_form_error">'.(empty($_GET['sl_message']) ? esc_html__('An unknown error has occurred. Please try again.', 'aspengrove-updater') : esc_html($_GET['sl_message'])).'</p>');
						} else if (defined('ds_icon_expansion_DEACTIVATE_ERROR')) {
							// ds_icon_expansion_DEACTIVATE_ERROR is already HTML escaped
							echo('<p id="ds_icon_expansion_license_key_form_error">'.ds_icon_expansion_DEACTIVATE_ERROR.'</p>');
						}
						
						submit_button('Continue');
					?>
				</div>
			</form>
		</div>
	<?php
}

function ds_icon_expansion_license_key_box() {
	$status  = get_option( 'ds_icon_expansion_license_status' );
	?>
		<div id="ds_icon_expansion_license_key_box">
			<form method="post" action="<?php echo(esc_url(ds_icon_expansion_PLUGIN_PAGE)); ?>" id="ds_icon_expansion_license_key_form">
				<div id="ds_icon_expansion_license_key_form_logo_container">
					<a href="https://aspengrovestudios.com/?utm_source=<?php echo(AGS_Divi_Icons::PLUGIN_SLUG); ?>&amp;utm_medium=plugin-credit-link&amp;utm_content=license-key-status" target="_blank">
						<img src="<?php echo(plugins_url('logo.png', __FILE__)); ?>" alt="<?php echo(ds_icon_expansion_BRAND_NAME); ?>" />
					</a>
				</div>
				
				<div id="ds_icon_expansion_license_key_form_body">
					<div id="ds_icon_expansion_license_key_form_title">
						<?php echo(esc_html(ds_icon_expansion_ITEM_NAME)); ?>
						<small>v<?php echo(AGS_Divi_Icons::PLUGIN_VERSION); ?></small>
					</div>
					
					<label>
						<span><?php esc_html_e('License Key:', 'aspengrove-updater'); ?></span>
						<input type="text" readonly="readonly" value="<?php echo(esc_html(get_option('ds_icon_expansion_license_key'))); ?>" />
					</label>
					
					<?php
						if (defined('ds_icon_expansion_DEACTIVATE_ERROR')) {
							echo('<p id="ds_icon_expansion_license_key_form_error">'.ds_icon_expansion_DEACTIVATE_ERROR.'</p>');
						}
						wp_nonce_field( 'ds_icon_expansion_license_key_deactivate', 'ds_icon_expansion_license_key_deactivate' );
						submit_button('Deactivate License Key', '');
					?>
				</div>
			</form>
		</div>
	<?php
}

function ds_icon_expansion_sanitize_license( $new ) {
	if (defined('ds_icon_expansion_LICENSE_KEY_VALIDATED')) {
		return $new;
	}
	$old = get_option( 'ds_icon_expansion_license_key' );
	if( $old && $old != $new ) {
		delete_option( 'ds_icon_expansion_license_status' ); // new license has been entered, so must reactivate
	}
	
	// Need to activate license here, only if submitted
	require_once(dirname(__FILE__).'/license-key-activation.php');
	ds_icon_expansion_activate_license($new); // Always redirects
}