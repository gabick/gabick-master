<?php
/*
Modifications by Aspen Grove Studios:
- 2019-01-01: change function prefix, rename above/below content widget areas, add "Replace Main Content" widget area
*/

// Create the new widget area
function ds_pbe_widget_area() {
    register_sidebar(array(
        'name' => 'PBE Above Header',
        'id' => 'pbe-above-header-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget' => '</div> <!-- end .et_pb_widget -->',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
    register_sidebar(array(
        'name' => 'PBE Below Header',
        'id' => 'pbe-below-header-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget' => '</div> <!-- end .et_pb_widget -->',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
    register_sidebar(array(
        'name' => 'PBE Footer',
        'id' => 'pbe-footer-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget' => '</div> <!-- end .et_pb_widget -->',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
    register_sidebar(array(
        'name' => 'PBE Above Main Content',
        'id' => 'pbe-above-content-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget' => '</div> <!-- end .et_pb_widget -->',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
    register_sidebar(array(
        'name' => 'PBE Below Main Content',
        'id' => 'pbe-below-content-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget' => '</div> <!-- end .et_pb_widget -->',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
    register_sidebar(array(
        'name' => 'PBE Replace Main Content',
        'id' => 'pbe-replace-content-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget' => '</div> <!-- end .et_pb_widget -->',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
}
add_action('widgets_init', 'ds_pbe_widget_area');

?>