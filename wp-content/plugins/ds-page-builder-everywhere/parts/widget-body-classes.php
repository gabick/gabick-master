<?php

// Add corresponding classes to the body tag based on active widget areas
function widget_body_classes($classes) {



    // add class to body if above header widget is active
    if ( is_active_sidebar( 'pbe-above-header-wa' ) ) {
        $classes[] = 'pbe-above-header';
    }

    // add class to body if below header widget is active
    if ( is_active_sidebar( 'pbe-below-header-wa' ) ) {
        $classes[] = 'pbe-below-header';
    }

    // add class to body if footer widget is active
    if ( is_active_sidebar( 'pbe-footer-wa' ) ) {
        $classes[] = 'pbe-footer';
    }

    // add class to body if above content widget is active
    if ( is_active_sidebar( 'pbe-above-content-wa' ) ) {
        $classes[] = 'pbe-above-content';
    }

    // add class to body if below content widget is active
    if ( is_active_sidebar( 'pbe-below-content-wa' ) ) {
        $classes[] = 'pbe-below-content';
    }

    return $classes;

}
add_filter( 'body_class', 'widget_body_classes' );

?>