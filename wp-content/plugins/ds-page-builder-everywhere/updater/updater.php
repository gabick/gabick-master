<?php
/* This file contains code from the Software Licensing addon by Easy Digital Downloads - GPLv2.0 or higher */
if (!defined('ABSPATH')) exit;

define( 'DS_PBE_FILE', realpath(dirname(__FILE__).'/../pbe.php') );
define( 'DS_PBE_STORE_URL', 'https://divi.space/' );
define( 'DS_PBE_ITEM_NAME', 'Page Builder Everywhere' ); // Needs to exactly match the download name in EDD
define( 'DS_PBE_PLUGIN_PAGE', 'admin.php?page=ds-page-builder-everywhere' );

if( !class_exists( 'DS_PBE_Plugin_Updater' ) ) {
	// load our custom updater
	include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
}

// Load translations
load_plugin_textdomain('aspengrove-updater', false, plugin_basename(dirname(__FILE__).'/lang'));

function DS_PBE_updater() {

	// retrieve our license key from the DB
	$license_key = trim( get_option( 'DS_PBE_license_key' ) );

	// setup the updater
	new DS_PBE_Plugin_Updater( DS_PBE_STORE_URL, DS_PBE_FILE, array(
			'version' 	=> DS_PBE_VERSION, // current version number
			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
			'item_name' => DS_PBE_ITEM_NAME, 	// name of this plugin
			'author' 	=> 'Divi Space',  // author of this plugin
			'beta'		=> false
		)
	);

}
add_action( 'admin_init', 'DS_PBE_updater', 0 );


function DS_PBE_has_license_key() {
	if (isset($_POST['DS_PBE_license_key_deactivate'])) {
		require_once(dirname(__FILE__).'/license-key-activation.php');
		DS_PBE_deactivate_license();
	}
	return (get_option('DS_PBE_license_status') === 'valid');
}

function DS_PBE_activate_page() {
	$license = get_option( 'DS_PBE_license_key' );
	$status  = get_option( 'DS_PBE_license_status' );
	?>
		<div class="wrap" id="DS_PBE_license_key_activation_page">
			<form method="post" action="options.php" id="DS_PBE_license_form">
				<div id="DS_PBE_license_form_header">
					<a href="https://divi.space/" target="_blank">
						<img src="<?php echo(plugins_url('divi-space-logo.png', __FILE__)); ?>" alt="Divi Space" />
					</a>
				</div>
				
				<div id="DS_PBE_license_form_body">
					<h3>
						<?php echo(esc_html(DS_PBE_ITEM_NAME)); ?>
						<small>v<?php echo(DS_PBE_VERSION); ?></small>
					</h3>
					
					<p>
						Thank you for purchasing <?php echo(htmlspecialchars(DS_PBE_ITEM_NAME)); ?>!<br />
						Please enter your license key below.
					</p>
					
					<?php settings_fields('DS_PBE_license'); ?>
					<?php if( false !== $license ) {
						// Need to activate license here, only if submitted
						require_once(dirname(__FILE__).'/license-key-activation.php');
						DS_PBE_activate_license();
					} ?>
					
					<label>
						<span><?php _e('License Key:', 'aspengrove-updater'); ?></span>
						<input name="DS_PBE_license_key" type="text" class="regular-text"<?php if (!empty($_GET['license_key'])) { ?> value="<?php echo(esc_attr($_GET['license_key'])); ?>"<?php } else if (!empty($license)) { ?> value="<?php echo(esc_attr($license)); ?>"<?php } ?> />
					</label>
					
					<?php
						if (isset($_GET['sl_activation']) && $_GET['sl_activation'] == 'false') {
							echo('<p id="DS_PBE_license_form_error">'.(empty($_GET['sl_message']) ? esc_html__('An unknown error has occurred. Please try again.', 'aspengrove-updater') : esc_html($_GET['sl_message'])).'</p>');
						}
						
						submit_button('Continue');
					?>
				</div>
			</form>
		</div>
	<?php
}

function DS_PBE_license_key_box() {
	$status  = get_option( 'DS_PBE_license_status' );
	$customize_url = admin_url( 'customize.php?autofocus[panel]=pbe_customizer_options' ); // Direct to Customizer Panel
	?>
		<div id="agsdi-admin-page" class="wrap">
			<div id="agsdi-instructions">
				<h2>Instructions</h2><p>With the Page Builder Everywhere plugin, you’ll be able to extend the reach of the Divi Builder plugin to outside of its standard scope to include the use of customizable modules in a range of new exciting areas, sections, and pages. From headers to footers and sidebars, 404 error pages and more, Page Builder Everywhere lets you use the Divi Builder in its normal state like never before.</p>

				<p>You can access the full documentation <a href="https://support.aspengrovestudios.com/article/70-page-builder-everywhere" target="_blank">here.</a></p>
				<p>Access the Page Builder Everywhere customizer settings <a href="<?php echo $customize_url; ?>" alt="Page Builder Everywhere Customizer">here</a></p>
			</div>


		<div id="DS_PBE_license_key_box">
			<form method="post" id="DS_PBE_license_form">
				<div id="DS_PBE_license_form_header">
					<a href="https://divi.space/" target="_blank">
						<img src="<?php echo(plugins_url('divi-space-logo.png', __FILE__)); ?>" alt="Divi Space" />
					</a>
				</div>
				
				<div id="DS_PBE_license_form_body">
					<h3>
						<?php echo(esc_html(DS_PBE_ITEM_NAME)); ?>
						<small>v<?php echo(DS_PBE_VERSION); ?></small>
					</h3>
					
					<label>
						<span><?php _e('License Key:', 'aspengrove-updater'); ?></span>
						<input type="text" readonly="readonly" value="<?php echo(esc_html(get_option('DS_PBE_license_key'))); ?>" />
					</label>
					<?php wp_nonce_field( 'DS_PBE_license_key_deactivate', 'DS_PBE_license_key_deactivate' ); ?>
					<?php
						if (isset($_GET['sl_activation']) && $_GET['sl_activation'] == 'false') {
							echo('<p id="DS_PBE_license_form_error">'.(empty($_GET['sl_message']) ? esc_html__('An unknown error has occurred. Please try again.', 'aspengrove-updater') : esc_html($_GET['sl_message'])).'</p>');
						}
						
						submit_button('Deactivate License Key', '');
					?>
				</div>
			</form>
		</div>

<h2>Check out these products too!</h2>
		<ul>

			<li><span style="font-size: 1.5em;">Get access to all of our WordPress and Divi plugins, child themes, layouts and icon packs for one low price with the <a href="https://divi.space/product/annual-membership/?utm_source=page-builder-everywhere&amp;utm_medium=plugin-ad&amp;utm_content=admin-page&amp;utm_campaign=annual-membership" target="_blank">Divi Space membership</a>!</span></li>

			<li><a href="https://aspengrovestudios.com/?utm_source=page-builder-everywhere&amp;utm_medium=plugin-ad&amp;utm_content=admin-page&amp;utm_campaign=subscribe-general#main-footer" target="_blank">Subscribe</a> to Aspen Grove Studios emails for the latest news, updates, special offers, and more!</li>
			
			<li>Create an engaging testimonial section for your website with <a href="https://divi.space/product/testify/?utm_source=page-builder-everywhere&amp;utm_medium=plugin-ad&amp;utm_content=admin-page&amp;utm_campaign=testify" target="_blank">Testify</a>!</li>
			
			<li><a href="https://divi.space/?utm_source=page-builder-everywhere&amp;utm_medium=plugin-ad&amp;utm_content=admin-page&amp;utm_campaign=subscribe-general#main-footer" target="_blank">Sign up</a> for emails from <strong>Divi Space</strong> to receive news, updates, special offers, and more!</li>
			
			<li>Get blog modules from the Extra theme in the Divi Builder with <a href="https://divi.space/product/divi-extras/?utm_source=page-builder-everywhere&amp;utm_medium=plugin-ad&amp;utm_content=admin-page&amp;utm_campaign=divi-extras" target="_blank">Divi Extras</a>!</li>
			
			<li>Create an impactful online presence for your online store with the <a href="https://divi.space/product/divi-ecommerce/?utm_source=page-builder-everywhere&amp;utm_medium=plugin-ad&amp;utm_content=admin-page&amp;utm_campaign=divi-ecommerce" target="_blank">Divi eCommerce Child Theme</a>!</li>
			
			<li>Showcase your business in a memorable &amp; engaging way with the <a href="https://divi.space/product/divi-business-pro/?utm_source=page-builder-everywhere&amp;utm_medium=plugin-ad&amp;utm_content=admin-page&amp;utm_campaign=divi-business-pro" target="_blank">Divi Business Pro child theme</a>!</li>
			
			<li>Take your Divi website to new heights with <a href="https://divi.space/product/divi-switch/?utm_source=page-builder-everywhere&amp;utm_medium=plugin-ad&amp;utm_content=admin-page&amp;utm_campaign=divi-switch" target="_blank">Divi Switch</a>, the Swiss Army Knife for Divi!</li>
			
			<li>Need a powerful sales reporting tool for WooCommerce? Check out <a href="https://aspengrovestudios.com/product/product-sales-report-pro-for-woocommerce/?utm_source=page-builder-everywhere&amp;utm_medium=plugin-ad&amp;utm_content=admin-page&amp;utm_campaign=product-sales-report-pro" target="_blank">Product Sales Report Pro</a>!</li>
			
			<li>Let your forum users upload images into their posts with <a href="https://aspengrovestudios.com/product/image-upload-for-bbpress-pro/?utm_source=page-builder-everywhere&amp;utm_medium=plugin-ad&amp;utm_content=admin-page&amp;utm_campaign=image-upload-for-bbpress-pro" target="_blank">Image Upload for bbPress Pro</a>!</li>
		</ul>

		<p><em>Divi is a registered trademark of Elegant Themes, Inc. This product is not affiliated with nor endorsed by Elegant Themes. Links to the Elegant Themes website on this page are affiliate links.</em></p>

	</div>
	<?php
}

function DS_PBE_register_option() {
	// creates our settings in the options table
	register_setting('DS_PBE_license', 'DS_PBE_license_key', 'DS_PBE_sanitize_license' );
}
add_action('admin_init', 'DS_PBE_register_option');

function DS_PBE_sanitize_license( $new ) {
	$old = get_option( 'DS_PBE_license_key' );
	if( $old && $old != $new ) {
		delete_option( 'DS_PBE_license_status' ); // new license has been entered, so must reactivate
	}
	return $new;
}