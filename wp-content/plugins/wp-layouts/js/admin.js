/*
This plugin includes code based on parts of the Divi theme and/or the
Divi Builder by Elegant Themes. See credit and licensing info in ags-layouts.php
(reproduced below with modification(s)).

This plugin includes code based on parts of the Divi theme and/or the
Divi Builder, copyright Elegant Themes, released under GPLv2, licensed under
GPLv3 for this project by special permission (see divi-CREDITS.md in the ../license
directory for credits applicable to Divi, and license.txt file in the ../license
directory for GPLv3 text).
*/
var ags_layouts_api_url, ags_layouts_take_screenshot, ags_layouts_extra_tasks;

jQuery(document).ready(function($) {
	ags_layouts_api_url = ags_layouts_admin_config.wpAjaxUrl;
}); // end document ready

function ags_layouts_message_dialog(title, message, buttons, okCb) {
	var $dialogContents = jQuery('<div>');
	
	( message.appendTo ? message : jQuery('<p>').text(message) ).appendTo($dialogContents);
	
	var dialog;
	var closeFunction = function() {
		dialog.close();
	};
	if (okCb) {
		var okFunction = function() {
			okCb();
			dialog.close();
		};
	} else {
		var okFunction = closeFunction;
	}
	switch (buttons) {
		case 'OC':
			var buttons = {
				'OK': okFunction,
				'Cancel': closeFunction
			};
			break;
		case 'YN':
			var buttons = {
				'Yes': okFunction,
				'No': closeFunction
			};
			break;
		default:
			var buttons = {
				'OK': okFunction
			};
	}
	
	dialog = agsLayoutsDialog.create({
		title: title,
		content: $dialogContents,
		reverseButtonOrder: true,
		dialogClass: 'ags-layouts-message-dialog',
		firstButtonClass: 'aspengrove-btn-primary',
		buttonClass: 'aspengrove-btn-secondary',
		buttons: buttons
	});
	
	return dialog;
}

function ags_layout_export_ui(editorName, postContent, screenshotContent, dialogContainer, dialogCloseCallback, layoutName, extraData) {
	var $dialogContents = jQuery('<div>');
	var dialog;
	
	function doExport(layoutName) {
		ags_layouts_export_job_ui(
			postContent,
			screenshotContent,
			editorName,
			layoutName,
			dialog,
			$dialogContents,
			dialog.setButtons,
			dialogCloseCallback ? dialogCloseCallback : dialog.close,
			extraData
		);
	}
	
	var dialogOptions = {
		title: 'Export Layout',
		content: $dialogContents,
		pageName: 'export-options',
		reverseButtonOrder: true,
		firstButtonClass: 'aspengrove-btn-primary',
		buttonClass: 'aspengrove-btn-secondary',
	};
	
	if (dialogContainer) {
		dialogOptions.container = dialogContainer;
	}
	
	if (layoutName) {
		dialog = agsLayoutsDialog.create(dialogOptions);
		doExport(layoutName);
	} else {
		jQuery('<h4>').text('To save this content in your online layout storage, enter a name for the layout below.').appendTo($dialogContents);
		var $layoutNameField = jQuery('<input>').attr({
			type: 'text',
			placeholder: 'Layout Name'
		}).appendTo($dialogContents);
		
		dialogOptions.buttons = {
			'Export': function() {
				var layoutName = $layoutNameField.val();
				if (layoutName) {
					doExport(layoutName);
				}
			},
			'Cancel': function() {
				dialog.close();
			}
		};
		dialog = agsLayoutsDialog.create(dialogOptions);
	}
	
	
	
	
	
}

function ags_layouts_export_job_ui(postContent, screenshotContent, editor, layoutName, dialog, $dialogBody, dialogButtonsCallback, dialogCloseCallback, extraData) {
	dialogButtonsCallback({});
	
	if (dialog) {
		dialog.setPageName('export-progress');
	}
	
	var $statusText = jQuery('<p>').attr('id', 'ags_layout_export_status').text('Generating layout image...').appendTo($dialogBody.empty());
	jQuery('<progress>').val(0).appendTo($dialogBody);
	
	
	var screenshotFailTimeout;
	
	var ags_layouts_after_screenshot = function(screenshotCanvas, previewHtml) {
		ags_layouts_after_screenshot = function() {
			console.log('Skipping duplicate after screenshot event');
		};
		clearTimeout(screenshotFailTimeout);
		
		if (dialog && dialog.isClosed()) {
			return;
		}
		$statusText.text('Uploading layout contents and image...');
		var exportParams = {
			postContent: postContent,
			layoutEditor: editor,
			layoutName: layoutName
		};
		
		if (extraData) {
			exportParams.extraData = extraData;
		}
		
		if (screenshotCanvas && screenshotCanvas.toDataURL) {
			exportParams.screenshotData = screenshotCanvas.toDataURL();
		}
		
		if (previewHtml && editor != 'Divi') {
			exportParams.previewHtml = previewHtml;
		}
		
		ags_layout_export(
			exportParams,
			dialog,
			$dialogBody,
			dialogButtonsCallback,
			dialogCloseCallback
		);
	
	};
	
	if (window.Promise) { // Prerequisite of screenshot library
		ags_layouts_take_screenshot = function($container) {
			var targetWidth = 750, targetHeight = 900, frameWidth = $container.width();
			ags_layouts_html2canvas_callback(
				$container[0],
				{
					width: frameWidth,
					height: targetHeight / targetWidth * frameWidth,
					scale: targetWidth / frameWidth,
					onclone: function(clone) {
						var inlineSvgs = ags_layouts_HTMLCollection_to_array( clone.getElementsByTagName('svg') );
						var inlineSvgsSource = ags_layouts_HTMLCollection_to_array( document.getElementsByTagName('svg') );
						if (inlineSvgs.length == inlineSvgsSource.length) {
							for (var i = 0; i < inlineSvgs.length; ++i) {
								var $svg = $(inlineSvgs[i]), $svgTempContainer = $('<div>'), svgWidth = $svg.width(), svgHeight = $svg.height();
								$svgTempContainer.insertAfter($svg).append($svg);
								var svgClasses = $svg.attr('class'), svgId = $svg.attr('id');
								$svg.attr({
									id: null,
									'class': null,
									xmlns: 'http://www.w3.org/2000/svg',
									'xmlns:xlink': 'http://www.w3.org/1999/xlink',
									width: svgWidth,
									height: svgHeight,
								});
								var svgChildren = $svg[0].getElementsByTagName('*');
								var svgChildrenSource = inlineSvgsSource[i].getElementsByTagName('*');
								if (svgChildren.length == svgChildrenSource.length) {
									for (var j = 0; j < svgChildren.length; ++j) {
										var svgChildStyle = getComputedStyle(svgChildrenSource[j]);
										var $svgChildClone = $(svgChildren[j]);
										for (var k = 0; k < svgChildStyle.length; ++k) {
											$svgChildClone.css(svgChildStyle[k], svgChildStyle[svgChildStyle[k]]);
										}
									}
								}
								
								try {
									$('<img>')
										.attr({
											id: svgId,
											'class': svgClasses,
									'data-ags-layouts-screenshot-width': svgWidth,
									'data-ags-layouts-screenshot-height': svgHeight,
											src: 'data:image/svg+xml;base64,' + btoa(  ags_layouts_utf16to8($svgTempContainer.html())  ) // Based on https://css-tricks.com/lodge/svg/09-svg-data-uris/
										})
										.replaceAll($svgTempContainer);
								} catch (ex) {
								
								}
								
								
							}
						}
						
						
					}
				},
				function(screenshotCanvas) {
					ags_layouts_after_screenshot(screenshotCanvas, $container.html());
				}
			);
		};
		
		screenshotFailTimeout = setTimeout(ags_layouts_after_screenshot, 10000);
		
		
		if (screenshotContent && screenshotContent.html) {// If screenshotContent is a jQuery element, no need to use the iframe method
			ags_layouts_take_screenshot(screenshotContent);
		}else {
			jQuery('<iframe>').attr({
				id: 'ags-layouts-screenshot-frame',
				name: 'ags-layouts-screenshot-frame'
			}).appendTo($dialogBody);
			jQuery('<form>')
			.attr({
				target: 'ags-layouts-screenshot-frame',
				method: 'post',
				action: ags_layouts_admin_config.editingPostUrl
			})
			.append(jQuery('<input>')
				.attr({
					type: 'hidden',
					name: 'ags_layouts_ss',
					value: 1
				})
			)
			.append(jQuery('<input>')
				.attr({
					type: 'hidden',
					name: 'ags_layouts_ss_content',
					value: screenshotContent ? screenshotContent : postContent
				})
			)
			.append(jQuery('<input>')
				.attr({
					type: 'hidden',
					name: 'ags_layouts_ss_editor',
					value: editor
				})
			)
			.appendTo($dialogBody)
			.submit();
		}
		
		
	} else {
		ags_layouts_after_screenshot(null);
	}
}

function ags_layouts_HTMLCollection_to_array(collection) {
	var cArray = [];
	for (var i = 0; i < collection.length; ++i) {
		cArray.push(collection.item(i));
	}
	return cArray;
}

function ags_layout_export(params, dialog, $dialogBody, dialogButtonsCallback, dialogCloseCallback) {
	if (dialog && dialog.isClosed()) {
		return;
	}
	params.action = 'ags_layouts_export';
	jQuery.post(ags_layouts_api_url, params, function(response) {
		if (response && response.success && response.data) {
			if (response.data.done) {
				jQuery('#ags_layout_export_status').text('Done!');
				$dialogBody.find('progress').val(1);
				dialogButtonsCallback(
					{
						'Close': function() {
							dialogCloseCallback();
						}
					}
				);
			} else {
				if (response.data.status) {
					jQuery('#ags_layout_export_status').text(response.data.status);
				}
				if (response.data.progress) {
					$dialogBody.find('progress').val(response.data.progress);
				}
				params.jobState = response.data.jobState;
				delete params.screenshotData;
				ags_layout_export(params, dialog, $dialogBody, dialogButtonsCallback, dialogCloseCallback);
			}
		} else {
			dialogCloseCallback();
			var errorMessage = 'Something went wrong while exporting your layout. Please try again or contact support.';
			if (response && response.data && response.data.error) {
				switch (response.data.error) {
					case 'quota':
						errorMessage = 'The layout could not be uploaded. Your layout storage quota may be exhausted, or your plan may be expired. Please upgrade or renew your plan, or contact us if you think this message is in error.';
						break;
					case 'ImageFetch':
						if (response.data.errorParams && response.data.errorParams.imageUrl) {
							errorMessage = $('<p>');
							errorMessage.html('The layout could not be uploaded (see <a href="https://support.wplayouts.space/article/507-why-do-i-see-an-error-when-i-export-my-layout-that-the-images-could-not-be-retrieved" target="_blank">this FAQ</a>). Image retrieval failed for the following image: ');
							$('<a>').text(response.data.errorParams.imageUrl).attr({
								href: response.data.errorParams.imageUrl,
								target: '_blank'
							}).appendTo(errorMessage);
						} else {
							errorMessage = 'The layout could not be uploaded (see <a href="https://support.wplayouts.space/article/507-why-do-i-see-an-error-when-i-export-my-layout-that-the-images-could-not-be-retrieved" target="_blank">this FAQ</a>). Image retrieval failed.';
						}
						
						break;
				}
			}
			ags_layouts_message_dialog('Error', errorMessage);
		}
	}, 'json');
}

function ags_layouts_table_grid_toggle_ui($table, dataTable, gridDefault, sortHandler) {
	var $ = jQuery;
	var $toggle = $('<div>')
					.addClass('ags-layouts-table-grid-toggle')
					.append(
						$('<span>').text('Display:')
					);
	var $gridButton = $('<button>')
							.attr('type', 'button')
							.text('Grid')
							.append(
								$('<span>')
									.text('Grid')
							)
							.data('ags-layouts-display-grid', 1)
							.appendTo($toggle);
	var $tableButton = $('<button>')
							.attr('type', 'button')
							.text('Table')
							.append(
								$('<span>')
									.text('Table')
							)
							.appendTo($toggle);
	
	var sortOptions = {
		'layoutDate-desc': 'Newest first',
		'layoutDate-asc': 'Oldest first',
		'layoutName-asc': 'A to Z',
		'layoutName-desc': 'Z to A'
	};
	
	var $sortSelect = $('<select>');
	for (sortOption in sortOptions) {
		$('<option>')
			.attr('value', sortOption)
			.text(sortOptions[sortOption])
			.appendTo($sortSelect);
	}
	$sortSelect
		.addClass('ags-layouts-sort-select')
		.change(function() {
			var sortSetting = $sortSelect.val().split('-');
			sortHandler(sortSetting[0], sortSetting[1]);
		})
		.appendTo($toggle);
	
	
	$toggle.children('button').click(function() {
		var $button = $(this);
		$button.addClass('ags-layouts-active').siblings().removeClass('ags-layouts-active');
		
		var $tableContainer = $table.closest('.ags-layouts-table');
		var currentValue = $tableContainer.hasClass('ags-layouts-display-grid');
		var newValue = ($button.data('ags-layouts-display-grid') == true);
		$tableContainer
			.toggleClass('ags-layouts-display-grid', newValue)
			.toggleClass('ags-layouts-display-table', !newValue);
		if (newValue != currentValue) {
			dataTable.draw('page');
		}
	});
	
	if (gridDefault) {
		$gridButton.click();
	} else {
		$tableButton.click();
	}
	
	return $toggle;
				
}

function ags_layout_import_ui(editorName, container, tab, contentsCb, forceImportLocation) {
	var dialog;
	var $ = jQuery;
	
	var dialogOptions = {
		title: 'Import Layout',
		autoLoader: true,
		reverseButtonOrder: true,
		firstButtonClass: 'aspengrove-btn-primary',
		buttonClass: 'aspengrove-btn-secondary',
		pageName: 'import-list',
		buttons: {}
	};
	
	var dialogShowLoader = function() {
		if (dialog) {
			dialog.showLoader();
		}
	};
	
	var dialogHideLoader = function() {
		if (dialog) {
			dialog.hideLoader();
		}
	};
	
	var onLayoutCollectionSelect = function(layoutCollection, $tabContent, $tableContainer) {
		if (layoutCollection) {
			dialog.showLoader();
			
			$.get(ags_layouts_api_url, {
				action: 'ags_layouts_list',
				ags_layouts_collection: layoutCollection.layoutId,
				ags_layouts_editor: editorName
			}, function(response) {
				if (response.collection) {
					var $layoutCollectionView = $('<div>').addClass('ags-layouts-collection-view');
					var $layoutCollectionLayouts = $('<ol>').addClass('ags-layouts-collection-layouts');
					
					if (response.layouts.length) {
						var layouts = response.layouts;
						for (var i = 0; i < layouts.length; ++i) {
							var $item = $('<li>')
											.addClass('ags-layout-collection-link')
											.data('ags-layout-id', layouts[i].layoutId)
											.click((function(layout) {
												return function(layoutClick) {
													if (!layoutClick.target.href) {
														ags_layout_import(layout, contentsCb, editorName, dialog, $tabContent, $layoutCollectionView, forceImportLocation);
													}
												};
											})(layouts[i]))
											.appendTo($layoutCollectionLayouts),
								$itemImage = $('<span>').addClass('ags-layouts-item-image').appendTo($item),
								$itemDetails = $('<span>').addClass('ags-layouts-item-details').appendTo($item);
							
							if (layouts[i].hasLayoutImage) {
								$itemImage.css('background-image', 'url(\'' + ags_layouts_api_url + '?action=ags_layouts_get_image&image=L&layoutId=' + layouts[i].layoutId + '\')');
							} else {
								$item.addClass('ags-layouts-no-image');
							}
							$('<span>').text(layouts[i].layoutName).appendTo($itemDetails);
							layouts[i].preview = '<a href="' + ags_layouts_admin_config.wpFrontendUrl + '?ags_layouts_preview=' + (layouts[i].layoutId * 1) + '" target="_blank" class="ags-layouts-preview-link">Live Preview</a>';
							$itemDetails.append(layouts[i].preview);
						}
					}
					
					$layoutCollectionView
						.append(
							$('<div>')
								.addClass('ags-layouts-button-back-container')
								.append(
									$('<button>')
										.addClass('ags-layouts-button-back aspengrove-btn-secondary')
										.text('Back')
										.click(function() {
											$layoutCollectionView.remove();
											dialog.setPageName('import-list');
											$tableContainer.show();
										})
								)
							
						);
					
					var $layoutCollectionInfo = $('<div>').addClass('ags-layouts-collection-info clearfix');
					
					if (response.collection.image) {
						$layoutCollectionView.addClass('ags-layouts-collection-has-image');
						$layoutCollectionInfo.append($('<img>').attr('src', response.collection.image));
					}
					
					$layoutCollectionInfo.append($('<h3>').text(response.collection.name)).append(
						$('<div>').addClass('ags-layouts-collection-description').html(response.collection.description)
					);
					
					$layoutCollectionView
						.append($layoutCollectionInfo)
						.append($layoutCollectionLayouts);
					
					$tableContainer.hide();
					dialog.setPageName('import-collection');
					$tabContent.append($layoutCollectionView);
					
				}
			}, 'json').always(function() {
				dialog.hideLoader();
			});
		}
		return 1; // Indicates selection has been handled, no further processing by DT
	}
	
	var onLayoutSelect = function(layout, $tabContent, $tableContainer) {
		if (layout) {
			ags_layout_import(layout, contentsCb, editorName, dialog, $tabContent, $tableContainer, forceImportLocation);
		}
		return 1; // Indicates selection has been handled, no further processing by DT
	}
	
	if (tab) {
		var listUi = ags_layouts_list_ui(
			tab == 'layouts_my' ? -1 : 0,
			editorName,
			'50vh',
			tab == 'layouts_my' ? onLayoutSelect : onLayoutCollectionSelect,
			dialogShowLoader,
			dialogHideLoader
		);
		dialogOptions.content = listUi[0];
		dialogOptions.onLoad = listUi[1];
	} else {
		var listUiAgs = ags_layouts_list_ui(0, editorName, '50vh', onLayoutCollectionSelect, dialogShowLoader, dialogHideLoader);
		var listUiMy = ags_layouts_list_ui(-1, editorName, '50vh', onLayoutSelect, dialogShowLoader, dialogHideLoader);
		dialogOptions.tabs = {
			layouts_my: {
				name: 'My Layouts',
				content: listUiMy[0],
				onLoad: listUiMy[1]
			},
			layouts_ags: {
				name: 'WP Layouts',
				content: listUiAgs[0],
				onLoad: listUiAgs[1]
			}
		}
	}
	
	if (container) {
		dialogOptions.container = container;
	}
	
	dialog = agsLayoutsDialog.create(dialogOptions);
	
}

function ags_layouts_list_ui(layoutCollectionId, editorName, maxBodyHeight, itemSelectCb, loaderShowCb, loaderHideCb, allowMultiSelection, longPages) {
	var $ = jQuery,
		isLayoutCollections = !layoutCollectionId,
		tableDom = '<"ags-layouts-list-header"fl<"ags-layouts-table-grid-toggle">>t<"ags-layouts-list-footer"ip>',
		tableLanguage = {
			search: '',
			lengthMenu: '_MENU_ per page',
		},
		$content = $('<div>')
					.addClass('ags-layouts-table')
					.append($('<table>')),
		dataTable,
		getDataTableFunction = function() {
			return dataTable;
		};
	
	var onContentLoad = function($tabContent) {
		var $tableContainer = $tabContent.find('.ags-layouts-table:first');
		var $table = $tableContainer.find('table:first');
		
		if (loaderShowCb) {
			$table.on('preXhr.dt', loaderShowCb);
		}
		
		if (loaderHideCb) {
			$table.on('xhr.dt', loaderHideCb);
		}
		
		if (isLayoutCollections) {
			var tableColumns = [
				{
					name: 'layoutName',
					data: 'layoutName',
					title: 'Layout Collection',
				},
				{
					name: 'collectionCount',
					data: 'collectionCount',
					title: 'Number of Layouts',
					sortable: false
				}
			];
		} else {
			var tableColumns = [
				{
					name: 'layoutName',
					data: 'layoutName',
					title: 'Layout Name',
				},
			];
			
			if (!editorName) {
				tableColumns.push({
					name: 'layoutEditor',
					data: 'layoutEditorDisplay',
					title: 'Editor',
					sortable: false
				});
			}
			
			
			tableColumns.push({
				name: 'layoutDate',
				data: 'layoutDate',
				title: 'Upload Date',
			});
			tableColumns.push({
				name: 'preview',
				data: 'preview',
				title: 'Preview',
				sortable:false,
				className: 'ags-layouts-table-preview-cell'
			});
			
			
		}
		
		var tableErrorDialog, hasNoCollectionsAccess;
		
		var tableParams = {
			columns: tableColumns,
			serverSide: true,
			dom: tableDom,
			language: tableLanguage,
			lengthMenu: [8, 16, 48, 96],
			ajax: {
				url: ags_layouts_api_url + (ags_layouts_api_url.indexOf('?') == -1 ? '?' : '&') + 'action=ags_layouts_list' + (editorName ? '&ags_layouts_editor=' + editorName : '') + (isLayoutCollections ? '' : '&ags_layouts_collection=' + layoutCollectionId),
				dataSrc: function(response) {
					if (response.data) {
						for (var i = 0; i < response.data.length; ++i) {
							if (!isLayoutCollections) {
								response.data[i].preview = '<a href="' + ags_layouts_admin_config.wpFrontendUrl + '?ags_layouts_preview=' + (response.data[i].layoutId * 1) + '&layoutEditor=' + (response.data[i].layoutEditor ? response.data[i].layoutEditor : editorName) + '" target="_blank" class="ags-layouts-preview-link">Live Preview</a>';
							}
							response.data[i].DT_RowClass = isLayoutCollections ? 'ags-layout-collection-link' : 'ags-layout-link';
							response.data[i].DT_RowData = {'ags-layout': response.data[i]};
							response.data[i].layoutEditorDisplay = ags_layouts_admin_config.editorNames[response.data[i].layoutEditor]
																	? ags_layouts_admin_config.editorNames[response.data[i].layoutEditor]
																	: ags_layouts_admin_config.editorNames['?'];
						}
						return response.data;
					}
					
					// If we reach here, something went wrong
					switch (response.message) {
						case 'NoCollectionsAccess':
							
							if (!hasNoCollectionsAccess) {
								hasNoCollectionsAccess = true;
								$tableContainer.hide().after(
									'<div class="ags-layouts-no-collections-access">'
									+ '<h4>Sorry, this feature is only available in WP Layouts Pro.</h4>'
									+ '<p>WP Layouts Pro includes unlimited access to our pre-designed layouts for use in your sites! Please <a href="https://wplayouts.space/" target="_blank">upgrade to a Pro pricing plan</a> to enable this feature in your WP Layouts account.</p>'
									+ '</div>'
								);
							}
								
							break;
						default:
							if (tableErrorDialog) {
								tableErrorDialog.close();
							}
							tableErrorDialog = ags_layouts_message_dialog('Error', response.message ? response.message : 'Something went wrong while retrieving the list of layouts. Please try again or contact support.');
					}
					
					return [];
				}
			},
			select: {
				style: allowMultiSelection ? 'os' : 'single',
				info: false,
			},
			drawCallback: function(tableSettings) {
				var isGridDisplay = $tableContainer.hasClass('ags-layouts-display-grid');
				$table.find('thead:first').toggle(!isGridDisplay);
				if (tableSettings.json && tableSettings.json.data && isGridDisplay) {
					
					if (tableSettings.json.data.length) {
						var $grid = $('<ol>').addClass('ags-layouts-list-grid');
						var gridItemFactory = function(layoutId) {
							return $('<li>')
										.addClass(isLayoutCollections ? 'ags-layout-collection-link' : 'ags-layout-link')
										.data('ags-layout', layoutId)
										.click(function(layoutClick) {
											if (!layoutClick.target.href) {
												itemSelectCb(layoutId, $tabContent, $tableContainer)
												|| $(this)
													.addClass('ags-layouts-grid-item-selected')
													.siblings('.ags-layouts-grid-item-selected')
													.removeClass('ags-layouts-grid-item-selected');
											}
										})
										.appendTo($grid);
						};
					
						var layouts = tableSettings.json.data;
						for (var i = 0; i < layouts.length; ++i) {
							var $item = gridItemFactory(layouts[i]),
										$itemImage = $('<span>').addClass('ags-layouts-item-image').appendTo($item),
										$itemDetails = $('<span>').addClass('ags-layouts-item-details').appendTo($item);
							if (layouts[i].hasLayoutImage) {
								$itemImage.css('background-image', 'url(\'' + ags_layouts_api_url + '?action=ags_layouts_get_image&image=L&layoutId=' + layouts[i].layoutId + '\')');
							} else {
								$item.addClass('ags-layouts-no-image');
							}
							$('<span>').text(layouts[i].layoutName).appendTo($itemDetails);
							if (layouts[i].preview) {
								$itemDetails.append(layouts[i].preview);
							}
							
						}
					} else {
						var $grid = $('<p>')
										.addClass('ags-layouts-list-grid-empty')
										.text(isLayoutCollections ? 'Sorry, we don\'t have any layout collections for this page builder yet. Please check back later!' : 'There are no layouts to display.');
					}
					
					$(tableSettings.nTBody).empty().append(
						$('<tr>').append(
							$('<td>').attr('colspan', 2).append(
								$grid
							)
						)
					);
					
					return false;
				}
			},
			
		};
		
		function getColumnIndexByName(columnName) {
			for (var i = 0; i < tableParams.columns.length; ++i) {
				if (tableParams.columns[i].name == columnName) {
					return i;
				}
			}
		}
		var defaultOrdering = [getColumnIndexByName(isLayoutCollections ? 'layoutName' : 'layoutDate'), 'desc'];
		tableParams.order = [defaultOrdering];
		
		if (maxBodyHeight) {
			tableParams.scrollY = maxBodyHeight;
		}
		
		dataTable = $table.ags_layouts_DataTable(tableParams);
		
		
		
		$tableContainer
			.find('.ags-layouts-table-grid-toggle')
			.replaceWith(ags_layouts_table_grid_toggle_ui($table, dataTable, true, function(sortColumn, sortDirection) {
				var newSortSetting = [
					getColumnIndexByName(sortColumn),
					sortDirection
				];
				dataTable.order(newSortSetting);
				dataTable.ajax.reload();
			}));
		
		var isUserSelecting = false;
		
		$table.on(
			'user-select.dt',
			function(a, b, c, d, clickEvent) {
				if (clickEvent.target && itemSelectCb) {
					var $clickTarget = $(clickEvent.target);
					var $link = $clickTarget.closest('.ags-layout-link, .ags-layout-collection-link');
					
					if ($clickTarget.hasClass('ags-layouts-preview-link')) {
						var shouldSelect = false;
					} else if ($link.hasClass('selected')) {
						var shouldSelect = true;
					} else {
						// Process multi selected layouts here
						var shouldSelect = !itemSelectCb($link.data('ags-layout'), $tabContent, $tableContainer);
						if (shouldSelect) {
							isUserSelecting = true;
						}
					}
					return shouldSelect;
				}
			}
		);
		
		$table.on(
			'select.dt',
			function() {
				isUserSelecting = false;
			}
		);
		
		$table.on(
			'deselect.dt',
			function() {
				if (!isUserSelecting && itemSelectCb) {
					itemSelectCb(null, $tabContent, $tableContainer);
				}
			}
		);
		
	};
	
	
	return [$content,onContentLoad,getDataTableFunction];
	
}

function ags_layout_import(layout, contentsCb, editorName, dialog, $tabContent, $currentView, forceImportLocation) {
	var $ = jQuery;
	
	$currentView.hide();	
	var $importer = $('<div>').appendTo($tabContent);
	
	var onSelectImportLocation = function(importLocation) {
		var importParams = {
			layoutId: layout.layoutId,
			layoutEditor: editorName,
			importLocation: importLocation
		};
		
		if (editorName == 'divi') {
			var dialogOptions = {
				title: 'Import Layout',
				firstButtonClass: 'aspengrove-btn-primary',
				buttonClass: 'aspengrove-btn-secondary',
				pageName: 'import-progress',
				buttons: {},
				content: $importer
			};
			dialog = agsLayoutsDialog.create(dialogOptions);
		} else {
			dialog.setPageName('import-progress');
		}
	
		
		var $statusText = $('<div>').attr('id', 'ags-layouts-import-status').appendTo($importer);
		var $progress = $('<progress>').attr('id', 'ags-layouts-import-progress').val(0).appendTo($importer);
		dialog.setButtons({
			/*'Cancel': function() {
				
			},*/
		});
		
		var progressCallback = function (progress, statusText, warnings) {
			$progress.val(progress);
			$statusText.text(statusText);
			if (progress == 1) {
				dialog.close();
				if (warnings) {
					ags_layouts_message_dialog('Import Warning(s)', warnings.join(' '));
				}
			}
		};
		
		ags_layouts_run_extra_tasks(editorName, 'import', 'before', progressCallback, function() {
		
			ags_layout_get(importParams, false, dialog, progressCallback, contentsCb);
			
		});
		
	};
	
	if (forceImportLocation) {
		onSelectImportLocation(forceImportLocation);
	} else {
		$importer.append($('<h4>').text('You have selected the layout "' + layout.layoutName + '" for import.'));
		
		if (editorName == 'divi') {
			$('<p>')
				.addClass('ags-layouts-notification ags-layouts-notification-info')
				.text('Any unsaved content in the Divi builder will be saved prior to import. Any unsaved changes you have made outside of the Divi builder will be lost.')
				.appendTo($importer);
		}
		
		var importLocations = {
			'above': 'Insert the layout above any existing content',
			'below': 'Insert the layout below any existing content',
			'replace': 'Replace any existing content with the layout'
		};
		
		var $importLocationOptions = $('<div>').addClass('ags-layout-import-location-options');
		var radioAttrs = {
			type: 'radio',
			name: 'ags_layout_import_location',
			checked: true
		};
		for (importLocation in importLocations) {
			radioAttrs.value = importLocation;
			$('<label>')
				.text(importLocations[importLocation])
				.prepend(
					$('<input>')
						.attr(radioAttrs)
				)
				.appendTo($importLocationOptions);
			delete radioAttrs.checked;
		}
		
		$importLocationOptions.appendTo($importer);
		
		
		dialog.setPageName('import-options');
		dialog.setButtons({
			'Import': function() {
				var selectedImportLocation = $importLocationOptions.find('input:checked').val();
				$importLocationOptions.siblings('p').remove();
				$importLocationOptions.remove();
				onSelectImportLocation(selectedImportLocation);
			},
			'Cancel': function() {
				dialog.setButtons({});
				$importer.remove();
				dialog.setPageName('import-list');
				$currentView.show();
				//dialog.showTabs();
			},
		});
	}
	
}

function ags_layout_get(params, existingJob, dialog, progressCb, contentsCb) {
	if (dialog && dialog.isClosed()) {
		return;
	}
	params.action = 'ags_layouts_get';
	params.postId = window.ags_layouts_admin_config.editingPostId;
	if (existingJob) {
		delete params.newJob;
	} else {
		params.newJob = 1;
	}
	
	jQuery.get(
		ags_layouts_api_url,
		params,
		function(response) {
			if (response && response.success && response.data && response.data.status) {
				if (progressCb) {
					progressCb(response.data.progress, response.data.status, response.data.warnings);
				}
				if (response.data.done) {
				
					ags_layouts_run_extra_tasks(params.layoutEditor, 'import', 'after', progressCb, function() {
						if (contentsCb) {
							contentsCb(response.data.layoutContents, params);
						} else {
							location.reload();
						}
					});
					
				} else {
					ags_layout_get(params, true, dialog, progressCb, contentsCb);
				}
			}
		},
		'json'
	);
}

function ags_layouts_register_extra_task(taskName, callback, editorName, operation, order) {
	/*
		Valid argument values:
			- operation: import, export*
			- order: before, after*
		(* not yet implemented)
	*/

	if (!ags_layouts_extra_tasks) {
		ags_layouts_extra_tasks = {};
	}
	if (!ags_layouts_extra_tasks[editorName]) {
		ags_layouts_extra_tasks[editorName] = {};
	}
	if (!ags_layouts_extra_tasks[editorName][operation]) {
		ags_layouts_extra_tasks[editorName][operation] = {};
	}
	if (!ags_layouts_extra_tasks[editorName][operation][order]) {
		ags_layouts_extra_tasks[editorName][operation][order] = [];
	}
	
	ags_layouts_extra_tasks[editorName][operation][order].push({
		name: taskName,
		callback: callback
	});
}

function ags_layouts_run_extra_tasks(editorName, operation, order, progressCallback, doneCallback, _index) {
	_index = _index ? _index : 0;
	if (ags_layouts_extra_tasks && ags_layouts_extra_tasks[editorName]
			&& ags_layouts_extra_tasks[editorName][operation] && ags_layouts_extra_tasks[editorName][operation][order]
			&& ags_layouts_extra_tasks[editorName][operation][order].length > _index) {
		var currentTask = ags_layouts_extra_tasks[editorName][operation][order][_index];
		progressCallback(order == 'after' ? 1 : 0, currentTask.name + '...');
		currentTask.callback(function() {
			ags_layouts_run_extra_tasks(editorName, operation, order, progressCallback, doneCallback, ++_index);
		});
	} else {
		doneCallback();
	}
}

/* The following functions contain code from WP and Divi Icons Pro (fb.js) by Aspen Grove Studios */
function ags_layouts_onCreateElementWithId(id, callback) {
	var MO = window.MutationObserver ? window.MutationObserver : window.WebkitMutationObserver;
	if (MO) {
		(new MO(function(events) {
			jQuery.each(events, function(i, event) {
				if (event.addedNodes && event.addedNodes.length) {
					jQuery.each(event.addedNodes, function(i, node) {
						if (node.id == id) {
							callback(node);
						}
					});
				}
			});
		})).observe(document.body, {characterData: true, childList: true, subtree: true});
	}
}
function ags_layouts_onElementClassAdded(element, className, callback) {
	var MO = window.MutationObserver ? window.MutationObserver : window.WebkitMutationObserver;
	if (MO) {
		var thisMO = new MO(function() {
			if (
				element.className.indexOf(' ' + className + ' ') != -1
				|| element.className.startsWith(className + ' ')
				|| element.className.endsWith(' ' + className)
				|| element.className == className
			) {
				thisMO.disconnect();
				callback(element);
			}
			
		});
		thisMO.observe(element, {attributes: true, attributeFilter: ['class']});
	}
}
/* End functions with WP and Divi Icons Pro code */



/* utf.js - UTF-8 <=> UTF-16 convertion
 *
 * Copyright (C) 1999 Masanao Izumo <iz@onicos.co.jp>
 * Version: 1.0
 * LastModified: Dec 25 1999
 * This library is free.  You can redistribute it and/or modify it.
 */

/*
 * Interfaces:
 * utf8 = utf16to8(utf16);
 * utf16 = utf16to8(utf8);
 */

function ags_layouts_utf16to8(str) {
    var out, i, len, c;

    out = "";
    len = str.length;
    for(i = 0; i < len; i++) {
	c = str.charCodeAt(i);
	if ((c >= 0x0001) && (c <= 0x007F)) {
	    out += str.charAt(i);
	} else if (c > 0x07FF) {
	    out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));
	    out += String.fromCharCode(0x80 | ((c >>  6) & 0x3F));
	    out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));
	} else {
	    out += String.fromCharCode(0xC0 | ((c >>  6) & 0x1F));
	    out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));
	}
    }
    return out;
}

function ags_layouts_utf8to16(str) {
    var out, i, len, c;
    var char2, char3;

    out = "";
    len = str.length;
    i = 0;
    while(i < len) {
	c = str.charCodeAt(i++);
	switch(c >> 4)
	{ 
	  case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
	    // 0xxxxxxx
	    out += str.charAt(i-1);
	    break;
	  case 12: case 13:
	    // 110x xxxx   10xx xxxx
	    char2 = str.charCodeAt(i++);
	    out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
	    break;
	  case 14:
	    // 1110 xxxx  10xx xxxx  10xx xxxx
	    char2 = str.charCodeAt(i++);
	    char3 = str.charCodeAt(i++);
	    out += String.fromCharCode(((c & 0x0F) << 12) |
					   ((char2 & 0x3F) << 6) |
					   ((char3 & 0x3F) << 0));
	    break;
	}
    }

    return out;
}

/* End utf.js */
