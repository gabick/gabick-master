<?php
/*
This file includes code based on parts of WordPress and the Gutenberg plugin
by Automattic, released under GPLv2+, licensed under GPLv3+ (see wp-license.txt
in the plugin root -> license directory for the license and additional credits
applicable to WordPress and Gutenberg, and the license.txt file in the plugin
root -> license directory for GPLv3 text).
*/

AGSLayouts::VERSION; // Access control

class AGSLayoutsGutenberg {
	
	public static function setup() {
		
		/* Hooks */
		add_action('admin_enqueue_scripts', array('AGSLayoutsGutenberg', 'scripts'));
		add_action('add_meta_boxes', array('AGSLayoutsGutenberg', 'gutenbergMetaBox'));
		add_filter('block_categories', array('AGSLayoutsGutenberg', 'registerBlockCategories'));
		
	}
	
	public static function scripts() {
		//wp_enqueue_style('ags-layouts-boilerplate', AGSLayouts::$pluginBaseUrl.'integrations/Boilerplate/boilerplate.css', array(), AGSLayouts::VERSION);
		wp_enqueue_script('ags-layouts-gutenberg', AGSLayouts::$pluginBaseUrl.'integrations/Gutenberg/gutenberg.js', array('jquery'), AGSLayouts::VERSION);
		
		if (!empty($_GET['ags_layouts_preview'])) {
			$layoutContents = @json_decode( get_the_content() );
			if ($layoutContents) {
				wp_localize_script('ags-layouts-gutenberg', 'ags_layouts_gutenberg_preview', $layoutContents);
			}
		}
	}
	
	public static function registerBlockCategories($blockCategories) {
		// Maybe add icon?
		$blockCategories[] = array(
			'slug' => 'ags-layouts-ags',
			'title' => 'WP Layouts'
		);
		$blockCategories[] = array(
			'slug' => 'ags-layouts-my',
			'title' => 'My WP Layouts'
		);
		return $blockCategories;
	}
	
	public static function gutenbergMetaBox() {
		if (isset($GLOBALS['post']) && function_exists('use_block_editor_for_post') && use_block_editor_for_post($GLOBALS['post'])) {
			add_meta_box('ags-layouts-gutenberg', 'Import/Export Layout', function() {
				$postContent = trim($GLOBALS['post']->post_content);
				$isNonGutenbergContent = !empty($postContent) && substr($postContent, 0, 4) != '<!--';
				if ($isNonGutenbergContent) {
?>
<p class="ags-layouts-notification ags-layouts-notification-info">This content doesn't look like it was created by the block editor (when the editor loaded). The block editor integration in WP Layouts is only designed to work with block editor content. <a href="#ags-layouts-gutenberg" onclick="jQuery(this).parent().next().removeClass('hidden').prev().remove();">Click here</a> to access the integration anyway.</p>
<?php
				}
?>
<div<?php if ($isNonGutenbergContent) echo(' class="hidden"'); ?>>
<button type="button" onclick="ags_layouts_gutenbergImportLayout();" class="aspengrove-btn-secondary">Import from WP Layouts</button>
<button type="button" onclick="ags_layouts_gutenbergExportAll();" class="aspengrove-btn-secondary">Export to WP Layouts</button>
</div>
<?php
			}, get_current_screen(), 'side');
		}
	}
	
	static function setupPreviewPost($previewPostId, $layoutContents) {
		return admin_url('post.php?post='.$previewPostId.'&action=edit');
	}
}

AGSLayoutsGutenberg::setup();