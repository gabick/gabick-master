<?php
AGSLayouts::VERSION; // Access control

class AGSLayoutsBoilerplate {

	//const VERSION = '0.1.4';
	//public static $pluginBaseUrl, $pluginDirectory;
	
	public static function setup() {
		
		/* Hooks */
		add_action('admin_enqueue_scripts', array('AGSLayoutsBoilerplate', 'scripts'));
		add_action('wp_enqueue_scripts', array('AGSLayoutsBoilerplate', 'scripts'));
		//add_action('wp_ajax_ags_layouts_get', array('AGSLayouts', 'getLayout'));
		
	}
	
	public static function scripts() {
		//wp_enqueue_style('ags-layouts-boilerplate', AGSLayouts::$pluginBaseUrl.'integrations/Boilerplate/boilerplate.css', array(), AGSLayouts::VERSION);
		wp_enqueue_script('ags-layouts-boilerplate', AGSLayouts::$pluginBaseUrl.'integrations/Boilerplate/boilerplate.js', array('jquery'), AGSLayouts::VERSION);
		
		/*$adminConfig = array(
			'pluginBaseUrl' => self::$pluginBaseUrl
		);
		wp_localize_script('ags-layouts-boilerplate', 'ags_layouts_boilerplate_config', $adminConfig);*/
	}
	
	
	public static function getLayout() {
		
	}
	
}

AGSLayoutsBoilerplate::setup();