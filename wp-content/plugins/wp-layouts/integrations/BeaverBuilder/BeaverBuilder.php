<?php
/*
This file includes code based on and/or copied from parts of Beaver Builder Plugin
(Standard Version) and/or Beaver Builder Plugin (Lite Version), copyright 2014 Beaver Builder,
released under GPLv2+ (according to the plugin's readme.txt file; fl-builder.php
specifies different licensing but we are using the licensing specified in readme.txt),
licensed under GPLv3+ (see license.txt file in this plugin's "license" directory for GPLv3 text).

File modified by Jonathan Hall: 2019-03-07, 2019-03-11, 2019-03-12, 2019-03-13, 2019-03-14, 2019-03-15, 2019-03-19, 2019-03-20, 2019-03-21, 2019-03-25, 2019-05-08, 2019-08-28, 2019-09-20
*/

AGSLayouts::VERSION; // Access control

class AGSLayoutsBB {

	private static $pluginBaseUrl, $pluginDirectory;
	
	public static function setup() {
		self::$pluginBaseUrl = plugin_dir_url(__FILE__);
		self::$pluginDirectory = __DIR__.'/';
		
		/* Hooks */
		add_action('wp_enqueue_scripts', array('AGSLayoutsBB', 'frontendScripts'));
		add_action('wp_ajax_ags_layouts_bb_get_nodes', array('AGSLayoutsBB', 'getPostNodesAjax'));
		add_filter('fl_builder_main_menu', array('AGSLayoutsBB', 'filterBuilderMenu'));
		add_filter('fl_builder_ui_bar_buttons', array('AGSLayoutsBB', 'filterBuilderTopBarButtons'));
	}
	
	public static function frontendScripts($forFrontend=false) {
		wp_enqueue_script('ags-layouts-bb', AGSLayouts::$pluginBaseUrl.'integrations/BeaverBuilder/bb.js', array('jquery'), AGSLayouts::VERSION);
	}
	
	public static function getPostNodesAjax() {
		if (!empty($_POST['postId'])) {
			AGSLayouts::requireEditPermission($_POST['postId']);
			$builderData = get_post_meta($_POST['postId'], '_fl_builder_draft', true);
			if (!empty($_POST['nodeId'])) {
				if (!isset($builderData[$_POST['nodeId']])) {
					wp_send_json_error();
				}
				$nodes = self::getNodeWithDescendants($_POST['nodeId'], $builderData);
			} else {
				$nodes = array_values($builderData);
			}
			wp_send_json_success(self::nodesToJson($nodes));
		}
		wp_send_json_error();
	}
	
	private static function getNodeWithDescendants($nodeId, $builderData, &$nodesArray=array()) {
		$nodesArray[] = $builderData[$nodeId];
		foreach ($builderData as $node) {
			if (isset($node->parent) && $node->parent == $nodeId) {
				self::getNodeWithDescendants($node->node, $builderData, $nodesArray);
			}
		}
		return $nodesArray;
	}
	
	public static function nodesToJson($data, $_recur=false) {
		switch (gettype($data)) {
			case 'array':
				$children = array();
				foreach ($data as $childKey => $childValue) {
					$childResult = self::nodesToJson($childValue, true);
					$children[$_recur ? $childResult[0].$childKey : $childKey] = $childResult[1];
				}
				return $_recur ? array('@', $children) : json_encode($children);
			case 'object':
				$children = array();
				foreach (get_object_vars($data) as $childKey => $childValue) {
					$childResult = self::nodesToJson($childValue, true);
					$children[$_recur ? $childResult[0].$childKey : $childKey] = $childResult[1];
				}
				return $_recur ? array('_', $children) : json_encode($children);
		}
		return $_recur ? array('_', $data) : json_encode($data);
	}
	
	public static function nodesFromJson($data, $_transformToArray=false, $_recur=false) {
		if (!$_recur) {
			$data = json_decode($data);
		}
		
		switch (gettype($data)) {
			case 'array':
				foreach ($data as $childKey => $childValue) {
					unset($data[$childKey]);
					$data[$_recur ? substr($childKey, 1) : $childKey] = self::nodesFromJson($childValue, $childKey[0] == '@', true);
				}
				return $data;
			case 'object':
				$objectVars = get_object_vars($data);
				if ($_transformToArray) {
					$data = array();
				}
				foreach ($objectVars as $childKey => $childValue) {
					$childResult = self::nodesFromJson($childValue, $childKey[0] == '@', true);
					$trueKey = substr($childKey, 1);
					if ($_transformToArray) {
						$data[$trueKey] = $childResult;
					} else {
						unset($data->$childKey);
						$data->$trueKey = $childResult;
					}
				}
				return $data;
		}
		return $data;
	}
	
	public static function insertLayout($layoutContents, $postId, $importLocation) {
		$layoutContents = self::nodesFromJson($layoutContents);
		if (!empty($postId) && is_array($layoutContents)) {
			AGSLayouts::requireEditPermission($postId);
			if ($importLocation == 'replace') {
				update_post_meta($postId, '_fl_builder_draft', array());
			}
			
			add_filter('fl_builder_node_status', array('AGSLayoutsBB', 'forceEditStatusDraft'));
			FLBuilderModel::set_post_id($postId);
			$rowNode = $importLocation == 'above' ? FLBuilderModel::add_row('1-col', 0) : FLBuilderModel::add_row('1-col');
			
			$builderData = get_post_meta($postId, '_fl_builder_draft', true);
			
			$allNodeIds = array();
			foreach ($layoutContents as &$node) {
				
				$originalNodeId = $node->node;
				do {
					$hasNodeConflict = false;
					foreach ($builderData as $nodeId => $existingNode) {
						if ($nodeId == $node->node) {
							$hasNodeConflict = true;
							break;
						}
					}
					if ($hasNodeConflict) {
						$node->node = FLBuilderModel::generate_node_id();
					}
				} while ($hasNodeConflict);
				
				if ($node->node != $originalNodeId) {
					foreach ($layoutContents as &$node2) {
						if ($node2->parent == $originalNodeId) {
							$node2->parent = $node->node;
						}
					}
				}
				
			}
			
			$newBuilderData = array();
			$rootNode = $layoutContents[0];
			foreach ($layoutContents as &$node) {
				if (empty($node->parent)) {
					$rootNode = $node;
					$node->parent = null;
				}
				$newBuilderData[$node->node] = $node;
			}
			
			$columnGroupNode = current(FLBuilderModel::get_child_nodes($rowNode));
			$columnNodeId = current(FLBuilderModel::get_child_nodes($columnGroupNode))->node;
			
			if ($rootNode->type == 'row') {
				$rootNode->position = $rowNode->position;
				unset($builderData[$rowNode->node], $builderData[$columnGroupNode->node], $builderData[$columnNodeId]);
			} else {
				$rootNode->position = 0;
				$rootNode->parent = $columnNodeId;
			}
			
			$builderData = array_merge($builderData, $newBuilderData);
			remove_filter('fl_builder_node_status', array('AGSLayoutsBB', 'forceEditStatusDraft'));
			
			update_post_meta($postId, '_fl_builder_draft', $builderData);
			return true;
		}
		
		return false;
	}
	
	public static function forceEditStatusDraft() {
		return 'draft';
	}
	
	public static function preUploadProcess($layout) {
		$layout = self::nodesFromJson($layout);
		
		if (is_array($layout)) {
		
			$nodeIds = array();
			foreach ($layout as $node) {
				$nodeIds[$node->node] = true;
			}
		
			foreach ($layout as &$node) {
				if (empty($nodeIds[$node->parent])) {
					unset($node->parent);
				}
				if (empty($node->parent)) {
					unset($node->position);
				}
				
				if (isset($node->settings) && isset($node->settings->type)) {
				
					if ($node->settings->type == 'photo') {
						unset($node->settings->data);
					} else {
						unset($node->settings->photo_data);
					}
				
				
					if (!empty($node->settings->photo) && is_numeric($node->settings->photo)) {
						$imageUrl = wp_get_attachment_url($node->settings->photo);
						if (!empty($imageUrl)) {
							$node->settings->photo = 'agslayouts.id:'.$imageUrl;
						}
					} else if (!empty($node->settings->photos)) {
						foreach ($node->settings->photos as &$photo) {
							if (is_numeric($photo)) {
								$imageUrl = wp_get_attachment_url($photo);
								if (!empty($imageUrl)) {
									$photo = 'agslayouts.id:'.$imageUrl;
								}
							}
						}
					}
				}
				
			}
		}
		
		return self::nodesToJson($layout);
	}
	
	public static function filterBuilderMenu($menu) {
		if (isset($menu['main']['items'])) {
			$menuIndex = 11;
			while (isset($menu['main']['items'][$menuIndex]) || isset($menu['main']['items'][$menuIndex + 1])) {
				++$menuIndex;
			}
			$menu['main']['items'][$menuIndex] = array(
				'label' => 'Import from WP Layouts',
				'type' => 'event',
				'eventName' => 'AGSLayoutsImport'
			);
			$menu['main']['items'][$menuIndex + 1] = array(
				'label' => 'Save to WP Layouts',
				'type' => 'event',
				'eventName' => 'AGSLayoutsSavePage'
			);
		}
		return $menu;
	}
	
	public static function filterBuilderTopBarButtons($buttons) {
		$newButtons = array();
		foreach ($buttons as $buttonId => $button) {
			if ($buttonId == 'content-panel') {
				$newButtons['ags-layouts-import'] = array(
					'label' => 'Add from WP Layouts',
					'onclick' => 'FLBuilder.triggerHook(\'AGSLayoutsImport\');'
				);
			}
			$newButtons[$buttonId] = $button;
		}
		return $newButtons;
	}
	
	static function setupPreviewPost($previewPostId, $layoutContents) {
		self::insertLayout($layoutContents, $previewPostId, 'replace');
		$previewUrl = get_permalink($previewPostId);
		$previewUrl .= (strpos($previewUrl, '?') === false ? '?' : '&').'fl_builder';
		return $previewUrl;
	}
	
}
AGSLayoutsBB::setup();