<?php
AGSLayouts::VERSION; // Access control

class AGSLayoutsLayoutUpdater {
	private static $curl;
	
	public static function run() {
		if (empty($_POST['ags_layouts_data']['layoutId'])
				|| !is_numeric($_POST['ags_layouts_data']['layoutId'])) {
			return;
		}
		
		$allowedFields = array(
			'layoutId' => true,
			'layoutName' => true
		);
		
		$request = array_intersect_key($_POST['ags_layouts_data'], $allowedFields);
		$request['action'] = 'ags_layouts_update_layout';
		include_once(__DIR__.'/account.php');
		$request['_ags_layouts_token'] = AGSLayoutsAccount::getToken();
		$request['_ags_layouts_site'] = get_option('siteurl');
		
		
		self::$curl = curl_init();
		curl_setopt_array(self::$curl, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $request,
			CURLOPT_URL => AGSLayouts::API_URL
		));
		
		$response = @curl_exec(self::$curl);
		$response = @json_decode($response, true);
		if (empty($response['success'])) {
			wp_send_json_error();
		} else {
			wp_send_json_success();
		}
		
	}
	
}
AGSLayoutsLayoutUpdater::run();