<?php
AGSLayouts::VERSION; // Access control

class AGSLayoutsDownloader {
	private static $curl;
	
	public static function run() {
		//header('Content-Type: text/javascript');
		$importLocations = array('return', 'replace', 'above', 'below');
		if (empty($_GET['layoutId']) || !is_numeric($_GET['layoutId'])
				|| empty($_GET['postId']) || !is_numeric($_GET['postId'])
				|| empty($_GET['importLocation']) || !in_array($_GET['importLocation'], $importLocations)) {
			return;
		}
		$layoutId = $_GET['layoutId'];
		$postId = $_GET['postId'];
		
		AGSLayouts::requireEditPermission($postId);
		
		self::$curl = curl_init();
		curl_setopt_array(self::$curl, array(
			CURLOPT_RETURNTRANSFER => true
		));
		
		if (empty($_GET['newJob'])) {
			$job = @unserialize(@base64_decode(get_post_meta($postId, '_ags_layouts_job', true)));
		}
		if (
			empty($job)
			|| time() - $job['lastUpdateTime'] > 300
			|| $job['layoutId'] != $layoutId
		) {
			$job = array(
				'layoutId' => $layoutId,
				'postId' => $postId
			);
			
			self::nextTask($job, 'getLayout', null, 'Downloading layout information...', 0);
		} else {
			$job['postId'] = $postId;
		}
		
		switch($job['nextTask']['task']) {
			case 'getLayout':
				$layout = self::getLayout($layoutId);
				if (empty($layout)) {
					return;
				}
				
				$job['layout'] = $layout;
				if (empty($layout['images'])) {
					self::nextTask($job, 'insertLayout', null, 'Processing layout...', 1 / 2);
				} else {
					$imageCount = count($layout['images']);
					self::nextTask($job, 'getLayoutImage', array('imageIndex' => 0), 'Downloading image 1 of '.$imageCount.'...', (1 / ($imageCount + 2)));
				}
				break;
			case 'getLayoutImage':
				$imageCount = count($job['layout']['images']);
				if (!isset($job['nextTask']['params']['imageIndex'])) {
					return;
				}
				
				$imageSuccess = false;
				$imageIndex = $job['nextTask']['params']['imageIndex'];
				if (isset($job['layout']['images'][$imageIndex])) {
					$imageData = self::getLayoutImage($layoutId, $postId, $job['layout']['images'][$imageIndex]['file'], $job['layout']['images'][$imageIndex]['name']);
					if (!empty($imageData)) {
						$job['layout']['images'][$imageIndex] = array_merge($job['layout']['images'][$imageIndex], $imageData);
						$imageSuccess = true;
					}
				}
				
				if (!$imageSuccess) {
					self::logWarning($job, 'One or more image(s) may not have been imported correctly. Please check the import to confirm.');
				}
				
				++$imageIndex;
				if ($imageIndex == $imageCount) {
					self::nextTask($job, 'insertLayout', null, 'Processing layout...', (($imageCount + 1) / ($imageCount + 2)));
				} else {
					self::nextTask($job, 'getLayoutImage', array('imageIndex' => $imageIndex), 'Downloading image '.($imageIndex + 1).' of '.$imageCount.'...', (($imageIndex + 2) / ($imageCount + 2)));
				}
				
				break;
			case 'insertLayout':
				if (empty($job['layout']['images'])) {
					$layoutContents = $job['layout']['contents'];
				} else {
					$insertions = array();
					foreach ($job['layout']['images'] as $i => $image) {
						foreach ($image['indexes'] as $index => $field) {
							$insertions[$index] = $image['local_'.$field];
						}
					}
					
					$origLayoutContents = $job['layout']['contents'];
				
					ksort($insertions);
					$lastIndex = 0;
					$layoutContents = '';
					foreach ($insertions as $index => $insertion) {
						if ($index != $lastIndex) {
							$layoutContents .= substr($origLayoutContents, $lastIndex, $index - $lastIndex);
						}
						$layoutContents .= $insertion;
						$lastIndex = $index;
					}
					$layoutContents .= substr($origLayoutContents, $lastIndex);
				}
				
				if ($_GET['importLocation'] != 'return') {
				
					$extraData = array();
					
					switch($_GET['layoutEditor']) {
						case 'beaverbuilder':
							AGSLayoutsBB::insertLayout($layoutContents, $postId, $_GET['importLocation']);
							break;
						case 'elementor':
							//AGSLayoutsBB::insertLayout($layoutContents, $postId, $_GET['importLocation']);
							break;
						case 'gutenberg':
							//AGSLayoutsBB::insertLayout($layoutContents, $postId, $_GET['importLocation']);
							$extraData['layoutContents'] = $layoutContents;
							break;
						case 'divi':
							$layoutContents = AGSLayoutsDivi::preInsertProcess($layoutContents);
							if (isset($job['layout']['extraData'])) {
								AGSLayoutsDivi::importExtraData($postId, $_GET['importLocation'], $job['layout']['extraData']);
							}
							// Do not break
						default:
							$post = get_post($postId);
							if (empty($post)) {
								return;
							}
							
							switch($_GET['importLocation']) {
								case 'replace':
									$post->post_content = $layoutContents;
									break;
								case 'above':
									$post->post_content = $layoutContents.$post->post_content;
									break;
								case 'below':
									$post->post_content .= $layoutContents;
									break;
							}
						
							wp_update_post($post);
					}
				
					
				} else {
					$extraData = array('layoutContents' => $layoutContents);
				}
				
				self::nextTask($job, 'done', null, 'Done!', 1, $extraData);
				break;
			case 'done':
				delete_post_meta($postId, '_ags_layouts_job');
		}
		
		
	}
	
	public static function getLayout($layoutId) {
		include_once(__DIR__.'/account.php');
		curl_setopt(self::$curl, CURLOPT_URL, AGSLayouts::API_URL.'?action=ags_layouts_get_layout&_ags_layouts_token='.urlencode(AGSLayoutsAccount::getToken()).'&_ags_layouts_site='.urlencode(get_option('siteurl')).'&layoutId='.$layoutId);
		$response = curl_exec(self::$curl);
		if (!empty($response)) {
			$response = json_decode($response, true);
			if (!empty($response['success']) && !empty($response['data']['contents'])) {
				return $response['data'];
			}
		}
		return false;
	}
	
	public static function getLayoutImage($layoutId, $postId, $imageFile, $imageName) {
		include_once(__DIR__.'/account.php');
		curl_setopt(self::$curl, CURLOPT_URL, AGSLayouts::API_URL.'?action=ags_layouts_get_layout_image&_ags_layouts_token='.urlencode(AGSLayoutsAccount::getToken()).'&_ags_layouts_site='.urlencode(get_option('siteurl')).'&layoutId='.$layoutId.'&imageFile='.urlencode($imageFile));
		$response = curl_exec(self::$curl);
		if (!empty($response)) {
			$downloadedFile = array(
				'tmp_name' => wp_tempnam(),
				'name' => $imageName
			);
			file_put_contents($downloadedFile['tmp_name'], $response);
			$imageId = media_handle_sideload($downloadedFile, $postId);
			@unlink($downloadedFile['tmp_name']);
			if (!empty($imageId)) {
				return array(
					'local_id' => $imageId,
					'local_url' => wp_get_attachment_url($imageId)
				);
			}
		}
		return false;
	}
	
	public static function nextTask($job, $nextTask, $taskParams, $statusText, $progress, $extraData=array()) {
		$job['nextTask'] = array('task' => $nextTask);
		if (!empty($taskParams)) {
			$job['nextTask']['params'] = $taskParams;
		}
		$job['lastUpdateTime'] = time();
		
		update_post_meta($job['postId'], '_ags_layouts_job', base64_encode(serialize($job)));
		$output = array(
			'status' => $statusText,
			'progress' => $progress
		);
		if ($nextTask == 'done') {
			$output['done'] = true;
			if (!empty($job['warnings'])) {
				$output['warnings'] = array_unique($job['warnings']);
			}
		}
		
		$output = array_merge($extraData, $output);
		
		wp_send_json_success($output);
	}
	
	static function logWarning(&$job, $warningMessage) {
		if (empty($job['warnings'])) {
			$job['warnings'] = array();
		}
		$job['warnings'][] = $warningMessage;
	}
}
AGSLayoutsDownloader::run();