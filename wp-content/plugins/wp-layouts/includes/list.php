<?php
AGSLayouts::VERSION; // Access control

class AGSLayoutsList {
	private static $curl;
	
	public static function run() {
		
		$output = array();
		include_once(__DIR__.'/account.php');
		$token = AGSLayoutsAccount::getToken();
		
		if ($token) {
			$request = array(
				'action' => 'ags_layouts_list_layouts',
				'_ags_layouts_token' => $token,
				'_ags_layouts_site' => get_option('siteurl'),
			);
			
			if (!empty($_GET['ags_layouts_editor'])) {
				$request['ags_layouts_editor'] = $_GET['ags_layouts_editor'];
			}
			
			if (isset($_GET['ags_layouts_collection'])) {
				$request['ags_layouts_collection'] = $_GET['ags_layouts_collection'];
			}
			
			if (!empty($_GET['search']['value'])) {
				$request['query'] = $_GET['search']['value'];
			}
			
			if (isset($_GET['order'][0]['column']) && !empty($_GET['columns'][$_GET['order'][0]['column']]['data'])) {
				$request['sortBy'] = $_GET['columns'][$_GET['order'][0]['column']]['data'];
				$request['sortOrder'] = isset($_GET['order'][0]['dir']) && $_GET['order'][0]['dir'] == 'desc' ? 'D' : 'A';
			}
			
			if (isset($_GET['start'])) {
				$request['offset'] = $_GET['start'];
			}
			
			if (isset($_GET['length'])) {
				$request['limit'] = $_GET['length'];
			}
			
			self::$curl = curl_init(AGSLayouts::API_URL);
			curl_setopt_array(self::$curl, array(
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $request,
			));
			$response = @curl_exec(self::$curl);
			$response = @json_decode($response, true);
			
			if (isset($_GET['draw'])) {
				$output['draw'] = $_GET['draw'];
			}
			
			if (empty($response['success']) || empty($response['data'])) {
				$errorCode = isset($response['data']['error']) ? $response['data']['error'] : '';
				switch ($errorCode) {
					case 'auth':
						$output['message'] = 'Your request could not be authenticated. Please try logging out and back in under WP Layouts > Settings, and contact support if this problem persists.';
						break;
					case 'noCollectionsAccess':
						$output['message'] = 'NoCollectionsAccess';
						break;
					default:
						$output['message'] = '';
				}
				$output['recordsTotal'] = 0;
				$output['recordsFiltered'] = 0;
			} else if (!empty($_GET['ags_layouts_collection']) && $_GET['ags_layouts_collection'] != -1) {
				$output['collection'] = $response['data']['collection'];
				$output['layouts'] = $response['data']['layouts'];
			} else {
				$output['recordsTotal'] = $response['data']['stats']['all'];
				$output['recordsFiltered'] = isset($response['data']['stats']['query']) ? $response['data']['stats']['query'] : $output['recordsTotal'];
				$output['data'] = $response['data']['layouts'];
			
			}
		} else {
			$output['message'] = 'You are currently not logged in. Please log in under WP Layouts > Settings and try again.';
			$output['recordsTotal'] = 0;
			$output['recordsFiltered'] = 0;
		}
		
		echo(json_encode($output));
	}
	
}
AGSLayoutsList::run();