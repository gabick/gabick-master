<?php
class AGSLayoutsUploader {
	private static $curl;
	
	public static function run() {
		/*if (empty($_POST['postId']) || !is_numeric($_POST['postId'])) {
			return;
		}*/
		
		include_once(__DIR__.'/account.php');
		$apiData = array(
			'action' => 'ags_layouts_store',
			'_ags_layouts_token' => AGSLayoutsAccount::getToken(),
			'_ags_layouts_site' => get_option('siteurl'),
		);
		
		if (empty($_POST['jobState'])) {
			if (empty($_POST['postContent']) || empty($_POST['layoutEditor']) || empty($_POST['layoutName'])) {
				return;
			}
			
			$apiData['layoutEditor'] = $_POST['layoutEditor'];
			if (!empty($_POST['screenshotData'])) {
				$apiData['screenshotData'] = $_POST['screenshotData'];
			}
			
			$apiData['layoutContents'] = stripslashes($_POST['postContent']);
			
			switch ($_POST['layoutEditor']) {
				case 'divi':
					// This is very similar to how Divi does it but developed independently :)
					$contents = json_decode($apiData['layoutContents'], true);
					if (empty($contents)) {
						return;
					}
					unset($contents[0]['attrs']['template_type']);
					$apiData['layoutContents'] = et_fb_process_to_shortcode($contents);
					
					if (!empty($_POST['extraData'])) {
						foreach (AGSLayoutsDivi::processExtraData($_POST['extraData']) as $extraDataField => $extraDataContents) {
							$apiData['extraData['.$extraDataField.']'] = $extraDataContents;
						}
					}
					break;
				case 'beaverbuilder':
					$apiData['layoutContents'] = AGSLayoutsBB::preUploadProcess($apiData['layoutContents']);
					break;
				case 'elementor':
					$apiData['layoutContents'] = AGSLayoutsElementor::preUploadProcess($apiData['layoutContents']);
					break;
			}
			
			$apiData['layoutName'] = stripslashes($_POST['layoutName']);
			
			$uploadsDirectoryInfo = wp_upload_dir();
			if (empty($uploadsDirectoryInfo['baseurl'])) {
				return;
			}
			$apiData['imagesUrl'] = $uploadsDirectoryInfo['baseurl'];
		} else if (!isset($_POST['jobState']['layoutId'])) {
			return;
		} else {
			$apiData['jobState[layoutId]'] = $_POST['jobState']['layoutId'];
		}
		
		self::$curl = curl_init(AGSLayouts::API_URL);
		curl_setopt_array(self::$curl, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $apiData
		));
		
		$response = curl_exec(self::$curl);
		$response = @json_decode($response, true);
		
		if (empty($response['success']) || empty($response['data'])) {
			if (isset($response['data']['error'])) {
				$errorResponse = array('error' => $response['data']['error']);
				if (isset($response['data']['errorParams'])) {
					$errorResponse['errorParams'] = $response['data']['errorParams'];
				}
				wp_send_json_error($errorResponse);
			}
			return;
		}
		
		if (!empty($response['data']['done'])) {
			wp_send_json_success(array('done' => true));
		}
		
		$output = array(
			'jobState' => empty($response['data']['jobState']) ? array() : $response['data']['jobState']
		);
		if (!empty($response['data']['status'])) {
			$output['status'] = $response['data']['status'];
		}
		if (!empty($response['data']['progress'])) {
			$output['progress'] = $response['data']['progress'];
		}
		
		wp_send_json_success($output);
	}
	
}
AGSLayoutsUploader::run();