<?php AGSLayouts::VERSION; // Access control ?>
<br><p class="ags-layouts-notification ags-layouts-notification-info"><strong>Thank you for being part of the WP Layouts Beta!</strong> We appreciate your patience if you encounter any problems with this product. Please visit our <a href="https://support.wplayouts.space/" target="_blank">support site</a> for tutorials, FAQs, and to contact us.</p>

<h1>My Layouts</h1>
<div id="ags-layouts-container">
	<div id="ags-layouts-list-container"></div>
	<div id="ags-layouts-details" class="ags-layouts-details-none">
		<form>
			<div id="ags-layouts-details-image"></div>
			<label>
				Layout Name:
				<input id="ags-layouts-details-name" required />
			</label>
			
			<div id="ags-layouts-details-buttons">
				<button id="ags-layouts-details-save" class="aspengrove-btn-primary" disabled>Save</button>
				<button type="button" id="ags-layouts-details-delete" class="aspengrove-btn-secondary" disabled>Delete</button>
			</div>
		</form>
	</div>
	<div id="ags-layouts-loader-overlay">
		<div id="ags-layouts-loader"></div>
	</div>
</div>

<script>
jQuery(document).ready(function($) {
	var $listContainer = $('#ags-layouts-list-container');
	var $layoutDetails = $('#ags-layouts-details');
	var $layoutDetailsImage = $('#ags-layouts-details-image');
	var $layoutDetailsName = $('#ags-layouts-details-name');
	var $loaderOverlay = $('#ags-layouts-loader-overlay');
	
	var layoutSelectHandler = function(layout, $tabContent, $tableContainer) {
		if (layout) {
			$('#ags-layouts-details-buttons > button').attr('disabled', false);
			$layoutDetails.data('ags-layout', layout)
				.removeClass('ags-layouts-details-none');
			if (layout.hasLayoutImage) {
				$layoutDetailsImage.css('background-image', 'url(\'' + ags_layouts_api_url + '?action=ags_layouts_get_image&image=L&layoutId=' + layout.layoutId + '\')');
			} else {
				$layoutDetailsImage.addClass('ags-layouts-no-image');
			}
			$layoutDetailsName.val(layout.layoutName);
		} else {
			$('#ags-layouts-details-buttons > button').attr('disabled', true);
			$layoutDetails
				.data('ags-layout', null)
				.addClass('ags-layouts-details-none');
			$layoutDetailsImage.css('background-image', '').removeClass('ags-layouts-no-image');
			$layoutDetailsName.val('');
		}
	};
	
	var listUi = ags_layouts_list_ui(-1, null, null, layoutSelectHandler, function() {
		$loaderOverlay.show();
	}, function() {
		$loaderOverlay.hide();
	});
	$listContainer.append(listUi[0]);
	listUi[1]($listContainer);
	var dataTable = listUi[2]();
	
	$layoutDetails.children('form:first').submit(function() {
		var $form = $(this),
			layout = $form.parent().data('ags-layout'),
			newLayoutName = $layoutDetailsName.val(),
			errorHandler = function() {
				ags_layouts_message_dialog(
					'Error',
					'Something went wrong while updating the layout. Please try again if your changes were not saved.',
					'O'
				);
			};
		
		if (layout && newLayoutName) {
			$loaderOverlay.show();
			$.post(ags_layouts_api_url, {
				action: 'ags_layouts_update',
				ags_layouts_data: {
					layoutId: layout.layoutId,
					layoutName: newLayoutName
				}
			}, function(response) {
				if (!response.success) {
					errorHandler();
				}
			}, 'json')
			.fail(errorHandler)
			.always(function(response) {
				layoutSelectHandler();
				dataTable.ajax.reload(null, false);
			});
		
		}
		
		return false;
	});
	
	$('#ags-layouts-details-delete').click(function() {
		var layout = $('#ags-layouts-details').data('ags-layout'),
			errorHandler = function() {
				ags_layouts_message_dialog(
					'Error',
					'Something went wrong while deleting the layout.',
					'O'
				);
			};
		
		if (layout) {
			ags_layouts_message_dialog(
				'Are you sure?',
				'Are you sure that you want to delete the layout "' + layout.layoutName + '"?',
				'YN',
				function() {
					$loaderOverlay.show();
					$.post(ags_layouts_api_url, {
						action: 'ags_layouts_delete',
						layoutId: layout.layoutId
					}, function(response) {
						if (!response.success) {
							errorHandler();
						}
					}, 'json')
					.fail(errorHandler)
					.always(function(response) {
						layoutSelectHandler();
						dataTable.ajax.reload(null, false);
					});
				}
			);
		}
	});
	
	
});
</script>