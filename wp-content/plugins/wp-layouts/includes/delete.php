<?php
AGSLayouts::VERSION; // Access control

class AGSLayoutsDeleter {
	private static $curl;
	
	public static function run() {
		if (empty($_POST['layoutId']) || !is_numeric($_POST['layoutId'])) {
			return;
		}
		
		include_once(__DIR__.'/account.php');
		
		$request = array(
			'action' => 'ags_layouts_delete_layout',
			'_ags_layouts_token' => AGSLayoutsAccount::getToken(),
			'_ags_layouts_site' => get_option('siteurl'),
			'layoutId' => $_POST['layoutId'],
		);
		
		self::$curl = curl_init();
		curl_setopt_array(self::$curl, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $request,
			CURLOPT_URL => AGSLayouts::API_URL
		));
		
		$response = @curl_exec(self::$curl);
		$response = @json_decode($response, true);
		if (empty($response['success'])) {
			wp_send_json_error();
		} else {
			wp_send_json_success();
		}
		
	}
	
}
AGSLayoutsDeleter::run();