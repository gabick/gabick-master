<?php
AGSLayouts::VERSION; // Access control

class AGSLayoutsPreviewer {
	private static $curl, $previewPostId, $allPreviewPostIds, $disablePostsFilter;
	
	public static function handlePreviewRequest() {
		self::$disablePostsFilter = true;
		
		// Check for elementor post GET variable, if it is set then call onInit directly with a post override
		if (empty($_GET['post'])) {
			add_action( 'wp', array('AGSLayoutsPreviewer', 'onInit') );
		} else {
			self::onInit($_GET['post']);
		}
		
	}
	
	static function onInit($currentPostId=null) {
		self::$previewPostId = self::getPreviewPageId();
		if (!self::$previewPostId) {
			self::$previewPostId = self::createPreviewPage();
		}
		
		if (!is_numeric( $currentPostId )) {
			global $post;
			if (isset($post) && isset($post->ID)) {
				$currentPostId = $post->ID;
			}
		}
		
		if ($currentPostId && $currentPostId == self::$previewPostId) {
			if (
				empty($_GET['previewKey'])
				|| $_GET['previewKey'] != get_post_meta(self::$previewPostId, '_ags_layouts_preview_key', true)
			) {
				wp_die('This layout preview is no longer available.');
			}
			
			add_filter('the_title', array('AGSLayoutsPreviewer', 'filterPostTitle'), 1, 2);
			
			if (!isset($_GET['elementor-preview'])) {
				add_action('wp_footer', array('AGSLayoutsPreviewer', 'outputPreviewNotice'));
			}
			
			// Required for Elementor:
			add_action('admin_print_footer_scripts', array('AGSLayoutsPreviewer', 'outputPreviewNotice'));
			
			add_filter('body_class', array('AGSLayoutsPreviewer', 'addPreviewBodyClasses'));
			add_filter('admin_body_class', array('AGSLayoutsPreviewer', 'addPreviewBodyClasses'));
			add_action('admin_print_footer_scripts', array('AGSLayoutsPreviewer', 'addPreviewBodyClassesScript'));
		} else {
			self::$curl = curl_init();
			curl_setopt_array(self::$curl, array(
				CURLOPT_RETURNTRANSFER => true
			));
			
			$layout = self::getLayout($_GET['ags_layouts_preview']);
			if (empty($layout)) {
				wp_die('The layout could not be retrieved.');
			}
			
			if (empty($layout['images'])) {
				$layoutContents = $layout['contents'];
			} else {
				$insertions = array();
				foreach ($layout['images'] as $i => $image) {
					foreach ($image['indexes'] as $index => $field) {
						$insertions[$index] = admin_url( 'admin-ajax.php?action=ags_layouts_get_image&layoutId='.$_GET['ags_layouts_preview'].'&image='.$image['file'] );
					}
				}
				
				$origLayoutContents = $layout['contents'];
			
				ksort($insertions);
				$lastIndex = 0;
				$layoutContents = '';
				foreach ($insertions as $index => $insertion) {
					if ($index != $lastIndex) {
						$layoutContents .= substr($origLayoutContents, $lastIndex, $index - $lastIndex);
					}
					$layoutContents .= $insertion;
					$lastIndex = $index;
				}
				$layoutContents .= substr($origLayoutContents, $lastIndex);
			}
			
			update_post_meta(self::$previewPostId, '_ags_layouts_preview_name', $layout['name']);
			update_post_meta(self::$previewPostId, '_ags_layouts_preview_editor', $_GET['layoutEditor']);
			$previewKey = wp_generate_password(20, false);
			update_post_meta(self::$previewPostId, '_ags_layouts_preview_key', $previewKey);
			
			$previewUrl = get_permalink(self::$previewPostId);
			
			switch($_GET['layoutEditor']) {
				case 'beaverbuilder':
					if (!class_exists('AGSLayoutsBB')) {
						wp_die('The layout "'.$layout['name'].'" was created with Beaver Builder. Please enable Beaver Builder to view this preview.');
					}
					$previewUrl = AGSLayoutsBB::setupPreviewPost(self::$previewPostId, $layoutContents);
					$skipUpdatePost = true;
					break;
				case 'elementor':
					if (!class_exists('AGSLayoutsElementor')) {
						wp_die('The layout "'.$layout['name'].'" was created with Elementor. Please enable Elementor to view this preview.');
					}
					$previewUrl = AGSLayoutsElementor::setupPreviewPost(self::$previewPostId, $layoutContents);
					$skipUpdatePost = true;
					break;
				case 'divi':
					if (!class_exists('AGSLayoutsDivi')) {
						wp_die('The layout "'.$layout['name'].'" was created with Divi. Please enable Divi to view this preview.');
					}
					$previewUrl = AGSLayoutsDivi::setupPreviewPost(self::$previewPostId, $layoutContents);
					break;
				case 'gutenberg':
					if (!class_exists('AGSLayoutsGutenberg')) {
						wp_die('The layout "'.$layout['name'].'" was created with the WordPress block editor (Gutenberg). Please enable the WordPress block editor to view this preview.');
					}
					$previewUrl = AGSLayoutsGutenberg::setupPreviewPost(self::$previewPostId, $layoutContents);
					break;
			}
			
			if (empty($skipUpdatePost)) {
				$post = get_post(self::$previewPostId);
				if (empty($post)) {
					return;
				}
				$post->post_content = $layoutContents;
				
				add_filter('wp_revisions_to_keep', array('AGSLayoutsPreviewer', 'disableRevisions'));
				wp_update_post($post);
				remove_filter('wp_revisions_to_keep', array('AGSLayoutsPreviewer', 'disableRevisions'));
			}
			
			$previewUrl .= (strpos($previewUrl, '?') === false ? '?' : '&').'ags_layouts_preview='.$_GET['ags_layouts_preview'].'&previewKey='.$previewKey;
			wp_redirect($previewUrl);
		}
		
	}
	
	static function getPreviewPageId($previewPageUserId=null, $multiple = false) {
		self::$disablePostsFilter = true;
		if (!$previewPageUserId) {
			$previewPageUserId = get_current_user_id();
		}
		$previewPageIds = get_posts(array(
			'author' => $previewPageUserId,
			'post_type' => 'page',
			'post_status' => 'private',
			'meta_key' => '_ags_layouts_preview_page',
			'posts_per_page' => 1,
			'fields' => 'ids'
		));
		self::$disablePostsFilter = false;
		return $multiple ? $previewPageIds : ( empty($previewPageIds) ? null : $previewPageIds[0] );
	}
	
	static function getAllPreviewPageIds() {
		self::$disablePostsFilter = true;
		$previewPageIds = get_posts(array(
			'post_type' => 'page',
			'post_status' => 'private',
			'meta_key' => '_ags_layouts_preview_page',
			'nopaging' => true,
			'fields' => 'ids'
		));
		self::$disablePostsFilter = false;
		return $previewPageIds;
	}
	
	static function createPreviewPage() {
		$previewPostId = wp_insert_post(array(
			'post_title' => 'WP Layouts Preview',
			'post_type' => 'page',
			'post_status' => 'private'
		));
		
		if ($previewPostId && is_numeric($previewPostId)) {
			update_post_meta($previewPostId, '_ags_layouts_preview_page', 1);
			return $previewPostId;
		}
	}
	
	static function outputPreviewNotice() {
		$layoutName = get_post_meta(self::$previewPostId, '_ags_layouts_preview_name', true);
		echo('<div class="ags_layouts_preview_notice">This is a preview of the layout <strong>'.esc_html($layoutName).'</strong>. Changes made here will not be saved in the layout.</div>');
	}
	
	static function filterPostsQuery($query) {
		if (empty(self::$disablePostsFilter)) {
			if (!isset(self::$allPreviewPostIds)) {
				self::$allPreviewPostIds = self::getAllPreviewPageIds();
			}
		
			if (isset($query->query_vars['post__not_in']) && is_array($query->query_vars['post__not_in'])) {
				$query->query_vars['post__not_in'] = array_merge($query->query_vars['post__not_in'], self::$allPreviewPostIds);
			} else {
				$query->query_vars['post__not_in'] = self::$allPreviewPostIds;
			}
		}
	}
	
	static function onUserDeleted($deletedUserId) {
		foreach (self::getPreviewPageId($deletedUserId, true) as $previewPostId) {
			wp_delete_post($previewPostId, true);
		}
	}
	
	// Hooked in main plugin file
	static function onPluginDeactivate() {
		foreach (self::getAllPreviewPageIds() as $previewPostId) {
			wp_delete_post($previewPostId, true);
		}
	}
	
	static function disableRevisions() {
		return 0;
	}
	
	static function isLayoutPreview() {
		self::$previewPostId = self::getPreviewPageId();
		if (self::$previewPostId) {
			global $post;
			if (isset($post) && isset($post->ID)) {
				$currentPostId = $post->ID;
			}
			
			if ($currentPostId && $currentPostId == self::$previewPostId) {
				return true;
			}
		}
		return false;
	}
	
	static function addPreviewBodyClasses($currentClasses) {
		$editor = get_post_meta(self::$previewPostId, '_ags_layouts_preview_editor', true);
		$editorClass = 'ags-layouts-preview-'.($editor ? esc_attr($editor) : 'unknown-editor');
		return
			is_array($currentClasses)
				? array_merge($currentClasses, array('ags-layouts-preview', $editorClass))
				: $currentClasses.' ags-layouts-preview '.$editorClass;
	}
	
	static function addPreviewBodyClassesScript() {
		echo('<script>document.body.className += \' '.implode(' ', self::addPreviewBodyClasses(array())).'\';</script>');
	}
	
	public static function filterPostTitle($currentTitle, $postId) {
		return $postId == self::$previewPostId ? 'Layout Preview' : $currentTitle;
	}
	
	public static function getLayout($layoutId) {
		include_once(__DIR__.'/account.php');
		curl_setopt(self::$curl, CURLOPT_URL, AGSLayouts::API_URL.'?action=ags_layouts_get_layout&_ags_layouts_token='.urlencode(AGSLayoutsAccount::getToken()).'&_ags_layouts_site='.urlencode(get_option('siteurl')).'&layoutId='.$layoutId);
		$response = curl_exec(self::$curl);
		if (!empty($response)) {
			$response = json_decode($response, true);
			if (!empty($response['success']) && !empty($response['data']['contents'])) {
				return $response['data'];
			}
		}
		return false;
	}
	
}
		
add_action('pre_get_posts', array('AGSLayoutsPreviewer', 'filterPostsQuery'));
add_action('deleted_user', array('AGSLayoutsPreviewer', 'onUserDeleted'));
add_action('remove_user_from_blog', array('AGSLayoutsPreviewer', 'onUserDeleted'));