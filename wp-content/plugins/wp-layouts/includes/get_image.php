<?php
/*
This file includes code based on parts of WordPress by Automattic, released under
GPLv2+, licensed under GPLv3+ (see wp-license.txt in the ../license directory for the
copyright, license, and additional credits applicable to WordPress, and the
license.txt file in the ../license directory for GPLv3 text).
*/

AGSLayouts::VERSION; // Access control

class AGSLayoutsGetImage {
	private static $curl;
	
	public static function run() {
		if (empty($_GET['layoutId']) || !is_numeric($_GET['layoutId']) || empty($_GET['image'])) {
			return;
		}
		include_once(__DIR__.'/account.php');
		self::$curl = curl_init(AGSLayouts::API_URL
								.'?action=ags_layouts_get_layout_image&_ags_layouts_token='.urlencode(AGSLayoutsAccount::getToken())
								.'&_ags_layouts_site='.urlencode(get_option('siteurl'))
								.'&layoutId='.$_GET['layoutId']
								.'&imageFile='.urlencode($_GET['image']));
		
		$isLayoutImage = $_GET['image'] == 'L';
		
		// The following code is copied from WordPress - see information near the top of this file - modified by Jonathan Hall 2019-09-09
		// Source: https://github.com/WordPress/WordPress/blob/master/wp-admin/load-scripts.php
		
		$protocol = $_SERVER['SERVER_PROTOCOL'];
		if ( ! in_array( $protocol, array( 'HTTP/1.1', 'HTTP/2', 'HTTP/2.0' ) ) ) {
			$protocol = 'HTTP/1.0';
		}
		$expires_offset = 31536000; // 1 year
		if ( isset( $_SERVER['HTTP_IF_NONE_MATCH'] ) ) {
			header( "$protocol 304 Not Modified" );
			exit();
		}
		header( 'Etag: 1' );
		if ($isLayoutImage) {
			header( 'Content-Type: image/jpeg' );
		}
		header( 'Expires: ' . gmdate( 'D, d M Y H:i:s', time() + $expires_offset ) . ' GMT' );
		header( "Cache-Control: public, max-age=$expires_offset" );
		
		// End code copied from WordPress
		
		curl_exec(self::$curl);
		
	}
}
AGSLayoutsGetImage::run();