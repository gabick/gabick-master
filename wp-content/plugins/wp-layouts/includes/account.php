<?php
AGSLayouts::VERSION; // Access control

class AGSLayoutsAccount {
	private static $curl, $lastLoginError;
	
	public static function login($email, $password) {
		self::$curl = curl_init(AGSLayouts::API_URL);
		curl_setopt_array(self::$curl, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array(
				'action' => 'ags_layouts_get_auth_token',
				'authTokenEmail' => $email,
				'authTokenPassword' => $password,
				'authTokenSite' => get_option('siteurl')
			),
		));
		
		$loginResult = @curl_exec(self::$curl);
		$loginResult = @json_decode($loginResult, true);
		
		if (!empty($loginResult['success']) && !empty($loginResult['data']['token'])) {
			return update_option(
				'ags_layouts_auth',
				array(
					'email' => $email,
					'token' => $loginResult['data']['token']
				),
				false
			);
		}
		
		self::$lastLoginError = empty($loginResult['success']) && !empty($loginResult['data']) ? $loginResult['data'] : '';
		
		return false;
	}
	
	static function getLastLoginError() {
		return self::$lastLoginError;
	}
	
	public static function getToken() {
		$auth = get_option('ags_layouts_auth');
		if (!empty($auth)) {
			return $auth['token'];
		}
	}
	public static function getAccountEmail() {
		$auth = get_option('ags_layouts_auth');
		if (!empty($auth)) {
			return $auth['email'];
		}
	}
	
	public static function isLoggedIn() {
		$token = self::getToken();
		return !empty($token);
	}
	
	public static function logout() {
		self::$curl = curl_init(AGSLayouts::API_URL);
		curl_setopt_array(self::$curl, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array(
				'action' => 'ags_layouts_cancel_auth_token',
				'authToken' => self::getToken(),
			),
		));
		
		$logoutResult = @curl_exec(self::$curl);
		$logoutResult = @json_decode($logoutResult, true);
	
		return delete_option('ags_layouts_auth') && !empty($logoutResult['success']);
	}
}