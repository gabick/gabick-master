<?php
/*
Plugin Name: WP Layouts
Plugin URI: https://wplayouts.space/
Description: Save, store and import layouts instantly, all in ONE place with the click of a button!
Version: 0.3.0
Author: WP Layouts
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html
*/

/*
Despite the following, this project is licensed exclusively
under GPLv3 (no future versions).
This statement modifies the following text.

WP Layouts plugin
Copyright (C) 2020  Aspen Grove Studios

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

========

Credits:

This plugin includes code based on parts of WordPress and the Gutenberg plugin
by Automattic, released under GPLv2+, licensed under GPLv3+ (see wp-license.txt
in the license directory for the copyright, license, and additional credits applicable
to WordPress and Gutenberg, and the license.txt file in the license directory
for GPLv3 text).

This plugin includes code based on parts of the Divi theme and/or the
Divi Builder, copyright Elegant Themes, released under GPLv2, licensed under
GPLv3 for this project by special permission (see divi-CREDITS.md in the license
directory for credits applicable to Divi, and license.txt file in the license
directory for GPLv3 text).

This plugin includes code based on parts of Beaver Builder Plugin (Standard Version)
and/or Beaver Builder Plugin (Lite Version), copyright 2014 Beaver Builder, released
under GPLv2+ (according to the plugin's readme.txt file; fl-builder.php specifies
different licensing but we are using the licensing specified in readme.txt),
licensed under GPLv3+ (see license.txt file in the license directory for GPLv3 text).

This plugin contains code based on and/or copied from the Elementor plugin (free version),
copyright Elementor, licensed under GNU General Public License version 3 (GPLv3) or later.
See license/license.txt for the text of GPLv3.

*/

// The following line must remain before all other code in this file so that no other code
// is executed if WordPress is not loaded
register_deactivation_hook(__FILE__, array('AGSLayoutsPreviewer', 'onPluginDeactivate'));

class AGSLayouts {

	const VERSION = '0.3.0';
	const API_URL = 'https://ls.wplayouts.space/wp-admin/admin-ajax.php';
	//const API_URL = 'http://localhost/dev/layoutsserver/wp-admin/admin-ajax.php';
	public static 	$pluginBaseUrl,
					$pluginDirectory,
					$pluginFile,
					$supportedEditors = array(
						'Gutenberg' => array(
							'displayName' => 'Gutenberg'
						),
						'Divi' => array(
							'displayName' => 'Divi Builder',
							'enableOnFunction' => 'ET_BUILDER_SHOULD_LOAD_FRAMEWORK',
						),
						'BeaverBuilder' => array(
							'displayName' => 'Beaver Builder',
							'enableOnClass' => 'FLBuilderLoader',
						),
						'Elementor' => array(
							'displayName' => 'Elementor',
							'enableOnClass' => 'Elementor\Plugin',
						),
					);
	private static $isDoingLayoutImage;
	
	public static function setup() {
		self::$pluginBaseUrl = plugin_dir_url(__FILE__);
		self::$pluginDirectory = __DIR__.'/';
		self::$pluginFile = __FILE__;
		
		include_once(self::$pluginDirectory.'updater/EDD_SL_Plugin_Updater.php');
		
		/* Hooks */
		add_action('admin_menu', array('AGSLayouts', 'registerAdminPage'));
		add_action('admin_enqueue_scripts', array('AGSLayouts', 'adminScripts'));
		add_action('wp_enqueue_scripts', array('AGSLayouts', 'frontendScripts'));
		add_action('wp_ajax_ags_layouts_get', array('AGSLayouts', 'getLayout'));
		add_action('wp_ajax_ags_layouts_get_image', array('AGSLayouts', 'getImage'));
		add_action('wp_ajax_ags_layouts_list', array('AGSLayouts', 'listLayouts'));
		add_action('wp_ajax_ags_layouts_export', array('AGSLayouts', 'exportLayout'));
		add_action('wp_ajax_ags_layouts_update', array('AGSLayouts', 'updateLayout'));
		add_action('wp_ajax_ags_layouts_delete', array('AGSLayouts', 'deleteLayout'));
		if (!empty($_POST['ags_layouts_ss']) && !empty($_POST['ags_layouts_ss_content']) && !empty($_POST['ags_layouts_ss_editor'])) {
			self::$isDoingLayoutImage=true;
			add_filter('the_content', array('AGSLayouts', 'filterContentForScreenshot'));
		}

		// Following filter is required here by the Divi integration
		add_filter('et_builder_load_requests', function($loadParameters) {
			if (isset($loadParameters['action'])) {
				$loadParameters['action'][] = 'ags_layouts_export';
			} else {
				$loadParameters['action'] = array('ags_layouts_export');
			}
			return $loadParameters;
		});
		
		add_action('init', array('AGSLayouts', 'loadIntegrations'),11);
		
		include_once(self::$pluginDirectory.'includes/previewer.php');
		if (!empty($_GET['ags_layouts_preview'])) {
			AGSLayoutsPreviewer::handlePreviewRequest();
		}
	}
	
	static function loadIntegrations() {
		foreach (self::$supportedEditors as $editorName => $editorDetails) {
				if (self::isIntegrationEnabled($editorName)) {
					@include(AGSLayouts::$pluginDirectory.'integrations/'.$editorName.'/'.$editorName.'.php');
				}
			}
	}
	
	static function isIntegrationEnabled($integrationName) {
		switch ($integrationName) {
			case 'Gutenberg':
				return apply_filters('use_block_editor_for_post_type', true, 'page') || apply_filters('use_block_editor_for_post_type', true, 'post');
			default:
				return !empty(self::$supportedEditors[$integrationName]['enableAlways'])
				|| (isset(self::$supportedEditors[$integrationName]['enableOnClass']) && class_exists(self::$supportedEditors[$integrationName]['enableOnClass']))
				|| (isset(self::$supportedEditors[$integrationName]['enableOnFunction']) && function_exists(self::$supportedEditors[$integrationName]['enableOnFunction']));
		}
	}
	
	public static function registerAdminPage() {
		/* Admin Pages */
		$mainPage = add_menu_page('WP Layouts', 'WP Layouts', 'edit_posts', 'ags-layouts', array('AGSLayouts', 'adminPage'), '', 650);
		add_submenu_page('ags-layouts', 'Themes', 'Themes', 'switch_themes', 'ags-layouts-themes', array('AGSLayouts', 'themesPage'));
		add_submenu_page('ags-layouts', 'Settings', 'Settings', 'edit_posts', 'ags-layouts-settings', array('AGSLayouts', 'settingsPage'));
		
	}
	
	public static function frontendScripts($forceIncludeAdminScripts=false) {
		/* Hooks */
		if ($forceIncludeAdminScripts
				|| (class_exists('AGSLayoutsDivi') && AGSLayoutsDivi::shouldLoadAdminScriptsOnFrontend())
				|| isset($_GET['fl_builder'])
		) {
			self::adminScripts(true);
		} else {
			if (!empty(self::$isDoingLayoutImage)) {
				wp_enqueue_script('jquery');
			}
			self::registerScriptDependencies();
		}
	}
	
	public static function adminPage() {
		include(self::$pluginDirectory.'includes/admin-page.php');
	}
	
	public static function themesPage() {
		include(self::$pluginDirectory.'includes/themes-page.php');
	}
	
	public static function settingsPage() {
		include(self::$pluginDirectory.'includes/settings-page.php');
	}
	
	public static function adminScripts($forFrontend=false) {
		self::registerScriptDependencies();
		wp_enqueue_style('ags-layouts-admin', self::$pluginBaseUrl.'css/admin.css', array(), self::VERSION);
		wp_enqueue_script('ags-layouts-admin', self::$pluginBaseUrl.'js/admin.js', array('jquery'), self::VERSION);
		
		$adminConfig = array(
			'wpFrontendUrl' => get_home_url(),
			'wpAjaxUrl' => admin_url( 'admin-ajax.php' ),
			'pluginBaseUrl' => self::$pluginBaseUrl,
			'editorNames' => array(
				'?' => 'Unknown',
				'gutenberg' => 'WordPress',
				'divi' => 'Divi',
				'elementor' => 'Elementor',
				'beaverbuilder' => 'Beaver Builder',
			)
		);
		$isEditingPost = ($forFrontend || (isset($GLOBALS['pagenow']) && $GLOBALS['pagenow'] == 'post.php')) && isset($GLOBALS['post']);
		if ($isEditingPost) {
			$adminConfig['editingPostId'] = $GLOBALS['post']->ID;
			$adminConfig['editingPostUrl'] = get_permalink($GLOBALS['post']);
		}
		wp_localize_script('ags-layouts-admin', 'ags_layouts_admin_config', $adminConfig);
		
		wp_enqueue_script('ags-layouts-html2canvas', self::$pluginBaseUrl.'js/html2canvas.min.js', array(), self::VERSION);
		
		wp_enqueue_style('ags-layouts-dialog', self::$pluginBaseUrl.'vendor/ags-dialog/ags-dialog.css', array(), self::VERSION);
		wp_enqueue_script('ags-layouts-dialog', self::$pluginBaseUrl.'vendor/ags-dialog/ags-dialog.js', array('jquery'), self::VERSION);
		
		wp_enqueue_style('ags-layouts-datatables', self::$pluginBaseUrl.'vendor/datatables/datatables.min.css', array(), self::VERSION);
		wp_enqueue_script('ags-layouts-datatables', self::$pluginBaseUrl.'vendor/datatables/datatables.min.js', array('jquery'), self::VERSION);
	}
	
	static function registerScriptDependencies() {
		wp_register_script('ags-layouts-util', self::$pluginBaseUrl.'js/util.js', array('jquery'), self::VERSION); // redone
	}
	
	public static function getLayout() {
		include(self::$pluginDirectory.'includes/downloader.php');
		exit;
	}
	
	public static function getImage() {
		include(self::$pluginDirectory.'includes/get_image.php');
		exit;
	}
	
	public static function listLayouts() {
		include(self::$pluginDirectory.'includes/list.php');
		exit;
	}
	
	public static function exportLayout() {
		include(self::$pluginDirectory.'includes/uploader.php');
		exit;
	}
	
	public static function updateLayout() {
		include(self::$pluginDirectory.'includes/update.php');
		exit;
	}
	
	public static function deleteLayout() {
		include(self::$pluginDirectory.'includes/delete.php');
		exit;
	}
	
	public static function filterContentForScreenshot() {
		
		$layoutContents = stripslashes($_POST['ags_layouts_ss_content']);
		// Switch statement copied from includes/uploader.php (modified)
		switch ($_POST['ags_layouts_ss_editor']) {
			case 'divi':
				// This is very similar to how Divi does it but developed independently :)
				$layoutContents = json_decode($layoutContents, true);
				if (empty($layoutContents)) {
					return;
				}
				$layoutContents = et_fb_process_to_shortcode($layoutContents);
				break;
			case 'gutenberg':
				break;
		}
		
		apply_filters('ags_layouts_screenshot_content_unfiltered', $layoutContents);
		
		remove_filter('the_content', array('AGSLayouts', 'filterContentForScreenshot'));
		$layoutContents = apply_filters('the_content', $layoutContents);
		add_filter('the_content', array('AGSLayouts', 'filterContentForScreenshot'));
		
		apply_filters('ags_layouts_screenshot_content_filtered', $layoutContents);
		
		
		return '
			<div class="ags_layouts_screenshot_container"></div>
			<script>
			var agsLayoutsScreenshotHasRun = false;
			jQuery(document).ready(function($) {
				if (!agsLayoutsScreenshotHasRun) {
					agsLayoutsScreenshotHasRun = true;
					var widthMax = 0;
					var $widestContainer;
					var $containers = $(\'.ags_layouts_screenshot_container\');
					for (var i = 0; i < $containers.length; ++i) {
						var $container = $($containers[i]);
						if ($container.width() > widthMax) {
							widthMax = $container.width();
							$widestContainer = $container;
						}
					}
					$widestContainer.html('.json_encode(wp_kses_post($layoutContents), 1, JSON_HEX_TAG & JSON_HEX_AMP & JSON_HEX_APOS & JSON_HEX_QUOT).');
					window.parent.ags_layouts_take_screenshot($widestContainer);
				}
			});
			</script>
		';
	}
	
	static function requireEditPermission($forPostId=null) {
		if ($forPostId === null ? !current_user_can('edit_posts') : !current_user_can('edit_post', $forPostId)) {
			exit;
		}
	}
	
}

AGSLayouts::setup();