<?php
/*
Changes by Aspen Grove Studios:
2019-01-02	Created class, partially using code from other Ghoster files and AGS Layouts class template
2019-01-03	Remove unnecessary code; fix incorrect method name
*/

class DiviGhosterCustomLogin {

	/**
	 * @var string path to login form template file
	 */
	private static $login_template_path;

	/**
	 * @var int page ID for the login form customizer page
	 */
	private static $login_template_page_id;

	/**
	 * @var string base url for the script directory
	 */
	private static $login_script_base_url;

	/**
	 * @var string base url for the style directory
	 */
	private static $login_style_base_url;

	/**
	 * @var string used as the page template instead of assigning the template path
	 */
	private static $login_customizer_page_template_name = '__agsdg_login__';

	public static function setup() {
		add_action( 'customize_register', array( 'DiviGhosterCustomLogin', 'wp_admin_area_customization' ) );
		// set up the builder customization
		add_action( 'customize_preview_init', array( 'DiviGhosterCustomLogin', 'customizer_preview_init' ) );
		add_action( 'customize_controls_enqueue_scripts', array( 'DiviGhosterCustomLogin', 'customizer_controls_init' ) );

		add_action( 'login_enqueue_scripts', array( 'DiviGhosterCustomLogin', 'load_wp_login_style'));
		add_action( 'login_enqueue_scripts', array( 'DiviGhosterCustomLogin', 'create_login_custom_inline_styles' ) );
		add_action( 'filter_login_custom_inline_styles', array( 'DiviGhosterCustomLogin', 'get_login_area_bg_image'));
		add_action( 'filter_login_custom_inline_styles', array( 'DiviGhosterCustomLogin', 'custom_login_logo'));
		add_action( 'filter_login_custom_inline_styles', array( 'DiviGhosterCustomLogin', 'get_login_area_color'));
		add_action( 'filter_login_custom_inline_styles', array( 'DiviGhosterCustomLogin', 'get_login_area_alighment'));

		add_filter( 'login_headerurl', array( 'DiviGhosterCustomLogin', 'custom_login_area_logo_image' ) );
		add_filter( 'login_headertitle', array( 'DiviGhosterCustomLogin', 'custom_login_area_logo_image_title' ) );

		add_action( 'init', array( __CLASS__, 'set_login_template_page_id' ) );
		self::$login_template_path = DiviGhoster::$pluginDirectory . 'includes/templates/login-customizer.php';
		self::$login_script_base_url = DiviGhoster::$pluginBaseUrl . 'js/';
		self::$login_style_base_url = DiviGhoster::$pluginBaseUrl . 'css/';
	}
	
	public static function wp_admin_area_customization($wp_customize) {
		// Check color control dependency
		if (class_exists('ET_Color_Alpha_Control')) {
			$colorControl = 'ET_Color_Alpha_Control';
		} else if (class_exists('ET_Divi_Customize_Color_Alpha_Control')) {
			$colorControl = 'ET_Divi_Customize_Color_Alpha_Control';
		} else {
			return;
		}

		$wp_customize->add_section('ghoster_custom_login', array(
			'title' => 'Login Customizer'
		));
		$wp_customize->add_setting('login_area_bg_image', array(
			'transport' => 'postMessage',
		));
		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'login_area_bg_image', array(
			'label' => __('Background Image', 'agsdg'),
			'section' => 'ghoster_custom_login'
		)));
		$wp_customize->add_setting('login_form_alignment', array(
			'default' => 'none',
			'transport' => 'postMessage',
		));
		$wp_customize->add_control('login_form_alignment', array(
			'label' => 'Login Form Alignment',
			'section' => 'ghoster_custom_login',
			'placeholder' => 'Align the login logo',
			'default' => 'none',
			'type' => 'radio',
			'choices' => array(
				'left' => 'Left',
				'none' => 'Center',
				'right' => 'Right'
			)
		));
		$wp_customize->add_setting('login_area_logo_image', array(
			'transport' => 'postMessage',
		));
		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'login_area_logo_image', array(
			'label' => __('Login Logo (in place of WP logo)', 'ags-ghoster'),
			'section' => 'ghoster_custom_login'
		)));
		$colors   = array();
		$colors[] = array(
			'slug' => 'login_background_color',
			'default' => '#f1f1f1',
			'label' => __('Background Color', 'ags-ghoster')
		);
		$colors[] = array(
			'slug' => 'content_link_color',
			'default' => '#999',
			'label' => __('Links Color', 'ags-ghoster')
		);
		$colors[] = array(
			'slug' => 'form_background_color',
			'default' => '#ffffff',
			'label' => __('Form Background Color', 'ags-ghoster')
		);
		$colors[] = array(
			'slug' => 'background_color_tint',
			'default' => 'rgba(0, 0, 0, 0)',
			'label' => __('Background Image Tint', 'ags-ghoster')
		);
		$colors[] = array(
			'slug' => 'login_submit_color',
			'default' => '#00a0d2',
			'label' => __('Submit Button Color', 'ags-ghoster')
		);
		
		foreach ($colors as $color) {
			$wp_customize->add_setting($color['slug'], array(
				'default' => $color['default'],
				'transport' => 'postMessage',
				'type' => 'option',
				'capability' => 'edit_theme_options'
			));
			$wp_customize->add_control(new $colorControl($wp_customize, $color['slug'], array(
				'label' => $color['label'],
				'section' => 'ghoster_custom_login',
				'settings' => $color['slug']
			)));
		}
	}

	public static function get_login_area_bg_image( $custom_styles ) {
		$default = '';
		$value = get_theme_mod( 'login_area_bg_image', $default );

		if ( empty( $value ) ) {
			return $custom_styles;
		}

		$custom_styles[] = sprintf( '.login { background-image: url(%s); background-size: cover; }', $value );

		return $custom_styles;
	}
	
	public static function custom_login_logo( $custom_styles ) {
		$default = '';
		$value = get_theme_mod('login_area_logo_image', $default);

		if ( empty( $value ) ) {
			return $custom_styles;
		}

		$custom_styles[] = sprintf( '#login h1 a { background-image: url(%s); }', $value );
		
		return $custom_styles;
	}
	
	public static function custom_login_area_logo_image() {
		return get_bloginfo('url');
	}
	
	public static function custom_login_area_logo_image_title() {
		return get_bloginfo('name');
	}

	public static function create_login_custom_inline_styles() {
		$custom_styles = array();
		$custom_styles = apply_filters( 'filter_login_custom_inline_styles', $custom_styles );

		if ( empty( $custom_styles ) ) {
			return;
		}

		$css_output = implode( "\n", $custom_styles );

		wp_add_inline_style( 'wp_login_css_dg', $css_output );

	}
	public static function get_login_area_alighment( $custom_styles ) {
		$default = '';
		$value   = get_theme_mod( 'login_form_alignment', $default );

		if ( empty( $value ) ) {
			return $custom_styles;
		}

		$custom_styles[] = sprintf( '#login { float: %s; }', $value );

		return $custom_styles;
	}
	
	public static function get_login_area_color( $custom_styles ) {

		$content_link_color = get_option( 'content_link_color', '' );
		$login_background_color = get_option( 'login_background_color', '' );
		$form_background_color = get_option( 'form_background_color', '' );
		$background_color_tint = get_option( 'background_color_tint', '' );
		$login_submit_color = get_option( 'login_submit_color', '' );

		if ( ! empty( $content_link_color ) ) {
			$custom_styles[] = sprintf( '.login #backtoblog a, #login form p label, .login #nav a, .login h1 a { color: %s; }', $content_link_color );
		}

		if ( ! empty( $login_background_color ) ) {
			$custom_styles[] = sprintf( '.login { background-color: %s; }', $login_background_color );
		}

		if ( ! empty( $form_background_color ) ) {
			$custom_styles[] = sprintf( '#loginform { background-color: %s; }', $form_background_color );
		}

		if ( ! empty( $background_color_tint ) ) {
			if ( is_customize_preview() ) {
				$custom_styles[] = sprintf( '.login .fake_login_after { background-color: %s; }', $background_color_tint );
			} else {
				$custom_styles[] = sprintf( '.login:after { background-color: %s; }', $background_color_tint );
			}
		}

		if ( ! empty( $login_submit_color ) ) {
			$custom_styles[] = sprintf( '#login .button-primary { background-color: %s; }', $login_submit_color );
		}

		return $custom_styles;
	}

	public static function load_wp_login_style() {
		wp_enqueue_style( 'wp_login_css_dg', self::$login_style_base_url . 'custom-login.css', array( 'login' ), DiviGhoster::PLUGIN_VERSION );
	}

	/**
	 * get things in place for the preview side of the customizer
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	public static function customizer_preview_init() {
		wp_enqueue_script( 'ags-ghost-login-customize-preview', self::$login_script_base_url . 'login-customize-preview.js', array( 'customize-preview' ), null, true );
		add_filter( 'template_include', array( __CLASS__, 'maybe_use_login_template' ) );
	}

	/**
	 * get things in place for the control side of the customizer
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	public static function customizer_controls_init() {
		wp_enqueue_script( 'ags-login-customize-controls', self::$login_script_base_url . 'login-customize-controls.js', array( 'customize-controls' ), null, true );

		$page = get_permalink( self::$login_template_page_id );
		// $page = wp_login_url();

		$localize = array(
			'page_url' => $page
		);

		wp_localize_script( 'ags-login-customize-controls', 'agsdg_login_customizer', $localize );
	}

	/**
	 * if customizer preview is loading our builder customizer page, load
	 * the necessary css and return the path to our template
	 *
	 * @param $template
	 *
	 * @return string
	 * @since: 3.0.0
	 *
	 */
	public static function maybe_use_login_template( $template ) {
		global $post;
		if ( ! $post ) {
			return $template;
		}

		$post_template = get_post_meta( $post->ID, '_wp_page_template', true );

		if ( empty( $post_template ) ) {
			return $template;
		}

		if ( self::$login_customizer_page_template_name !== $post_template ) {
			return $template;
		}

		if ( ! file_exists( self::$login_template_path ) ) {
			return $template;
		}

		wp_enqueue_style(
			'agsdg-customize-login-styles',
			self::$login_style_base_url . 'custom-login.css',
			array(),
			DiviGhoster::PLUGIN_VERSION
		);

		return self::$login_template_path;

	}

	/**
	 * get the page ID that will be used for customizing the login form
	 *
	 * @return int|WP_Error
	 * @since: 3.0.0
	 *
	 */
	public static function set_login_template_page_id() {
		self::$login_template_page_id = DiviGhosterCustomizerBase::get_customizer_template_page_id( self::$login_customizer_page_template_name, 'Login' );
	}

}

DiviGhosterCustomLogin::setup();