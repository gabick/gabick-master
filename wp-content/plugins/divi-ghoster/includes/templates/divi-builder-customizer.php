<?php
/**
 * Template Name: Divi Builder Customizer
 *
 * Template to display Divi Builder elements for customization purposes.
 */

if ( ! is_customize_preview() ) {
	if ( is_multisite() ) {
		$url = esc_url( network_home_url( '/' ) );
	} else {
		$url = esc_url( home_url( '/' ) );
	}
	wp_safe_redirect( $url );
}

function asgdg_customizer_add_body_classes( $classes ) {
	/*$classes[] = 'agsdg-customizer-template';*/
	$classes[] = 'et-fb-preview--desktop';
	return $classes;
}

add_filter( 'body_class', 'asgdg_customizer_add_body_classes' );

get_header();

?>

	<div id="main-content" class="et-fb-root-ancestor et-fb-iframe-ancestor">

		<article id="ghost-builder-customizer">
			<div class="entry-content et-fb-root-ancestor et-fb-iframe-ancestor">
				<div id="et-boc" class="et-boc et-fb-root-ancestor et-fb-iframe-ancestor">
					<div class="et_builder_inner_content et_pb_gutters3 et-fb-root-ancestor et-fb-iframe-ancestor">
						<div id="et-fb-app" class="et-fb-root-ancestor et-fb-iframe-ancestor">
							<div id="et_pb_root" class="et_pb_root--vb">
								<h3>Wireframe Builder</h3>
								<div id="et_pb_layout" class="postbox  first-visible et-loaded" style="">
									<h2 class="hndle ui-sortable-handle">
										<span>The Divi Builder</span>
										<span class="et-bfb-page-settings-bar">
											<div class="et-fb-page-settings-bar">
												<div class="et-fb-button-group et-fb-page-settings-bar__column et-fb-page-settings-bar__column--main">
													<div class="et-fb-button-group" style="margin-right: 0px; border-radius: 3px 0px 0px 3px; transition: none 0s ease 0s;">
														<button type="button" data-tip="Load From Library" class="et-fb-button et-fb-button--primary et-fb-button--round et-fb-button--tooltip et-fb-button--toggle-add" currentitem="false">
															<div class="et-fb-icon et-fb-icon--add" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g>
																		<path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path>
																	</g>
																</svg>
															</div>
															<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
														</button>
														<button type="button" data-tip="Save To Library" class="et-fb-button et-fb-button--primary et-fb-button--round et-fb-button--tooltip et-fb-button--toggle-save" currentitem="false">
															<div class="et-fb-icon et-fb-icon--save" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g>
																		<path d="M18.95 9.051a1 1 0 1 0-1.414 1.414 5 5 0 1 1-7.07 0A1 1 0 0 0 9.05 9.051a7 7 0 1 0 9.9.001v-.001zm-5.378 8.235a.5.5 0 0 0 .857 0l2.117-3.528a.5.5 0 0 0-.429-.758H15V8a1 1 0 0 0-2 0v5h-1.117a.5.5 0 0 0-.428.758l2.117 3.528z" fill-rule="evenodd"></path>
																	</g>
																</svg>
															</div>
															<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
														</button>
														<button type="button" data-tip="Portability" class="et-fb-button et-fb-button--primary et-fb-button--round et-fb-button--tooltip et-fb-button--toggle-portability" currentitem="false">
															<div class="et-fb-icon et-fb-icon--portability" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g>
																		<path d="M9.6 20.8c0.2 0.3 0.7 0.3 0.9 0l2.1-3.5c0.2-0.3 0-0.8-0.4-0.8H11V8c0-0.6-0.4-1-1-1C9.4 7 9 7.4 9 8v8.5H7.9c-0.4 0-0.6 0.4-0.4 0.8L9.6 20.8z" fill-rule="evenodd"></path>
																		<path d="M18.4 7.2c-0.2-0.3-0.7-0.3-0.9 0l-2.1 3.5c-0.2 0.3 0 0.8 0.4 0.8H17V20c0 0.6 0.4 1 1 1 0.6 0 1-0.4 1-1v-8.5h1.1c0.4 0 0.6-0.4 0.4-0.8L18.4 7.2z" fill-rule="evenodd"></path>
																	</g>
																</svg>
															</div>
															<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
														</button>
													</div>
													<div class="et-fb-button-group">
														<button type="button" data-tip="Editing History" class="et-fb-button et-fb-button--primary et-fb-button--round et-fb-button--toggle-history" currentitem="false">
															<div class="et-fb-icon et-fb-icon--history" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g>
																		<path d="M14 6.5C9.9 6.5 6.5 9.9 6.5 14 6.5 18.1 9.9 21.5 14 21.5 18.1 21.5 21.5 18.1 21.5 14 21.5 9.9 18.1 6.5 14 6.5L14 6.5ZM14 19.5C11 19.5 8.5 17 8.5 14 8.5 11 11 8.5 14 8.5 17 8.5 19.5 11 19.5 14 19.5 17 17 19.5 14 19.5L14 19.5Z" fill-rule="evenodd"></path>
																		<path d="M17 13L15 13 15 11C15 10.5 14.5 10 14 10 13.5 10 13 10.5 13 11L13 14C13 14.5 13.5 15 14 15L17 15C17.5 15 18 14.5 18 14 18 13.5 17.5 13 17 13L17 13Z" fill-rule="evenodd"></path>
																	</g>
																</svg>
															</div>
															<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
														</button>
														<button type="button" data-tip="Clear Layout" class="et-fb-button et-fb-button--primary et-fb-button--round et-fb-button--tooltip et-fb-button--toggle-delete" currentitem="false">
															<div class="et-fb-icon et-fb-icon--delete" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g>
																		<path d="M19 9h-3V8a1 1 0 0 0-1-1h-2a1 1 0 0 0-1 1v1H9a1 1 0 1 0 0 2h10a1 1 0 0 0 .004-2H19zM9 20c.021.543.457.979 1 1h8c.55-.004.996-.45 1-1v-7H9v7zm2.02-4.985h2v4h-2v-4zm4 0h2v4h-2v-4z" fill-rule="evenodd"></path>
																	</g>
																</svg>
															</div>
															<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
														</button>
														<button type="button" data-tip="Page Settings" class="et-fb-button et-fb-button--primary et-fb-button--round et-fb-button--toggle-setting" currentitem="false">
															<div class="et-fb-icon et-fb-icon--setting" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g>
																		<path d="M20.426 13.088l-1.383-.362a.874.874 0 0 1-.589-.514l-.043-.107a.871.871 0 0 1 .053-.779l.721-1.234a.766.766 0 0 0-.116-.917 6.682 6.682 0 0 0-.252-.253.768.768 0 0 0-.917-.116l-1.234.722a.877.877 0 0 1-.779.053l-.107-.044a.87.87 0 0 1-.513-.587l-.362-1.383a.767.767 0 0 0-.73-.567h-.358a.768.768 0 0 0-.73.567l-.362 1.383a.878.878 0 0 1-.513.589l-.107.044a.875.875 0 0 1-.778-.054l-1.234-.722a.769.769 0 0 0-.918.117c-.086.082-.17.166-.253.253a.766.766 0 0 0-.115.916l.721 1.234a.87.87 0 0 1 .053.779l-.043.106a.874.874 0 0 1-.589.514l-1.382.362a.766.766 0 0 0-.567.731v.357a.766.766 0 0 0 .567.731l1.383.362c.266.07.483.26.588.513l.043.107a.87.87 0 0 1-.053.779l-.721 1.233a.767.767 0 0 0 .115.917c.083.087.167.171.253.253a.77.77 0 0 0 .918.116l1.234-.721a.87.87 0 0 1 .779-.054l.107.044a.878.878 0 0 1 .513.589l.362 1.383a.77.77 0 0 0 .731.567h.356a.766.766 0 0 0 .73-.567l.362-1.383a.878.878 0 0 1 .515-.589l.107-.044a.875.875 0 0 1 .778.054l1.234.721c.297.17.672.123.917-.117.087-.082.171-.166.253-.253a.766.766 0 0 0 .116-.917l-.721-1.234a.874.874 0 0 1-.054-.779l.044-.107a.88.88 0 0 1 .589-.513l1.383-.362a.77.77 0 0 0 .567-.731v-.357a.772.772 0 0 0-.569-.724v-.005zm-6.43 3.9a2.986 2.986 0 1 1 2.985-2.986 3 3 0 0 1-2.985 2.987v-.001z" fill-rule="evenodd"></path>
																	</g>
																</svg></div><canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
														</button><button type="button" class="et-fb-button et-fb-button--app-modal">
															<div class="et-fb-icon et-fb-icon--app-setting" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: 0px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g>
																		<path d="M2.001 4.5a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 6a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 6a2 2 0 1 1 0-4 2 2 0 0 1 0 4z" fill-rule="evenodd"></path>
																	</g>
																</svg></div><canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
														</button></div>
												</div>
											</div>
										</span>
									</h2>
									<div class="inside">
										<div class="et_divi_builder">
											<div id="et_pb_root" class="et_pb_root--bfb">
												<div class="et-fb-post-content ui-sortable">
													<div class="et_pb_section ui-sortable et_pb_section_0 et_section_regular et_pb_scroll_0" data-address="0">
														<div class="et-fb-skeleton  et-fb-skeleton--section ui-sortable-handle">
															<div class="et-fb-skeleton__header">
																<div class="et-fb-button-group et-fb-button-group--main"><button type="button" data-tip="Move Section" class="et-fb-button et-fb-button--info" currentitem="false">
																		<div class="et-fb-icon et-fb-icon--move" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																				<g>
																					<path d="M12.968 17.968a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057l1.835 1.835a.308.308 0 0 0 .437 0l1.836-1.835a.749.749 0 0 0 0-1.057.752.752 0 0 0-1.06 0v-2.994h3a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.056 0l1.836-1.835a.31.31 0 0 0 0-.437l-1.836-1.835a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057h-3V9.987a1.28 1.28 0 0 0 1.057 0 .749.749 0 0 0 0-1.057l-1.827-1.84a.308.308 0 0 0-.437 0l-1.835 1.836a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.057 0v2.994h-2.99a.747.747 0 0 0 0-1.057.747.747 0 0 0-1.058 0L7.09 13.755a.31.31 0 0 0 0 .437l1.835 1.835a.747.747 0 0 0 1.057 0 .747.747 0 0 0 0-1.057h2.991l-.005 2.998z" fill-rule="evenodd"></path>
																				</g>
																			</svg></div>
																	</button><button type="button" data-tip="Section Settings" class="et-fb-button et-fb-button--info" currentitem="false">
																		<div class="et-fb-icon et-fb-icon--setting" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																				<g>
																					<path d="M20.426 13.088l-1.383-.362a.874.874 0 0 1-.589-.514l-.043-.107a.871.871 0 0 1 .053-.779l.721-1.234a.766.766 0 0 0-.116-.917 6.682 6.682 0 0 0-.252-.253.768.768 0 0 0-.917-.116l-1.234.722a.877.877 0 0 1-.779.053l-.107-.044a.87.87 0 0 1-.513-.587l-.362-1.383a.767.767 0 0 0-.73-.567h-.358a.768.768 0 0 0-.73.567l-.362 1.383a.878.878 0 0 1-.513.589l-.107.044a.875.875 0 0 1-.778-.054l-1.234-.722a.769.769 0 0 0-.918.117c-.086.082-.17.166-.253.253a.766.766 0 0 0-.115.916l.721 1.234a.87.87 0 0 1 .053.779l-.043.106a.874.874 0 0 1-.589.514l-1.382.362a.766.766 0 0 0-.567.731v.357a.766.766 0 0 0 .567.731l1.383.362c.266.07.483.26.588.513l.043.107a.87.87 0 0 1-.053.779l-.721 1.233a.767.767 0 0 0 .115.917c.083.087.167.171.253.253a.77.77 0 0 0 .918.116l1.234-.721a.87.87 0 0 1 .779-.054l.107.044a.878.878 0 0 1 .513.589l.362 1.383a.77.77 0 0 0 .731.567h.356a.766.766 0 0 0 .73-.567l.362-1.383a.878.878 0 0 1 .515-.589l.107-.044a.875.875 0 0 1 .778.054l1.234.721c.297.17.672.123.917-.117.087-.082.171-.166.253-.253a.766.766 0 0 0 .116-.917l-.721-1.234a.874.874 0 0 1-.054-.779l.044-.107a.88.88 0 0 1 .589-.513l1.383-.362a.77.77 0 0 0 .567-.731v-.357a.772.772 0 0 0-.569-.724v-.005zm-6.43 3.9a2.986 2.986 0 1 1 2.985-2.986 3 3 0 0 1-2.985 2.987v-.001z" fill-rule="evenodd"></path>
																				</g>
																			</svg></div>
																	</button><button type="button" data-tip="Duplicate Section" class="et-fb-button et-fb-button--info" currentitem="false">
																		<div class="et-fb-icon et-fb-icon--copy" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																				<g>
																					<path d="M16.919 15.391c.05-.124.074-.257.072-.39v-6a1.02 1.02 0 0 0-.072-.389.969.969 0 0 0-.893-.612H7.969a.97.97 0 0 0-.893.611c-.05.124-.076.256-.076.39v6c0 .134.026.266.076.39.146.365.5.604.893.605h8.057a.968.968 0 0 0 .893-.605zm3.074-3.413a1 1 0 0 0-1 1v5.011h-7.008a1 1 0 1 0 0 2h8a1 1 0 0 0 1-1v-6.013a1 1 0 0 0-.992-.998zm-5.016 2.013H8.991v-3.988h5.986v3.993-.005z" fill-rule="evenodd"></path>
																				</g>
																			</svg></div>
																	</button><button type="button" data-tip="Save Section To Library" class="et-fb-button et-fb-button--info et-fb-button--tooltip" currentitem="false">
																		<div class="et-fb-icon et-fb-icon--save" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																				<g>
																					<path d="M18.95 9.051a1 1 0 1 0-1.414 1.414 5 5 0 1 1-7.07 0A1 1 0 0 0 9.05 9.051a7 7 0 1 0 9.9.001v-.001zm-5.378 8.235a.5.5 0 0 0 .857 0l2.117-3.528a.5.5 0 0 0-.429-.758H15V8a1 1 0 0 0-2 0v5h-1.117a.5.5 0 0 0-.428.758l2.117 3.528z" fill-rule="evenodd"></path>
																				</g>
																			</svg></div>
																	</button><button type="button" data-tip="Delete Section" class="et-fb-button et-fb-button--info" currentitem="false">
																		<div class="et-fb-icon et-fb-icon--delete" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																				<g>
																					<path d="M19 9h-3V8a1 1 0 0 0-1-1h-2a1 1 0 0 0-1 1v1H9a1 1 0 1 0 0 2h10a1 1 0 0 0 .004-2H19zM9 20c.021.543.457.979 1 1h8c.55-.004.996-.45 1-1v-7H9v7zm2.02-4.985h2v4h-2v-4zm4 0h2v4h-2v-4z" fill-rule="evenodd"></path>
																				</g>
																			</svg></div>
																	</button></div>
																<div class="et-fb-skeleton__title"><span class="et-fb-editable-element" data-shortcode-id="0-1552574771279" contenteditable="false">Section</span></div>
																<div class="et-fb-button-group et-fb-button-group--toggle"><button type="button" class="et-fb-button et-fb-button--info">
																		<div class="et-fb-icon et-fb-icon--chevron-up" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																				<g>
																					<path d="M20 14.62L15.38 10a1.25 1.25 0 0 0-1.77 0L9 14.62a1.25 1.25 0 0 0 1.77 1.77l3.71-3.71 3.71 3.71A1.25 1.25 0 1 0 20 14.62z" fill-rule="evenodd"></path>
																				</g>
																			</svg></div>
																	</button></div>
															</div>
															<div class="et-fb-skeleton__content ui-sortable" style="padding-bottom: 35px;">
																<div class="et_pb_row ui-sortable et_pb_row_0 et-first-child et-last-child" data-address="0.0">
																	<div class="et-fb-skeleton  et-fb-skeleton--row">
																		<div class="et-fb-skeleton__header">
																			<div class="et-fb-button-group et-fb-button-group--main"><button type="button" data-tip="Move Row" class="et-fb-button" currentitem="false">
																					<div class="et-fb-icon et-fb-icon--move" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																							<g>
																								<path d="M12.968 17.968a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057l1.835 1.835a.308.308 0 0 0 .437 0l1.836-1.835a.749.749 0 0 0 0-1.057.752.752 0 0 0-1.06 0v-2.994h3a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.056 0l1.836-1.835a.31.31 0 0 0 0-.437l-1.836-1.835a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057h-3V9.987a1.28 1.28 0 0 0 1.057 0 .749.749 0 0 0 0-1.057l-1.827-1.84a.308.308 0 0 0-.437 0l-1.835 1.836a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.057 0v2.994h-2.99a.747.747 0 0 0 0-1.057.747.747 0 0 0-1.058 0L7.09 13.755a.31.31 0 0 0 0 .437l1.835 1.835a.747.747 0 0 0 1.057 0 .747.747 0 0 0 0-1.057h2.991l-.005 2.998z" fill-rule="evenodd"></path>
																							</g>
																						</svg></div>
																				</button><button type="button" data-tip="Row Settings" class="et-fb-button" currentitem="false">
																					<div class="et-fb-icon et-fb-icon--setting" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																							<g>
																								<path d="M20.426 13.088l-1.383-.362a.874.874 0 0 1-.589-.514l-.043-.107a.871.871 0 0 1 .053-.779l.721-1.234a.766.766 0 0 0-.116-.917 6.682 6.682 0 0 0-.252-.253.768.768 0 0 0-.917-.116l-1.234.722a.877.877 0 0 1-.779.053l-.107-.044a.87.87 0 0 1-.513-.587l-.362-1.383a.767.767 0 0 0-.73-.567h-.358a.768.768 0 0 0-.73.567l-.362 1.383a.878.878 0 0 1-.513.589l-.107.044a.875.875 0 0 1-.778-.054l-1.234-.722a.769.769 0 0 0-.918.117c-.086.082-.17.166-.253.253a.766.766 0 0 0-.115.916l.721 1.234a.87.87 0 0 1 .053.779l-.043.106a.874.874 0 0 1-.589.514l-1.382.362a.766.766 0 0 0-.567.731v.357a.766.766 0 0 0 .567.731l1.383.362c.266.07.483.26.588.513l.043.107a.87.87 0 0 1-.053.779l-.721 1.233a.767.767 0 0 0 .115.917c.083.087.167.171.253.253a.77.77 0 0 0 .918.116l1.234-.721a.87.87 0 0 1 .779-.054l.107.044a.878.878 0 0 1 .513.589l.362 1.383a.77.77 0 0 0 .731.567h.356a.766.766 0 0 0 .73-.567l.362-1.383a.878.878 0 0 1 .515-.589l.107-.044a.875.875 0 0 1 .778.054l1.234.721c.297.17.672.123.917-.117.087-.082.171-.166.253-.253a.766.766 0 0 0 .116-.917l-.721-1.234a.874.874 0 0 1-.054-.779l.044-.107a.88.88 0 0 1 .589-.513l1.383-.362a.77.77 0 0 0 .567-.731v-.357a.772.772 0 0 0-.569-.724v-.005zm-6.43 3.9a2.986 2.986 0 1 1 2.985-2.986 3 3 0 0 1-2.985 2.987v-.001z" fill-rule="evenodd"></path>
																							</g>
																						</svg></div>
																				</button><button type="button" data-tip="Duplicate Row" class="et-fb-button" currentitem="false">
																					<div class="et-fb-icon et-fb-icon--copy" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																							<g>
																								<path d="M16.919 15.391c.05-.124.074-.257.072-.39v-6a1.02 1.02 0 0 0-.072-.389.969.969 0 0 0-.893-.612H7.969a.97.97 0 0 0-.893.611c-.05.124-.076.256-.076.39v6c0 .134.026.266.076.39.146.365.5.604.893.605h8.057a.968.968 0 0 0 .893-.605zm3.074-3.413a1 1 0 0 0-1 1v5.011h-7.008a1 1 0 1 0 0 2h8a1 1 0 0 0 1-1v-6.013a1 1 0 0 0-.992-.998zm-5.016 2.013H8.991v-3.988h5.986v3.993-.005z" fill-rule="evenodd"></path>
																							</g>
																						</svg></div>
																				</button><button type="button" data-tip="Change Column Structure" class="et-fb-button et-fb-button--tooltip" currentitem="false">
																					<div class="et-fb-icon et-fb-icon--column" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																							<g>
																								<path d="M20 8H8a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V9a1 1 0 0 0-1-.999V8zm-7 2h2v8h-2v-8zm-2 8H9v-8h2v8zm6-8h2v8h-2v-8z" fill-rule="evenodd"></path>
																							</g>
																						</svg></div>
																				</button><button type="button" data-tip="Save Row To Library" class="et-fb-button et-fb-button--tooltip" currentitem="false">
																					<div class="et-fb-icon et-fb-icon--save" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																							<g>
																								<path d="M18.95 9.051a1 1 0 1 0-1.414 1.414 5 5 0 1 1-7.07 0A1 1 0 0 0 9.05 9.051a7 7 0 1 0 9.9.001v-.001zm-5.378 8.235a.5.5 0 0 0 .857 0l2.117-3.528a.5.5 0 0 0-.429-.758H15V8a1 1 0 0 0-2 0v5h-1.117a.5.5 0 0 0-.428.758l2.117 3.528z" fill-rule="evenodd"></path>
																							</g>
																						</svg></div>
																				</button><button type="button" data-tip="Delete Row" class="et-fb-button" currentitem="false">
																					<div class="et-fb-icon et-fb-icon--delete" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																							<g>
																								<path d="M19 9h-3V8a1 1 0 0 0-1-1h-2a1 1 0 0 0-1 1v1H9a1 1 0 1 0 0 2h10a1 1 0 0 0 .004-2H19zM9 20c.021.543.457.979 1 1h8c.55-.004.996-.45 1-1v-7H9v7zm2.02-4.985h2v4h-2v-4zm4 0h2v4h-2v-4z" fill-rule="evenodd"></path>
																							</g>
																						</svg></div>
																				</button></div>
																			<div class="et-fb-skeleton__title"><span class="et-fb-editable-element" data-shortcode-id="0.0-1552574771310" contenteditable="false">Row</span></div>
																			<div class="et-fb-button-group et-fb-button-group--toggle"><button type="button" class="et-fb-button">
																					<div class="et-fb-icon et-fb-icon--chevron-up" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																							<g>
																								<path d="M20 14.62L15.38 10a1.25 1.25 0 0 0-1.77 0L9 14.62a1.25 1.25 0 0 0 1.77 1.77l3.71-3.71 3.71 3.71A1.25 1.25 0 1 0 20 14.62z" fill-rule="evenodd"></path>
																							</g>
																						</svg></div>
																				</button></div>
																		</div>
																		<div class="et-fb-skeleton__content" style="padding-bottom: 35px;">
																			<div class="et_pb_column ui-sortable et_pb_column_1_2 et_pb_column_0 et-first-child et-last-child-2 et_pb_column_empty" data-address="0.0.0">
																				<div class="et-fb-mousetrap et-fb-mousetrap--column"></div>
																				<div class="et-fb-add-wrap et-fb-children--column"><button type="button" data-tip="Add New Module" class="et-fb-button et-fb-button--elevate et-fb-button--inverse et-fb-button--round et-fb-button--tooltip" currentitem="false" style="opacity: 1; transform: scale(1);">
																						<div class="et-fb-icon et-fb-icon--add" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																								<g>
																									<path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path>
																								</g>
																							</svg></div><canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
																					</button></div>
																			</div>
																			<div class="et_pb_column ui-sortable et_pb_column_1_2 et_pb_column_1 et-last-child" data-address="0.0.1">
																				<div class="et-fb-mousetrap et-fb-mousetrap--column"></div>
																				<div class="et_pb_module" data-address="0.0.1.0">
																					<div class="et-fb-skeleton  et-fb-skeleton--module">
																						<div class="et-fb-skeleton__header">
																							<div class="et-fb-button-group et-fb-button-group--main"><button type="button" data-tip="Move Module" class="et-fb-button" currentitem="false">
																									<div class="et-fb-icon et-fb-icon--move" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																											<g>
																												<path d="M12.968 17.968a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057l1.835 1.835a.308.308 0 0 0 .437 0l1.836-1.835a.749.749 0 0 0 0-1.057.752.752 0 0 0-1.06 0v-2.994h3a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.056 0l1.836-1.835a.31.31 0 0 0 0-.437l-1.836-1.835a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057h-3V9.987a1.28 1.28 0 0 0 1.057 0 .749.749 0 0 0 0-1.057l-1.827-1.84a.308.308 0 0 0-.437 0l-1.835 1.836a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.057 0v2.994h-2.99a.747.747 0 0 0 0-1.057.747.747 0 0 0-1.058 0L7.09 13.755a.31.31 0 0 0 0 .437l1.835 1.835a.747.747 0 0 0 1.057 0 .747.747 0 0 0 0-1.057h2.991l-.005 2.998z" fill-rule="evenodd"></path>
																											</g>
																										</svg></div>
																								</button><button type="button" data-tip="Module Settings" class="et-fb-button" currentitem="false">
																									<div class="et-fb-icon et-fb-icon--setting" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																											<g>
																												<path d="M20.426 13.088l-1.383-.362a.874.874 0 0 1-.589-.514l-.043-.107a.871.871 0 0 1 .053-.779l.721-1.234a.766.766 0 0 0-.116-.917 6.682 6.682 0 0 0-.252-.253.768.768 0 0 0-.917-.116l-1.234.722a.877.877 0 0 1-.779.053l-.107-.044a.87.87 0 0 1-.513-.587l-.362-1.383a.767.767 0 0 0-.73-.567h-.358a.768.768 0 0 0-.73.567l-.362 1.383a.878.878 0 0 1-.513.589l-.107.044a.875.875 0 0 1-.778-.054l-1.234-.722a.769.769 0 0 0-.918.117c-.086.082-.17.166-.253.253a.766.766 0 0 0-.115.916l.721 1.234a.87.87 0 0 1 .053.779l-.043.106a.874.874 0 0 1-.589.514l-1.382.362a.766.766 0 0 0-.567.731v.357a.766.766 0 0 0 .567.731l1.383.362c.266.07.483.26.588.513l.043.107a.87.87 0 0 1-.053.779l-.721 1.233a.767.767 0 0 0 .115.917c.083.087.167.171.253.253a.77.77 0 0 0 .918.116l1.234-.721a.87.87 0 0 1 .779-.054l.107.044a.878.878 0 0 1 .513.589l.362 1.383a.77.77 0 0 0 .731.567h.356a.766.766 0 0 0 .73-.567l.362-1.383a.878.878 0 0 1 .515-.589l.107-.044a.875.875 0 0 1 .778.054l1.234.721c.297.17.672.123.917-.117.087-.082.171-.166.253-.253a.766.766 0 0 0 .116-.917l-.721-1.234a.874.874 0 0 1-.054-.779l.044-.107a.88.88 0 0 1 .589-.513l1.383-.362a.77.77 0 0 0 .567-.731v-.357a.772.772 0 0 0-.569-.724v-.005zm-6.43 3.9a2.986 2.986 0 1 1 2.985-2.986 3 3 0 0 1-2.985 2.987v-.001z" fill-rule="evenodd"></path>
																											</g>
																										</svg></div>
																								</button><button type="button" data-tip="Duplicate Module" class="et-fb-button" currentitem="false">
																									<div class="et-fb-icon et-fb-icon--copy" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																											<g>
																												<path d="M16.919 15.391c.05-.124.074-.257.072-.39v-6a1.02 1.02 0 0 0-.072-.389.969.969 0 0 0-.893-.612H7.969a.97.97 0 0 0-.893.611c-.05.124-.076.256-.076.39v6c0 .134.026.266.076.39.146.365.5.604.893.605h8.057a.968.968 0 0 0 .893-.605zm3.074-3.413a1 1 0 0 0-1 1v5.011h-7.008a1 1 0 1 0 0 2h8a1 1 0 0 0 1-1v-6.013a1 1 0 0 0-.992-.998zm-5.016 2.013H8.991v-3.988h5.986v3.993-.005z" fill-rule="evenodd"></path>
																											</g>
																										</svg></div>
																								</button><button type="button" data-tip="Save Module To Library" class="et-fb-button et-fb-button--tooltip" currentitem="false">
																									<div class="et-fb-icon et-fb-icon--save" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																											<g>
																												<path d="M18.95 9.051a1 1 0 1 0-1.414 1.414 5 5 0 1 1-7.07 0A1 1 0 0 0 9.05 9.051a7 7 0 1 0 9.9.001v-.001zm-5.378 8.235a.5.5 0 0 0 .857 0l2.117-3.528a.5.5 0 0 0-.429-.758H15V8a1 1 0 0 0-2 0v5h-1.117a.5.5 0 0 0-.428.758l2.117 3.528z" fill-rule="evenodd"></path>
																											</g>
																										</svg></div>
																								</button><button type="button" data-tip="Delete Module" class="et-fb-button" currentitem="false">
																									<div class="et-fb-icon et-fb-icon--delete" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																											<g>
																												<path d="M19 9h-3V8a1 1 0 0 0-1-1h-2a1 1 0 0 0-1 1v1H9a1 1 0 1 0 0 2h10a1 1 0 0 0 .004-2H19zM9 20c.021.543.457.979 1 1h8c.55-.004.996-.45 1-1v-7H9v7zm2.02-4.985h2v4h-2v-4zm4 0h2v4h-2v-4z" fill-rule="evenodd"></path>
																											</g>
																										</svg></div>
																								</button></div>
																							<div class="et-fb-skeleton__title"><span class="et-fb-editable-element" data-shortcode-id="0.0.1.0-1552574771344" contenteditable="false">Blurb</span></div>
																						</div>
																						<div class="et-fb-skeleton__content" style="padding: 0px;"><span class="et-fb-module-button-wrap--add"><button type="button" data-tip="Add New Module" class="et-fb-button et-fb-button--elevate et-fb-button--inverse et-fb-button--round et-fb-button--tooltip" currentitem="false" style="opacity: 1;">
                                                                    <div class="et-fb-icon et-fb-icon--add" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
                                                                            <g>
                                                                                <path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path>
                                                                            </g>
                                                                        </svg></div><canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
                                                                </button></span></div>
																					</div>
																				</div>
																			</div><span class="et-fb-row-button-wrap--add"><button type="button" data-tip="Add New Row" class="et-fb-button et-fb-button--round et-fb-button--success et-fb-button--tooltip" currentitem="false" style="opacity: 1;">
                                                    <div class="et-fb-icon et-fb-icon--add" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
                                                            <g>
                                                                <path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path>
                                                            </g>
                                                        </svg></div><canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
                                                </button></span>
																		</div>
																	</div>
																	<div class="et-fb-mousetrap et-fb-mousetrap--row et-fb-mousetrap-move" style="background: transparent;"></div>
																</div><span class="et-fb-section-button-wrap--add"><button type="button" data-tip="Add New Section" class="et-fb-button et-fb-button--elevate et-fb-button--info et-fb-button--round et-fb-button--tooltip" currentitem="false" style="opacity: 1;">
                                        <div class="et-fb-icon et-fb-icon--add" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;"><svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
                                                <g>
                                                    <path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path>
                                                </g>
                                            </svg></div><canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
                                    </button></span>
															</div>
														</div>
														<div class="et-fb-mousetrap et-fb-mousetrap--section et-fb-mousetrap-move ui-sortable-handle" style="background: transparent;"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<h3>New Builder</h3>
								<div class="et-fb-post-content ui-sortable">
									<div class="et_pb_section ui-sortable et_pb_section_0 et-animated--vb et_section_regular et_pb_scroll_0 et_fb_element_controls_visible" data-address="0">
										<div class="et-fb-mousetrap et-fb-mousetrap--section et-fb-mousetrap-move ui-sortable-handle" style="background: transparent;">
											<div class="et-fb-component-settings et-fb-component-settings--section" style="display: block;">
												<div class="et-fb-button-group et-fb-button-group--elevate et-fb-button-group--info" style="opacity: 1; transform: scale(1);">
													<div class="et-fb-button-group">
														<div class="et-fb-button et-fb-button--info et_fb_move_module_handler ui-sortable-handle" data-tip="Move Section" currentitem="false">
															<div class="et-fb-icon et-fb-icon--move" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M12.968 17.968a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057l1.835 1.835a.308.308 0 0 0 .437 0l1.836-1.835a.749.749 0 0 0 0-1.057.752.752 0 0 0-1.06 0v-2.994h3a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.056 0l1.836-1.835a.31.31 0 0 0 0-.437l-1.836-1.835a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057h-3V9.987a1.28 1.28 0 0 0 1.057 0 .749.749 0 0 0 0-1.057l-1.827-1.84a.308.308 0 0 0-.437 0l-1.835 1.836a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.057 0v2.994h-2.99a.747.747 0 0 0 0-1.057.747.747 0 0 0-1.058 0L7.09 13.755a.31.31 0 0 0 0 .437l1.835 1.835a.747.747 0 0 0 1.057 0 .747.747 0 0 0 0-1.057h2.991l-.005 2.998z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</div>
														<button type="button" data-tip="Section Settings" class="et-fb-button et-fb-button--info et_fb_edit_module_handler" currentitem="false">
															<div class="et-fb-icon et-fb-icon--setting" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M20.426 13.088l-1.383-.362a.874.874 0 0 1-.589-.514l-.043-.107a.871.871 0 0 1 .053-.779l.721-1.234a.766.766 0 0 0-.116-.917 6.682 6.682 0 0 0-.252-.253.768.768 0 0 0-.917-.116l-1.234.722a.877.877 0 0 1-.779.053l-.107-.044a.87.87 0 0 1-.513-.587l-.362-1.383a.767.767 0 0 0-.73-.567h-.358a.768.768 0 0 0-.73.567l-.362 1.383a.878.878 0 0 1-.513.589l-.107.044a.875.875 0 0 1-.778-.054l-1.234-.722a.769.769 0 0 0-.918.117c-.086.082-.17.166-.253.253a.766.766 0 0 0-.115.916l.721 1.234a.87.87 0 0 1 .053.779l-.043.106a.874.874 0 0 1-.589.514l-1.382.362a.766.766 0 0 0-.567.731v.357a.766.766 0 0 0 .567.731l1.383.362c.266.07.483.26.588.513l.043.107a.87.87 0 0 1-.053.779l-.721 1.233a.767.767 0 0 0 .115.917c.083.087.167.171.253.253a.77.77 0 0 0 .918.116l1.234-.721a.87.87 0 0 1 .779-.054l.107.044a.878.878 0 0 1 .513.589l.362 1.383a.77.77 0 0 0 .731.567h.356a.766.766 0 0 0 .73-.567l.362-1.383a.878.878 0 0 1 .515-.589l.107-.044a.875.875 0 0 1 .778.054l1.234.721c.297.17.672.123.917-.117.087-.082.171-.166.253-.253a.766.766 0 0 0 .116-.917l-.721-1.234a.874.874 0 0 1-.054-.779l.044-.107a.88.88 0 0 1 .589-.513l1.383-.362a.77.77 0 0 0 .567-.731v-.357a.772.772 0 0 0-.569-.724v-.005zm-6.43 3.9a2.986 2.986 0 1 1 2.985-2.986 3 3 0 0 1-2.985 2.987v-.001z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</button>
														<button type="button" data-tip="Duplicate Section" class="et-fb-button et-fb-button--info" currentitem="false">
															<div class="et-fb-icon et-fb-icon--copy" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M16.919 15.391c.05-.124.074-.257.072-.39v-6a1.02 1.02 0 0 0-.072-.389.969.969 0 0 0-.893-.612H7.969a.97.97 0 0 0-.893.611c-.05.124-.076.256-.076.39v6c0 .134.026.266.076.39.146.365.5.604.893.605h8.057a.968.968 0 0 0 .893-.605zm3.074-3.413a1 1 0 0 0-1 1v5.011h-7.008a1 1 0 1 0 0 2h8a1 1 0 0 0 1-1v-6.013a1 1 0 0 0-.992-.998zm-5.016 2.013H8.991v-3.988h5.986v3.993-.005z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</button>
														<button type="button" data-tip="Save Section To Library" class="et-fb-button et-fb-button--info et-fb-button--tooltip" currentitem="false">
															<div class="et-fb-icon et-fb-icon--save" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M18.95 9.051a1 1 0 1 0-1.414 1.414 5 5 0 1 1-7.07 0A1 1 0 0 0 9.05 9.051a7 7 0 1 0 9.9.001v-.001zm-5.378 8.235a.5.5 0 0 0 .857 0l2.117-3.528a.5.5 0 0 0-.429-.758H15V8a1 1 0 0 0-2 0v5h-1.117a.5.5 0 0 0-.428.758l2.117 3.528z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</button>
														<button type="button" data-tip="Delete Section" class="et-fb-button et-fb-button--info" currentitem="false">
															<div class="et-fb-icon et-fb-icon--delete" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M19 9h-3V8a1 1 0 0 0-1-1h-2a1 1 0 0 0-1 1v1H9a1 1 0 1 0 0 2h10a1 1 0 0 0 .004-2H19zM9 20c.021.543.457.979 1 1h8c.55-.004.996-.45 1-1v-7H9v7zm2.02-4.985h2v4h-2v-4zm4 0h2v4h-2v-4z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</button>
													</div>
												</div>
											</div>
										</div>
										<span class="et-fb-outline et-fb-outline--section et-fb-outline--top" style="opacity: 1;">
											<span class="et-fb-draggable-handle et-fb-draggable-handle--top"></span>
											<span class="et-fb-padding-indicator et-fb-padding-indicator--section et-fb-padding-indicator--top" style="height: 57px; display: none; opacity: 0;">
												<span class="et-fb-padding-indicator-value">
													<span class="text">60px</span>
													<span class="et-fb-link ">
														<div class="et-fb-icon et-fb-icon--unlinked" style="fill: rgb(190, 201, 214); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
															<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																<g><path d="M16.75 9.14a1 1 0 0 1 .37 1.39l-4.5 8a1 1 0 0 1-1.37.37 1 1 0 0 1-.37-1.39l4.5-8a1 1 0 0 1 1.37-.37zM19.71 10H20a3 3 0 0 1 3 3v2a3 3 0 0 1-3 3h-4.81l1.13-2H20a1 1 0 0 0 1-1v-2a1 1 0 0 0-1-1h-1.42zM12.81 10l-1.13 2H8a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h1.42l-1.13 2H8a3 3 0 0 1-3-3v-2a3 3 0 0 1 3-3z" fill-rule="evenodd"></path></g>
															</svg>
														</div>
													</span>
												</span>
											</span>
										</span>
										<span class="et-fb-outline et-fb-outline--section et-fb-outline--right" style="opacity: 1;">
											<span class="et-fb-draggable-handle et-fb-draggable-handle--right"></span>
											<span class="et-fb-padding-indicator et-fb-padding-indicator--section et-fb-padding-indicator--right" style="display: none; opacity: 0;"></span>
										</span>
										<span class="et-fb-outline et-fb-outline--section et-fb-outline--bottom" style="opacity: 1;">
											<span class="et-fb-draggable-handle et-fb-draggable-handle--bottom"></span>
											<span class="et-fb-padding-indicator et-fb-padding-indicator--section et-fb-padding-indicator--bottom" style="height: 57px; display: none; opacity: 0;">
												<span class="et-fb-padding-indicator-value">
													<span class="text">60px</span>
													<span class="et-fb-link ">
														<div class="et-fb-icon et-fb-icon--unlinked" style="fill: rgb(190, 201, 214); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
															<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																<g><path d="M16.75 9.14a1 1 0 0 1 .37 1.39l-4.5 8a1 1 0 0 1-1.37.37 1 1 0 0 1-.37-1.39l4.5-8a1 1 0 0 1 1.37-.37zM19.71 10H20a3 3 0 0 1 3 3v2a3 3 0 0 1-3 3h-4.81l1.13-2H20a1 1 0 0 0 1-1v-2a1 1 0 0 0-1-1h-1.42zM12.81 10l-1.13 2H8a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h1.42l-1.13 2H8a3 3 0 0 1-3-3v-2a3 3 0 0 1 3-3z" fill-rule="evenodd"></path></g>
															</svg>
														</div>
													</span>
												</span>
											</span>
										</span>
										<span class="et-fb-outline et-fb-outline--section et-fb-outline--left" style="opacity: 1;">
											<span class="et-fb-draggable-handle et-fb-draggable-handle--left"></span>
											<span class="et-fb-padding-indicator et-fb-padding-indicator--section et-fb-padding-indicator--left" style="display: none; opacity: 0;"></span>
										</span>
										<div class="et_pb_row ui-sortable et_pb_row_0 et-animated--vb et-fb-row--has-short-module et_fb_element_controls_visible et-first-child et-last-child" data-address="0.0">
											<div class="et_pb_column ui-sortable et-animated--vb et_pb_column_1_2 et_pb_column_0 et-first-child et-last-child-2 et_pb_column_empty" data-address="0.0.0" style="">
												<div class="et-fb-mousetrap et-fb-mousetrap--column"></div>
												<div class="et-fb-add-wrap et-fb-children--column">
													<button type="button" data-tip="Add New Module" class="et-fb-button et-fb-button--elevate et-fb-button--inverse et-fb-button--round et-fb-button--tooltip" style="opacity: 1; transform: scale(1);" currentitem="false">
														<div class="et-fb-icon et-fb-icon--add" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
															<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																<g><path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path></g>
															</svg>
														</div>
														<canvas height="80" width="80" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 100%;"></canvas>
													</button>
												</div>
											</div>
											<!-- module when hovered -->
											<div class="et_pb_column ui-sortable et-animated--vb et_pb_column_1_2 et_pb_column_1 et-last-child" data-address="0.0.1" style="">
												<div class="et-fb-mousetrap et-fb-mousetrap--column"></div>
												<div class="et_pb_hovered et-first-child et-last-child et_fb_element_controls_visible et_pb_search et_pb_module ui-sortable et_pb_search_0 et-animated--vb et_pb_text_align_left et_pb_bg_layout_light et-fb-module--short et-fb-module--has-mousetrap" data-address="0.0.1.0" dynamic="[object Object]" matching="[object Object]">
													<form role="search" method="get" class="et_pb_searchform">
														<div>
															<label class="screen-reader-text" for="s">Search for:</label>
															<input type="text" name="s" class="et_pb_s" placeholder="" data-quickaccess-id="field" value="" style="padding-right: 75px;">
															<input type="hidden" name="et_pb_searchform_submit" value="et_search_proccess">
															<div class="et_pb_searchsubmit_container">
																<input type="submit" class="et_pb_searchsubmit" data-quickaccess-id="button" value="Search" style="">
															</div>
														</div>
													</form>
													<span class="et-fb-module-button-wrap--add">
														<button type="button" data-tip="Add New Module" class="et-fb-button et-fb-button--elevate et-fb-button--inverse et-fb-button--round et-fb-button--tooltip" currentitem="false" style="opacity: 1; transform: scale(1);">
															<div class="et-fb-icon et-fb-icon--add" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
															<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 100%;"></canvas>
														</button>
													</span>
													<div class="et-fb-mousetrap et-fb-mousetrap--module et-fb-mousetrap-move" style="background: 0px center; pointer-events: auto; top: -22px; bottom: 0px;"></div>
													<div class="et-fb-component-settings et-fb-component-settings--module" style="display: block; margin-left: -77px;">
														<div class="et-fb-button-group et-fb-button-group--elevate et-fb-button-group--inverse" style="opacity: 1; transform: scale(1);">
															<div class="et-fb-button-group">
																<div class="et-fb-button et-fb-button--inverse et_fb_move_module_handler ui-sortable-handle" data-tip="Move Module" currentitem="false">
																	<div class="et-fb-icon et-fb-icon--move" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																		<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																			<g><path d="M12.968 17.968a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057l1.835 1.835a.308.308 0 0 0 .437 0l1.836-1.835a.749.749 0 0 0 0-1.057.752.752 0 0 0-1.06 0v-2.994h3a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.056 0l1.836-1.835a.31.31 0 0 0 0-.437l-1.836-1.835a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057h-3V9.987a1.28 1.28 0 0 0 1.057 0 .749.749 0 0 0 0-1.057l-1.827-1.84a.308.308 0 0 0-.437 0l-1.835 1.836a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.057 0v2.994h-2.99a.747.747 0 0 0 0-1.057.747.747 0 0 0-1.058 0L7.09 13.755a.31.31 0 0 0 0 .437l1.835 1.835a.747.747 0 0 0 1.057 0 .747.747 0 0 0 0-1.057h2.991l-.005 2.998z" fill-rule="evenodd"></path></g>
																		</svg>
																	</div>
																</div>
																<button type="button" data-tip="Module Settings" class="et-fb-button et-fb-button--inverse et_fb_edit_module_handler" currentitem="false">
																	<div class="et-fb-icon et-fb-icon--setting" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																		<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																			<g><path d="M20.426 13.088l-1.383-.362a.874.874 0 0 1-.589-.514l-.043-.107a.871.871 0 0 1 .053-.779l.721-1.234a.766.766 0 0 0-.116-.917 6.682 6.682 0 0 0-.252-.253.768.768 0 0 0-.917-.116l-1.234.722a.877.877 0 0 1-.779.053l-.107-.044a.87.87 0 0 1-.513-.587l-.362-1.383a.767.767 0 0 0-.73-.567h-.358a.768.768 0 0 0-.73.567l-.362 1.383a.878.878 0 0 1-.513.589l-.107.044a.875.875 0 0 1-.778-.054l-1.234-.722a.769.769 0 0 0-.918.117c-.086.082-.17.166-.253.253a.766.766 0 0 0-.115.916l.721 1.234a.87.87 0 0 1 .053.779l-.043.106a.874.874 0 0 1-.589.514l-1.382.362a.766.766 0 0 0-.567.731v.357a.766.766 0 0 0 .567.731l1.383.362c.266.07.483.26.588.513l.043.107a.87.87 0 0 1-.053.779l-.721 1.233a.767.767 0 0 0 .115.917c.083.087.167.171.253.253a.77.77 0 0 0 .918.116l1.234-.721a.87.87 0 0 1 .779-.054l.107.044a.878.878 0 0 1 .513.589l.362 1.383a.77.77 0 0 0 .731.567h.356a.766.766 0 0 0 .73-.567l.362-1.383a.878.878 0 0 1 .515-.589l.107-.044a.875.875 0 0 1 .778.054l1.234.721c.297.17.672.123.917-.117.087-.082.171-.166.253-.253a.766.766 0 0 0 .116-.917l-.721-1.234a.874.874 0 0 1-.054-.779l.044-.107a.88.88 0 0 1 .589-.513l1.383-.362a.77.77 0 0 0 .567-.731v-.357a.772.772 0 0 0-.569-.724v-.005zm-6.43 3.9a2.986 2.986 0 1 1 2.985-2.986 3 3 0 0 1-2.985 2.987v-.001z" fill-rule="evenodd"></path></g>
																		</svg>
																	</div>
																</button>
																<button type="button" data-tip="Duplicate Module" class="et-fb-button et-fb-button--inverse" currentitem="false">
																	<div class="et-fb-icon et-fb-icon--copy" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																		<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																			<g><path d="M16.919 15.391c.05-.124.074-.257.072-.39v-6a1.02 1.02 0 0 0-.072-.389.969.969 0 0 0-.893-.612H7.969a.97.97 0 0 0-.893.611c-.05.124-.076.256-.076.39v6c0 .134.026.266.076.39.146.365.5.604.893.605h8.057a.968.968 0 0 0 .893-.605zm3.074-3.413a1 1 0 0 0-1 1v5.011h-7.008a1 1 0 1 0 0 2h8a1 1 0 0 0 1-1v-6.013a1 1 0 0 0-.992-.998zm-5.016 2.013H8.991v-3.988h5.986v3.993-.005z" fill-rule="evenodd"></path></g>
																		</svg>
																	</div>
																</button>
																<button type="button" data-tip="Save Module To Library" class="et-fb-button et-fb-button--inverse et-fb-button--tooltip" currentitem="false">
																	<div class="et-fb-icon et-fb-icon--save" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																		<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																			<g><path d="M18.95 9.051a1 1 0 1 0-1.414 1.414 5 5 0 1 1-7.07 0A1 1 0 0 0 9.05 9.051a7 7 0 1 0 9.9.001v-.001zm-5.378 8.235a.5.5 0 0 0 .857 0l2.117-3.528a.5.5 0 0 0-.429-.758H15V8a1 1 0 0 0-2 0v5h-1.117a.5.5 0 0 0-.428.758l2.117 3.528z" fill-rule="evenodd"></path></g>
																		</svg>
																	</div>
																</button>
																<button type="button" data-tip="Delete Module" class="et-fb-button et-fb-button--inverse" currentitem="false">
																	<div class="et-fb-icon et-fb-icon--delete" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																		<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																			<g><path d="M19 9h-3V8a1 1 0 0 0-1-1h-2a1 1 0 0 0-1 1v1H9a1 1 0 1 0 0 2h10a1 1 0 0 0 .004-2H19zM9 20c.021.543.457.979 1 1h8c.55-.004.996-.45 1-1v-7H9v7zm2.02-4.985h2v4h-2v-4zm4 0h2v4h-2v-4z" fill-rule="evenodd"></path></g>
																		</svg>
																	</div>
																</button>
															</div>
														</div>
													</div>
													<style type="text/css" class="et-fb-custom-css-output">.et_pb_search_0 input.et_pb_s{margin-top: !important;padding-top: !important;height: auto; min-height: 0;}</style>
												</div>
											</div>
											<div class="et-fb-mousetrap et-fb-mousetrap--row et-fb-mousetrap-move" style="background: transparent;"></div>
											<div class="et-fb-component-settings et-fb-component-settings--row" style="display: block;">
												<div class="et-fb-button-group et-fb-button-group--elevate et-fb-button-group--success" style="opacity: 1; transform: scale(1);">
													<div class="et-fb-button-group">
														<div class="et-fb-button et-fb-button--success et_fb_move_module_handler ui-sortable-handle" data-tip="Move Row" currentitem="false">
															<div class="et-fb-icon et-fb-icon--move" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M12.968 17.968a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057l1.835 1.835a.308.308 0 0 0 .437 0l1.836-1.835a.749.749 0 0 0 0-1.057.752.752 0 0 0-1.06 0v-2.994h3a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.056 0l1.836-1.835a.31.31 0 0 0 0-.437l-1.836-1.835a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057h-3V9.987a1.28 1.28 0 0 0 1.057 0 .749.749 0 0 0 0-1.057l-1.827-1.84a.308.308 0 0 0-.437 0l-1.835 1.836a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.057 0v2.994h-2.99a.747.747 0 0 0 0-1.057.747.747 0 0 0-1.058 0L7.09 13.755a.31.31 0 0 0 0 .437l1.835 1.835a.747.747 0 0 0 1.057 0 .747.747 0 0 0 0-1.057h2.991l-.005 2.998z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</div>
														<button type="button" data-tip="Row Settings" class="et-fb-button et-fb-button--success et_fb_edit_module_handler" currentitem="false">
															<div class="et-fb-icon et-fb-icon--setting" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M20.426 13.088l-1.383-.362a.874.874 0 0 1-.589-.514l-.043-.107a.871.871 0 0 1 .053-.779l.721-1.234a.766.766 0 0 0-.116-.917 6.682 6.682 0 0 0-.252-.253.768.768 0 0 0-.917-.116l-1.234.722a.877.877 0 0 1-.779.053l-.107-.044a.87.87 0 0 1-.513-.587l-.362-1.383a.767.767 0 0 0-.73-.567h-.358a.768.768 0 0 0-.73.567l-.362 1.383a.878.878 0 0 1-.513.589l-.107.044a.875.875 0 0 1-.778-.054l-1.234-.722a.769.769 0 0 0-.918.117c-.086.082-.17.166-.253.253a.766.766 0 0 0-.115.916l.721 1.234a.87.87 0 0 1 .053.779l-.043.106a.874.874 0 0 1-.589.514l-1.382.362a.766.766 0 0 0-.567.731v.357a.766.766 0 0 0 .567.731l1.383.362c.266.07.483.26.588.513l.043.107a.87.87 0 0 1-.053.779l-.721 1.233a.767.767 0 0 0 .115.917c.083.087.167.171.253.253a.77.77 0 0 0 .918.116l1.234-.721a.87.87 0 0 1 .779-.054l.107.044a.878.878 0 0 1 .513.589l.362 1.383a.77.77 0 0 0 .731.567h.356a.766.766 0 0 0 .73-.567l.362-1.383a.878.878 0 0 1 .515-.589l.107-.044a.875.875 0 0 1 .778.054l1.234.721c.297.17.672.123.917-.117.087-.082.171-.166.253-.253a.766.766 0 0 0 .116-.917l-.721-1.234a.874.874 0 0 1-.054-.779l.044-.107a.88.88 0 0 1 .589-.513l1.383-.362a.77.77 0 0 0 .567-.731v-.357a.772.772 0 0 0-.569-.724v-.005zm-6.43 3.9a2.986 2.986 0 1 1 2.985-2.986 3 3 0 0 1-2.985 2.987v-.001z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</button>
														<button type="button" data-tip="Duplicate Row" class="et-fb-button et-fb-button--success" currentitem="false">
															<div class="et-fb-icon et-fb-icon--copy" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M16.919 15.391c.05-.124.074-.257.072-.39v-6a1.02 1.02 0 0 0-.072-.389.969.969 0 0 0-.893-.612H7.969a.97.97 0 0 0-.893.611c-.05.124-.076.256-.076.39v6c0 .134.026.266.076.39.146.365.5.604.893.605h8.057a.968.968 0 0 0 .893-.605zm3.074-3.413a1 1 0 0 0-1 1v5.011h-7.008a1 1 0 1 0 0 2h8a1 1 0 0 0 1-1v-6.013a1 1 0 0 0-.992-.998zm-5.016 2.013H8.991v-3.988h5.986v3.993-.005z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</button>
														<button type="button" data-tip="Change Column Structure" class="et-fb-button et-fb-button--success et-fb-button--tooltip" currentitem="false">
															<div class="et-fb-icon et-fb-icon--column" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M20 8H8a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V9a1 1 0 0 0-1-.999V8zm-7 2h2v8h-2v-8zm-2 8H9v-8h2v8zm6-8h2v8h-2v-8z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</button>
														<button type="button" data-tip="Save Row To Library" class="et-fb-button et-fb-button--success et-fb-button--tooltip" currentitem="false">
															<div class="et-fb-icon et-fb-icon--save" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M18.95 9.051a1 1 0 1 0-1.414 1.414 5 5 0 1 1-7.07 0A1 1 0 0 0 9.05 9.051a7 7 0 1 0 9.9.001v-.001zm-5.378 8.235a.5.5 0 0 0 .857 0l2.117-3.528a.5.5 0 0 0-.429-.758H15V8a1 1 0 0 0-2 0v5h-1.117a.5.5 0 0 0-.428.758l2.117 3.528z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</button>
														<button type="button" data-tip="Delete Row" class="et-fb-button et-fb-button--success" currentitem="false">
															<div class="et-fb-icon et-fb-icon--delete" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px; opacity: 1; transform: scale(1);">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M19 9h-3V8a1 1 0 0 0-1-1h-2a1 1 0 0 0-1 1v1H9a1 1 0 1 0 0 2h10a1 1 0 0 0 .004-2H19zM9 20c.021.543.457.979 1 1h8c.55-.004.996-.45 1-1v-7H9v7zm2.02-4.985h2v4h-2v-4zm4 0h2v4h-2v-4z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</button>
													</div>
												</div>
											</div>
											<span class="et-fb-outline et-fb-outline--row et-fb-outline--top" style="opacity: 1;">
												<span class="et-fb-draggable-handle et-fb-draggable-handle--top"></span>
												<span class="et-fb-padding-indicator et-fb-padding-indicator--row et-fb-padding-indicator--top" style="height: 27px; display: none; opacity: 0;">
													<span class="et-fb-padding-indicator-value">
														<span class="text">30px</span>
														<span class="et-fb-link ">
															<div class="et-fb-icon et-fb-icon--unlinked" style="fill: rgb(190, 201, 214); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M16.75 9.14a1 1 0 0 1 .37 1.39l-4.5 8a1 1 0 0 1-1.37.37 1 1 0 0 1-.37-1.39l4.5-8a1 1 0 0 1 1.37-.37zM19.71 10H20a3 3 0 0 1 3 3v2a3 3 0 0 1-3 3h-4.81l1.13-2H20a1 1 0 0 0 1-1v-2a1 1 0 0 0-1-1h-1.42zM12.81 10l-1.13 2H8a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h1.42l-1.13 2H8a3 3 0 0 1-3-3v-2a3 3 0 0 1 3-3z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</span>
													</span>
												</span>
											</span>
											<span class="et-fb-outline et-fb-outline--row et-fb-outline--right" style="opacity: 1;">
												<span class="et-fb-draggable-handle et-fb-draggable-handle--right"></span>
												<span class="et-fb-padding-indicator et-fb-padding-indicator--row et-fb-padding-indicator--right" style="display: none; opacity: 0;"></span>
											</span>
											<span class="et-fb-outline et-fb-outline--row et-fb-outline--bottom" style="opacity: 1;">
												<span class="et-fb-draggable-handle et-fb-draggable-handle--bottom"></span>
												<span class="et-fb-padding-indicator et-fb-padding-indicator--row et-fb-padding-indicator--bottom" style="height: 27px; display: none; opacity: 0;">
													<span class="et-fb-padding-indicator-value">
														<span class="text">30px</span>
														<span class="et-fb-link ">
															<div class="et-fb-icon et-fb-icon--unlinked" style="fill: rgb(190, 201, 214); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M16.75 9.14a1 1 0 0 1 .37 1.39l-4.5 8a1 1 0 0 1-1.37.37 1 1 0 0 1-.37-1.39l4.5-8a1 1 0 0 1 1.37-.37zM19.71 10H20a3 3 0 0 1 3 3v2a3 3 0 0 1-3 3h-4.81l1.13-2H20a1 1 0 0 0 1-1v-2a1 1 0 0 0-1-1h-1.42zM12.81 10l-1.13 2H8a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h1.42l-1.13 2H8a3 3 0 0 1-3-3v-2a3 3 0 0 1 3-3z" fill-rule="evenodd"></path></g>
																</svg>
															</div>
														</span>
													</span>
												</span>
											</span>
											<span class="et-fb-outline et-fb-outline--row et-fb-outline--left" style="opacity: 1;">
												<span class="et-fb-draggable-handle et-fb-draggable-handle--left"></span>
												<span class="et-fb-padding-indicator et-fb-padding-indicator--row et-fb-padding-indicator--left" style="display: none; opacity: 0;"></span>
											</span>
											<span class="et-fb-row-button-wrap--add">
												<button type="button" data-tip="Add New Row" class="et-fb-button et-fb-button--round et-fb-button--success et-fb-button--tooltip" currentitem="false" style="opacity: 1; transform: scale(1);">
													<div class="et-fb-icon et-fb-icon--add" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
														<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
															<g><path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path></g>
														</svg>
													</div>
													<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 100%;"></canvas>
												</button>
											</span>
											<style type="text/css" class="et-fb-custom-css-output">.et_pb_row_0.et_pb_row{margin-top: !important;padding-top: !important;}</style>
										</div>
										<span class="et-fb-section-button-wrap--add">
											<button type="button" data-tip="Add New Section" class="et-fb-button et-fb-button--elevate et-fb-button--info et-fb-button--round et-fb-button--tooltip" currentitem="false" style="opacity: 1; transform: scale(1);">
												<div class="et-fb-icon et-fb-icon--add" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g><path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 100%;"></canvas>
											</button>
										</span>
										<style type="text/css" class="et-fb-custom-css-output ui-sortable-handle">.et_pb_section_0{margin-top:;padding-top:;}</style>
									</div>
								</div>
								<!-- begin frontend bottom tools -->
								<div class="et-fb-page-settings-bar et-fb-page-settings-bar--active et-fb-page-settings-bar--horizontal" style="width: calc(100% - 17px); height: 90px; top: auto; right: auto; bottom: 0px; left: 0px;">
									<div class="et-fb-page-settings-bar__column et-fb-page-settings-bar__column--left" style="position: relative; right: 0px; opacity: 1;">
										<div class="et-fb-button-group et-fb-button-group--elevate et-fb-button-group--inverse et-fb-button-group--responsive-mode">
											<button type="button" class="et-fb-button et-fb-button--inverse et-fb-button--app-modal">
												<svg width="4" height="18" viewBox="0 0 4 17" xmlns="http://www.w3.org/2000/svg" style="margin-bottom: -4px;">
													<path d="M2.001 4.5a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 6a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 6a2 2 0 1 1 0-4 2 2 0 0 1 0 4z" fill="#FFFFFF" fill-rule="evenodd"></path>
												</svg>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Wireframe View" class="et-fb-button et-fb-button--inverse" currentitem="false">
												<div class="et-fb-icon et-fb-icon--wireframe" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g>
															<path d="M20 7H8C7.5 7 7 7.5 7 8v4c0 0.5 0.5 1 1 1h12c0.5 0 1-0.5 1-1V8C21 7.5 20.5 7 20 7zM19 11H9V9h10V11z" fill-rule="evenodd"></path>
															<path d="M12 15H8c-0.5 0-1 0.5-1 1v4c0 0.5 0.5 1 1 1h4c0.5 0 1-0.5 1-1v-4C13 15.5 12.5 15 12 15zM11 19H9v-2h2V19z" fill-rule="evenodd"></path>
															<path d="M20 15h-4c-0.5 0-1 0.5-1 1v4c0 0.5 0.5 1 1 1h4c0.5 0 1-0.5 1-1v-4C21 15.5 20.5 15 20 15zM19 19h-2v-2h2V19z" fill-rule="evenodd"></path>
														</g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Zoom Out" class="et-fb-button et-fb-button--inverse" currentitem="false">
												<div class="et-fb-icon et-fb-icon--zoom-in" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g><path d="M15.508 7a5.511 5.511 0 0 0-5.505 5.5 5.426 5.426 0 0 0 .847 2.92l-3.737 2.67c-.389.39.32 1.74.708 2.13.39.39 1.764 1.06 2.153.67l2.646-3.71A5.5 5.5 0 1 0 15.508 7zm0 9.01a3.505 3.505 0 1 1 3.5-3.51 3.514 3.514 0 0 1-3.5 3.51zm.5-5.01h-1v1h-1v1h1v1h1v-1h1v-1h-1v-1z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Desktop View" class="et-fb-button et-fb-button--inverse" currentitem="false">
												<div class="et-fb-icon et-fb-icon--desktop" style="fill: rgb(112, 195, 169); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g><path d="M20 7H8C7.5 7 7 7.5 7 8v10c0 0.5 0.5 1 1 1h5v1h-1c-0.5 0-1 0.5-1 1s0.5 1 1 1h4c0.5 0 1-0.5 1-1s-0.5-1-1-1h-1v-1h5c0.5 0 1-0.5 1-1V8C21 7.5 20.5 7 20 7zM15 18h-2v-1h2V18zM19 16H9V9h10V16z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Tablet View" class="et-fb-button et-fb-button--inverse" currentitem="false">
												<div class="et-fb-icon et-fb-icon--tablet" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g><path d="M19 7H9C8.5 7 8 7.5 8 8v12c0 0.5 0.5 1 1 1h10c0.5 0 1-0.5 1-1V8C20 7.5 19.5 7 19 7zM15 20h-2v-1h2V20zM18 18h-8V9h8V18z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Phone View" class="et-fb-button et-fb-button--inverse" currentitem="false">
												<div class="et-fb-icon et-fb-icon--phone" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g><path d="M17 7h-6c-0.5 0-1 0.5-1 1v12c0 0.5 0.5 1 1 1h6c0.5 0 1-0.5 1-1V8C18 7.5 17.5 7 17 7zM15 20h-2v-1h2V20zM16 18h-4V9h4V18z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
										</div>
										<div class="et-fb-button-group et-fb-button-group--elevate et-fb-button-group--inverse et-fb-button-group--builder-mode" style="position: relative; left: 10px; margin-right: 10px; opacity: 1;">
											<button type="button" data-tip="Hover Mode" class="et-fb-button et-fb-button--inverse">
												<div class="et-fb-icon et-fb-icon--hover" style="fill: rgb(112, 195, 169); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g fill-rule="evenodd"><path d="M17.1 18.1l-5.7-5.2c-.2-.1-.4 0-.4.2v7.4c0 .4.4.7.8.6l1.4-.6.8 2c.2.5.8.7 1.3.5.5-.2.7-.8.5-1.3l-.8-2 1.9-.8c.3-.3.4-.6.2-.8zM20 10c-.6 0-1-.4-1-1-.6 0-1-.4-1-1s.4-1 1-1c1.1 0 2 .9 2 2 0 .6-.4 1-1 1zM8 10c-.6 0-1-.4-1-1 0-1.1.9-2 2-2 .6 0 1 .4 1 1s-.4 1-1 1c0 .6-.4 1-1 1zM9 20c-1.1 0-2-.9-2-2 0-.6.4-1 1-1s1 .4 1 1c.6 0 1 .4 1 1s-.4 1-1 1zM19 20c-.6 0-1-.4-1-1s.4-1 1-1c0-.6.4-1 1-1s1 .4 1 1c0 1.1-.9 2-2 2zM14.8 9h-1.5c-.6 0-1-.4-1-1s.4-1 1-1h1.5c.6 0 1 .4 1 1s-.5 1-1 1zM20 15c-.6 0-1-.4-1-1v-1c0-.6.4-1 1-1s1 .4 1 1v1c0 .6-.4 1-1 1zM8 15c-.6 0-1-.4-1-1v-1c0-.6.4-1 1-1s1 .4 1 1v1c0 .6-.4 1-1 1z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Click Mode" class="et-fb-button et-fb-button--inverse">
												<div class="et-fb-icon et-fb-icon--click" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g fill-rule="evenodd"><path d="M15 10V8c0-.6-.4-1-1-1s-1 .4-1 1v2c0 .3.2.6.4.8.2 0 .5.1.7.2.5-.1.9-.5.9-1zM20 15c.6 0 1-.4 1-1s-.4-1-1-1h-2c-.4 0-.7.2-.9.6l1.6 1.4H20zM10 13H8c-.6 0-1 .4-1 1s.4 1 1 1h2c.6 0 1-.4 1-1s-.4-1-1-1zM9.8 11.2c.2.2.5.3.7.3s.5-.1.7-.3c.4-.4.4-1 0-1.4l-1-1c-.4-.4-1-.4-1.4 0s-.4 1 0 1.4l1 1zM9.8 16.8l-1.1 1.1c-.4.4-.4 1 0 1.4.2.2.5.3.7.3s.5-.1.7-.3l.9-.9v-1.8c-.4-.2-.9-.1-1.2.2zM17.5 11.5c.3 0 .5-.1.7-.3l1-1c.4-.4.4-1 0-1.4s-1-.4-1.4 0l-1 1c-.4.4-.4 1 0 1.4.2.2.4.3.7.3zM13.4 12.9s-.1-.1-.2-.1-.3.1-.3.3v7.4c0 .3.3.6.6.6h.2l1.4-.6.8 2c.2.4.5.6.9.6.1 0 .3 0 .4-.1.5-.2.7-.8.5-1.3l-.8-2 1.9-.8c.3-.1.3-.5.1-.7l-5.5-5.3z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Grid Mode" class="et-fb-button et-fb-button--inverse">
												<div class="et-fb-icon et-fb-icon--grid" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g><path d="M20 7H8C7.5 7 7 7.5 7 8v12c0 0.5 0.5 1 1 1h12c0.5 0 1-0.5 1-1V8C21 7.5 20.5 7 20 7zM15 9v2h-2V9H15zM15 13v2h-2v-2H15zM9 9h2v2H9V9zM9 13h2v2H9V13zM9 19v-2h2v2H9zM13 19v-2h2v2H13zM19 19h-2v-2h2V19zM19 15h-2v-2h2V15zM19 11h-2V9h2V11z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
										</div>
									</div>
									<button type="button" class="et-fb-button et-fb-button--elevate et-fb-button--large et-fb-button--primary et-fb-button--round et-fb-page-settings-bar__toggle-button et-fb-button--active" style="z-index: 1000; position: absolute; top: 50%; left: 50%; width: 60px; height: 60px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; transform: translate(-50%, -50%) scale(1);">
										<div class="et-fb-icon et-fb-icon--page-settings-main-buttons" style="position: absolute; top: 50%; left: 50%; margin: 0px; transform: translate(-50%, -50%) rotate(0deg); opacity: 0;">
											<div class="et-fb-icon__child et-fb-icon__child--active et-fb-icon__child--animate"></div>
										</div>
										<div class="et-fb-icon et-fb-icon--move" style="fill: rgb(255, 255, 255); width: 48px; min-width: 48px; height: 48px; margin: 0px; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%) rotate(90deg); opacity: 1;">
											<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
												<g><path d="M12.968 17.968a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057l1.835 1.835a.308.308 0 0 0 .437 0l1.836-1.835a.749.749 0 0 0 0-1.057.752.752 0 0 0-1.06 0v-2.994h3a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.056 0l1.836-1.835a.31.31 0 0 0 0-.437l-1.836-1.835a.747.747 0 0 0-1.057 0 .747.747 0 0 0 0 1.057h-3V9.987a1.28 1.28 0 0 0 1.057 0 .749.749 0 0 0 0-1.057l-1.827-1.84a.308.308 0 0 0-.437 0l-1.835 1.836a.747.747 0 0 0 0 1.057.747.747 0 0 0 1.057 0v2.994h-2.99a.747.747 0 0 0 0-1.057.747.747 0 0 0-1.058 0L7.09 13.755a.31.31 0 0 0 0 .437l1.835 1.835a.747.747 0 0 0 1.057 0 .747.747 0 0 0 0-1.057h2.991l-.005 2.998z" fill-rule="evenodd"></path></g>
											</svg>
										</div>
										<canvas height="81" width="81" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
									</button>
									<div class="et-fb-button-group et-fb-page-settings-bar__column et-fb-page-settings-bar__column--main">
										<div class="et-fb-button-group" style="margin-right: 30px; border-radius: 3px 0px 0px 3px; transition: none 0s ease 0s; width: 192px;">
											<button type="button" data-tip="Load From Library" class="et-fb-button et-fb-button--elevate et-fb-button--primary et-fb-button--round et-fb-button--tooltip et-fb-button--toggle-add" currentitem="false" style="position: relative; right: 0px; border-radius: 100px; margin-right: 24px; opacity: 1;">
												<div class="et-fb-icon et-fb-icon--add" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g><path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Save To Library" class="et-fb-button et-fb-button--elevate et-fb-button--primary et-fb-button--round et-fb-button--tooltip et-fb-button--toggle-save" currentitem="false" style="position: relative; right: 0px; border-radius: 100px; margin-right: 24px; opacity: 1;">
												<div class="et-fb-icon et-fb-icon--save" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g><path d="M18.95 9.051a1 1 0 1 0-1.414 1.414 5 5 0 1 1-7.07 0A1 1 0 0 0 9.05 9.051a7 7 0 1 0 9.9.001v-.001zm-5.378 8.235a.5.5 0 0 0 .857 0l2.117-3.528a.5.5 0 0 0-.429-.758H15V8a1 1 0 0 0-2 0v5h-1.117a.5.5 0 0 0-.428.758l2.117 3.528z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Clear Layout" class="et-fb-button et-fb-button--elevate et-fb-button--primary et-fb-button--round et-fb-button--tooltip et-fb-button--toggle-delete" currentitem="false" style="position: relative; right: 0px; border-radius: 100px; margin-right: 24px; opacity: 1;">
												<div class="et-fb-icon et-fb-icon--delete" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g><path d="M19 9h-3V8a1 1 0 0 0-1-1h-2a1 1 0 0 0-1 1v1H9a1 1 0 1 0 0 2h10a1 1 0 0 0 .004-2H19zM9 20c.021.543.457.979 1 1h8c.55-.004.996-.45 1-1v-7H9v7zm2.02-4.985h2v4h-2v-4zm4 0h2v4h-2v-4z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
										</div>
										<div class="et-fb-button-group" style="margin-left: 30px; border-radius: 0px 3px 3px 0px; transition: none 0s ease 0s; width: 192px;">
											<button type="button" data-tip="Page Settings" class="et-fb-button et-fb-button--elevate et-fb-button--primary et-fb-button--round et-fb-button--toggle-setting" currentitem="false" style="position: relative; left: 0px; border-radius: 100px; margin-left: 24px; opacity: 1;">
												<div class="et-fb-icon et-fb-icon--setting" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g><path d="M20.426 13.088l-1.383-.362a.874.874 0 0 1-.589-.514l-.043-.107a.871.871 0 0 1 .053-.779l.721-1.234a.766.766 0 0 0-.116-.917 6.682 6.682 0 0 0-.252-.253.768.768 0 0 0-.917-.116l-1.234.722a.877.877 0 0 1-.779.053l-.107-.044a.87.87 0 0 1-.513-.587l-.362-1.383a.767.767 0 0 0-.73-.567h-.358a.768.768 0 0 0-.73.567l-.362 1.383a.878.878 0 0 1-.513.589l-.107.044a.875.875 0 0 1-.778-.054l-1.234-.722a.769.769 0 0 0-.918.117c-.086.082-.17.166-.253.253a.766.766 0 0 0-.115.916l.721 1.234a.87.87 0 0 1 .053.779l-.043.106a.874.874 0 0 1-.589.514l-1.382.362a.766.766 0 0 0-.567.731v.357a.766.766 0 0 0 .567.731l1.383.362c.266.07.483.26.588.513l.043.107a.87.87 0 0 1-.053.779l-.721 1.233a.767.767 0 0 0 .115.917c.083.087.167.171.253.253a.77.77 0 0 0 .918.116l1.234-.721a.87.87 0 0 1 .779-.054l.107.044a.878.878 0 0 1 .513.589l.362 1.383a.77.77 0 0 0 .731.567h.356a.766.766 0 0 0 .73-.567l.362-1.383a.878.878 0 0 1 .515-.589l.107-.044a.875.875 0 0 1 .778.054l1.234.721c.297.17.672.123.917-.117.087-.082.171-.166.253-.253a.766.766 0 0 0 .116-.917l-.721-1.234a.874.874 0 0 1-.054-.779l.044-.107a.88.88 0 0 1 .589-.513l1.383-.362a.77.77 0 0 0 .567-.731v-.357a.772.772 0 0 0-.569-.724v-.005zm-6.43 3.9a2.986 2.986 0 1 1 2.985-2.986 3 3 0 0 1-2.985 2.987v-.001z" fill-rule="evenodd"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Editing History" class="et-fb-button et-fb-button--elevate et-fb-button--primary et-fb-button--round et-fb-button--toggle-history" currentitem="false" style="position: relative; left: 0px; border-radius: 100px; margin-left: 24px; opacity: 1;">
												<div class="et-fb-icon et-fb-icon--history" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g>
															<path d="M14 6.5C9.9 6.5 6.5 9.9 6.5 14 6.5 18.1 9.9 21.5 14 21.5 18.1 21.5 21.5 18.1 21.5 14 21.5 9.9 18.1 6.5 14 6.5L14 6.5ZM14 19.5C11 19.5 8.5 17 8.5 14 8.5 11 11 8.5 14 8.5 17 8.5 19.5 11 19.5 14 19.5 17 17 19.5 14 19.5L14 19.5Z" fill-rule="evenodd"></path>
															<path d="M17 13L15 13 15 11C15 10.5 14.5 10 14 10 13.5 10 13 10.5 13 11L13 14C13 14.5 13.5 15 14 15L17 15C17.5 15 18 14.5 18 14 18 13.5 17.5 13 17 13L17 13Z" fill-rule="evenodd"></path>
														</g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Portability" class="et-fb-button et-fb-button--elevate et-fb-button--primary et-fb-button--round et-fb-button--tooltip et-fb-button--toggle-portability" currentitem="false" style="position: relative; left: 0px; border-radius: 100px; margin-left: 24px; opacity: 1;">
												<div class="et-fb-icon et-fb-icon--portability" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g>
															<path d="M9.6 20.8c0.2 0.3 0.7 0.3 0.9 0l2.1-3.5c0.2-0.3 0-0.8-0.4-0.8H11V8c0-0.6-0.4-1-1-1C9.4 7 9 7.4 9 8v8.5H7.9c-0.4 0-0.6 0.4-0.4 0.8L9.6 20.8z" fill-rule="evenodd"></path>
															<path d="M18.4 7.2c-0.2-0.3-0.7-0.3-0.9 0l-2.1 3.5c-0.2 0.3 0 0.8 0.4 0.8H17V20c0 0.6 0.4 1 1 1 0.6 0 1-0.4 1-1v-8.5h1.1c0.4 0 0.6-0.4 0.4-0.8L18.4 7.2z" fill-rule="evenodd"></path>
														</g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
										</div>
									</div>
									<div class="et-fb-page-settings-bar__column et-fb-page-settings-bar__column--right" style="right: auto;">
										<div class="et-fb-button-group et-fb-button-group--save-changes" style="position: relative; right: auto; left: 0px; opacity: 1;">
											<button type="button" class="et-fb-button et-fb-button--elevate et-fb-button--round et-fb-button--small et-fb-button--quick-actions">
												<div class="et-fb-icon et-fb-icon--search" style="fill: rgb(76, 88, 102); width: 24px; min-width: 24px; height: 24px; margin: -4px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g><path d="M12.13,5a4.88,4.88,0,0,0-4.18,7.39L5.23,15.11a.78.78,0,0,0,0,1.11l.55.55a.78.78,0,0,0,1.11,0l2.72-2.72A4.88,4.88,0,1,0,12.13,5Zm0,7.75A2.88,2.88,0,1,1,15,9.88,2.87,2.87,0,0,1,12.13,12.75Z"></path></g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" class="et-fb-button et-fb-button--elevate et-fb-button--round et-fb-button--small et-fb-button--help">
												<div class="et-fb-icon et-fb-icon--help" style="fill: rgb(76, 88, 102); width: 24px; min-width: 24px; height: 24px; margin: -4px;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g>
															<circle cx="14" cy="19" r="1"></circle>
															<path d="M13 16a3.17 3.17 0 0 1 1.59-2.68c.74-.46 1.41-.8 1.41-1.82 0-.5-.45-1.5-2-1.5-1.73 0-2 .95-2 1-.12.6-.33 1-1 1-.67 0-1.12-.4-1-1a3.89 3.89 0 0 1 4-3 3.68 3.68 0 0 1 4 3.5 3.72 3.72 0 0 1-2.23 3.5 1.53 1.53 0 0 0-.77 1 .93.93 0 0 1-1 1 .93.93 0 0 1-1-1z"></path>
														</g>
													</svg>
												</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Save Draft" class="et-fb-button et-fb-button--elevate et-fb-button--info et-fb-button--save-draft" currentitem="false">
												<div class="et-fb-icon et-fb-icon--loading" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px auto; display: none;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g>
															<circle class="et-fb-icon__circle et-fb-icon__circle--1" cx="2" cy="2" r="2" transform="translate(4 12)"></circle>
															<circle class="et-fb-icon__circle et-fb-icon__circle--2" cx="2.3" cy="2.7" r="2" transform="rotate(72 4.397 10.865)"></circle>
															<circle class="et-fb-icon__circle et-fb-icon__circle--3" cx="2.3" cy="2.2" r="2" transform="rotate(144 10.216 8.724)"></circle>
															<circle class="et-fb-icon__circle et-fb-icon__circle--4" cx="2.6" cy="2" r="2" transform="rotate(-144 14.235 7.453)"></circle>
															<circle class="et-fb-icon__circle et-fb-icon__circle--5" cx="2.8" cy="2.1" r="2" transform="rotate(-72 20.635 5.838)"></circle>
														</g>
													</svg>
												</div>
												<div>Save Draft</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
											<button type="button" data-tip="Save" class="et-fb-button et-fb-button--elevate et-fb-button--success et-fb-button--publish" currentitem="false">
												<div class="et-fb-icon et-fb-icon--loading" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px auto; display: none;">
													<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
														<g>
															<circle class="et-fb-icon__circle et-fb-icon__circle--1" cx="2" cy="2" r="2" transform="translate(4 12)"></circle>
															<circle class="et-fb-icon__circle et-fb-icon__circle--2" cx="2.3" cy="2.7" r="2" transform="rotate(72 4.397 10.865)"></circle>
															<circle class="et-fb-icon__circle et-fb-icon__circle--3" cx="2.3" cy="2.2" r="2" transform="rotate(144 10.216 8.724)"></circle>
															<circle class="et-fb-icon__circle et-fb-icon__circle--4" cx="2.6" cy="2" r="2" transform="rotate(-144 14.235 7.453)"></circle>
															<circle class="et-fb-icon__circle et-fb-icon__circle--5" cx="2.8" cy="2.1" r="2" transform="rotate(-72 20.635 5.838)"></circle>
														</g>
													</svg>
												</div>
												<div>Publish</div>
												<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
											</button>
										</div>
									</div>
								</div>
							</div>
							<div id="modals">
								<!-- builder Settings Modal -->
								<div id="agsdg-builder-modal" class="et-core-modal-overlay et-core-modal-two-buttons et-builder-exit-modal et-core-modal-disabled-scrollbar">
									<div class="et-fb-modal et-fb-modal--app et-fb-modal--tabs-count-3" style="width: 35vw; opacity: 1; transform: scale(1); top: auto; bottom: 10vh; left: 2vw;">
										<div class="et-fb-modal__header">
											<span class="et-fb-modal__title">Builder Settings</span>
											<div class="et-fb-button-group et-fb-button-group--block">
												<button type="button" data-tip="Discard All Changes" class="et-fb-button et-fb-button--inverse" currentitem="false">
													<div class="et-fb-icon et-fb-icon--exit" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
														<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
															<g><path d="M19.71 16.857l-2.85-2.854 2.85-2.854c.39-.395.39-1.03 0-1.426l-1.43-1.427a1 1 0 0 0-1.42 0L14 11.15l-2.85-2.854a1.013 1.013 0 0 0-1.43 0L8.3 9.723a1 1 0 0 0 0 1.426l2.85 2.854-2.85 2.853a1 1 0 0 0 0 1.427l1.42 1.427a1.011 1.011 0 0 0 1.43 0L14 16.856l2.86 2.854a1 1 0 0 0 1.42 0l1.43-1.427c.39-.395.39-1.03 0-1.426z" fill-rule="evenodd"></path></g>
														</svg>
													</div>
													<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
												</button>
											</div>
										</div>
										<div class="et-fb-modal__content" style="height: 80vh; max-height: 800px;">
											<div class="et-fb-tabs__panel et-fb-tabs__panel--active et-fb-tabs__panel--null" role="tabpanel">
												<div class="et-fb-form">
													<div class="et-fb-form__group">
														<span class="et-fb-form__label">
															<span class="et-fb-form__label-text">Customize Builder Settings Toolbar</span>
														</span>
														<div class="et-fb-button-group">
															<button type="button" class="et-fb-button" style="background: rgb(242, 246, 249);">
																<div class="et-fb-icon et-fb-icon--wireframe" style="fill: rgb(92, 105, 120); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g>
																			<path d="M20 7H8C7.5 7 7 7.5 7 8v4c0 0.5 0.5 1 1 1h12c0.5 0 1-0.5 1-1V8C21 7.5 20.5 7 20 7zM19 11H9V9h10V11z" fill-rule="evenodd"></path>
																			<path d="M12 15H8c-0.5 0-1 0.5-1 1v4c0 0.5 0.5 1 1 1h4c0.5 0 1-0.5 1-1v-4C13 15.5 12.5 15 12 15zM11 19H9v-2h2V19z" fill-rule="evenodd"></path>
																			<path d="M20 15h-4c-0.5 0-1 0.5-1 1v4c0 0.5 0.5 1 1 1h4c0.5 0 1-0.5 1-1v-4C21 15.5 20.5 15 20 15zM19 19h-2v-2h2V19z" fill-rule="evenodd"></path>
																		</g>
																	</svg>
																</div>
																<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
															</button>
															<button type="button" class="et-fb-button" style="background: rgb(242, 246, 249);">
																<div class="et-fb-icon et-fb-icon--zoom-in" style="fill: rgb(92, 105, 120); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M15.508 7a5.511 5.511 0 0 0-5.505 5.5 5.426 5.426 0 0 0 .847 2.92l-3.737 2.67c-.389.39.32 1.74.708 2.13.39.39 1.764 1.06 2.153.67l2.646-3.71A5.5 5.5 0 1 0 15.508 7zm0 9.01a3.505 3.505 0 1 1 3.5-3.51 3.514 3.514 0 0 1-3.5 3.51zm.5-5.01h-1v1h-1v1h1v1h1v-1h1v-1h-1v-1z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
																<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
															</button>
															<button type="button" class="et-fb-button" style="background: rgb(242, 246, 249);">
																<div class="et-fb-icon et-fb-icon--desktop" style="fill: rgb(92, 105, 120); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M20 7H8C7.5 7 7 7.5 7 8v10c0 0.5 0.5 1 1 1h5v1h-1c-0.5 0-1 0.5-1 1s0.5 1 1 1h4c0.5 0 1-0.5 1-1s-0.5-1-1-1h-1v-1h5c0.5 0 1-0.5 1-1V8C21 7.5 20.5 7 20 7zM15 18h-2v-1h2V18zM19 16H9V9h10V16z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
																<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
															</button>
															<button type="button" class="et-fb-button" style="background: rgb(242, 246, 249);">
																<div class="et-fb-icon et-fb-icon--tablet" style="fill: rgb(92, 105, 120); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M19 7H9C8.5 7 8 7.5 8 8v12c0 0.5 0.5 1 1 1h10c0.5 0 1-0.5 1-1V8C20 7.5 19.5 7 19 7zM15 20h-2v-1h2V20zM18 18h-8V9h8V18z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
																<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
															</button>
															<button type="button" class="et-fb-button" style="background: rgb(242, 246, 249);">
																<div class="et-fb-icon et-fb-icon--phone" style="fill: rgb(92, 105, 120); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M17 7h-6c-0.5 0-1 0.5-1 1v12c0 0.5 0.5 1 1 1h6c0.5 0 1-0.5 1-1V8C18 7.5 17.5 7 17 7zM15 20h-2v-1h2V20zM16 18h-4V9h4V18z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
																<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
															</button>
														</div>
														<div class="et-fb-button-group" style="margin-left: 3px;">
															<button type="button" class="et-fb-button" style="background: rgb(242, 246, 249);">
																<div class="et-fb-icon et-fb-icon--hover" style="fill: rgb(190, 201, 214); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g fill-rule="evenodd"><path d="M17.1 18.1l-5.7-5.2c-.2-.1-.4 0-.4.2v7.4c0 .4.4.7.8.6l1.4-.6.8 2c.2.5.8.7 1.3.5.5-.2.7-.8.5-1.3l-.8-2 1.9-.8c.3-.3.4-.6.2-.8zM20 10c-.6 0-1-.4-1-1-.6 0-1-.4-1-1s.4-1 1-1c1.1 0 2 .9 2 2 0 .6-.4 1-1 1zM8 10c-.6 0-1-.4-1-1 0-1.1.9-2 2-2 .6 0 1 .4 1 1s-.4 1-1 1c0 .6-.4 1-1 1zM9 20c-1.1 0-2-.9-2-2 0-.6.4-1 1-1s1 .4 1 1c.6 0 1 .4 1 1s-.4 1-1 1zM19 20c-.6 0-1-.4-1-1s.4-1 1-1c0-.6.4-1 1-1s1 .4 1 1c0 1.1-.9 2-2 2zM14.8 9h-1.5c-.6 0-1-.4-1-1s.4-1 1-1h1.5c.6 0 1 .4 1 1s-.5 1-1 1zM20 15c-.6 0-1-.4-1-1v-1c0-.6.4-1 1-1s1 .4 1 1v1c0 .6-.4 1-1 1zM8 15c-.6 0-1-.4-1-1v-1c0-.6.4-1 1-1s1 .4 1 1v1c0 .6-.4 1-1 1z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
																<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
															</button>
															<button type="button" class="et-fb-button" style="background: rgb(242, 246, 249);">
																<div class="et-fb-icon et-fb-icon--click" style="fill: rgb(190, 201, 214); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g fill-rule="evenodd"><path d="M15 10V8c0-.6-.4-1-1-1s-1 .4-1 1v2c0 .3.2.6.4.8.2 0 .5.1.7.2.5-.1.9-.5.9-1zM20 15c.6 0 1-.4 1-1s-.4-1-1-1h-2c-.4 0-.7.2-.9.6l1.6 1.4H20zM10 13H8c-.6 0-1 .4-1 1s.4 1 1 1h2c.6 0 1-.4 1-1s-.4-1-1-1zM9.8 11.2c.2.2.5.3.7.3s.5-.1.7-.3c.4-.4.4-1 0-1.4l-1-1c-.4-.4-1-.4-1.4 0s-.4 1 0 1.4l1 1zM9.8 16.8l-1.1 1.1c-.4.4-.4 1 0 1.4.2.2.5.3.7.3s.5-.1.7-.3l.9-.9v-1.8c-.4-.2-.9-.1-1.2.2zM17.5 11.5c.3 0 .5-.1.7-.3l1-1c.4-.4.4-1 0-1.4s-1-.4-1.4 0l-1 1c-.4.4-.4 1 0 1.4.2.2.4.3.7.3zM13.4 12.9s-.1-.1-.2-.1-.3.1-.3.3v7.4c0 .3.3.6.6.6h.2l1.4-.6.8 2c.2.4.5.6.9.6.1 0 .3 0 .4-.1.5-.2.7-.8.5-1.3l-.8-2 1.9-.8c.3-.1.3-.5.1-.7l-5.5-5.3z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
																<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
															</button>
															<button type="button" class="et-fb-button" style="background: rgb(242, 246, 249);">
																<div class="et-fb-icon et-fb-icon--grid" style="fill: rgb(190, 201, 214); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M20 7H8C7.5 7 7 7.5 7 8v12c0 0.5 0.5 1 1 1h12c0.5 0 1-0.5 1-1V8C21 7.5 20.5 7 20 7zM15 9v2h-2V9H15zM15 13v2h-2v-2H15zM9 9h2v2H9V9zM9 13h2v2H9V13zM9 19v-2h2v2H9zM13 19v-2h2v2H13zM19 19h-2v-2h2V19zM19 15h-2v-2h2V15zM19 11h-2V9h2V11z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
																<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
															</button>
														</div>
													</div>
													<div class="et-fb-form__group">
														<span class="et-fb-form__label">
															<span class="et-fb-form__label-text">Builder Default View Mode</span>
														</span>
														<div class="et-fb-settings-option">
															<div class="et-fb-settings-option-container">
																<select class="et-core-control-select et-fb-settings-option-select" name="builder_view_mode" id="et-fb-builder_view_mode">
																	<option value="desktop">Desktop View</option>
																	<option value="tablet">Tablet View</option>
																	<option value="phone">Phone View</option>
																	<option value="wireframe">Wireframe View</option>
																</select>
															</div>
														</div>
													</div>
													<div class="et-fb-form__group">
														<span class="et-fb-form__label">
															<span class="et-fb-form__label-text">Builder Default Interaction Mode</span>
														</span>
														<div class="et-fb-settings-option">
															<div class="et-fb-settings-option-container">
																<select class="et-core-control-select et-fb-settings-option-select" name="view_mode" id="et-fb-view_mode">
																	<option value="0">Hover Mode</option>
																	<option value="1">Click Mode</option>
																	<option value="2">Grid Mode</option>
																</select>
															</div>
														</div>
													</div>
													<div class="et-fb-form__group">
														<span class="et-fb-form__label">
															<span class="et-fb-form__label-text">History State Interval</span>
														</span>
														<div class="et-fb-settings-option">
															<div class="et-fb-settings-option-container">
																<select class="et-core-control-select et-fb-settings-option-select" name="history" id="et-fb-history">
																	<option value="0">After Every Action</option>
																	<option value="1">After Every 10th Action</option>
																	<option value="2">After Every 20th Action</option>
																	<option value="3">After Every 30th Action</option>
																	<option value="4">After Every 40th Action</option>
																</select>
															</div>
														</div>
													</div>
													<div class="et-fb-form__group">
														<span class="et-fb-form__label">
															<span class="et-fb-form__label-text">Settings Modal Default Position</span>
														</span>
														<div class="et-fb-settings-option">
															<div class="et-fb-settings-option-container">
																<select class="et-core-control-select et-fb-settings-option-select" name="modal_position" id="et-fb-modal_position">
																	<option value="0">Last Used Position</option>
																	<option value="1">Floating Minimum Size</option>
																	<option value="2">Fullscreen</option>
																	<option value="3">Fixed Left Sidebar</option>
																	<option value="4">Fixed Right Sidebar</option>
																	<option value="5">Fixed Bottom Panel</option>
																</select>
															</div>
														</div>
													</div>
													<div class="et-fb-form__group">
														<span class="et-fb-form__label">
															<span class="et-fb-form__label-text">Page Creation Flow</span>
														</span>
														<div class="et-fb-settings-option">
															<div class="et-fb-settings-option-container">
																<select class="et-core-control-select et-fb-settings-option-select" name="page_creation" id="et-fb-page_creation">
																	<option value="0">Give Me A Choice</option>
																	<option value="1">Build From Scratch</option>
																	<option value="2">Load Premade Layout</option>
																	<option value="3">Clone Existing Page</option>
																</select>
															</div>
														</div>
													</div>
													<div class="et-fb-form__group">
														<span class="et-fb-form__label">
															<span class="et-fb-form__label-text">Builder Interface Animations</span>
														</span>
														<div class="et-fb-settings-option">
															<div class="et-fb-settings-option-container">
																<div class="et-core-control-toggle et-core-control-toggle--on">
																	<div class="et-core-control-toggle__label et-core-control-toggle__label--on">
																		<div class="et-core-control-toggle__text">On</div>
																		<div class="et-core-control-toggle__handle"></div>
																	</div>
																	<div class="et-core-control-toggle__label et-core-control-toggle__label--off">
																		<div class="et-core-control-toggle__text">Off</div>
																		<div class="et-core-control-toggle__handle"></div>
																	</div>
																	<input type="hidden" id="et-fb-undefined" value="on">
																</div>
															</div>
														</div>
													</div>
													<div class="et-fb-form__group">
														<span class="et-fb-form__label">
															<span class="et-fb-form__label-text">Show Disabled Modules At 50% Opacity</span>
														</span>
														<div class="et-fb-settings-option">
															<div class="et-fb-settings-option-container">
																<div class="et-core-control-toggle et-core-control-toggle--on">
																	<div class="et-core-control-toggle__label et-core-control-toggle__label--on">
																		<div class="et-core-control-toggle__text">On</div>
																		<div class="et-core-control-toggle__handle"></div>
																	</div>
																	<div class="et-core-control-toggle__label et-core-control-toggle__label--off">
																		<div class="et-core-control-toggle__text">Off</div>
																		<div class="et-core-control-toggle__handle"></div>
																	</div>
																	<input type="hidden" id="et-fb-undefined" value="on">
																</div>
															</div>
														</div>
													</div>
													<div class="et-fb-form__group">
														<span class="et-fb-form__label">
															<span class="et-fb-form__label-text">Group Settings Into Closed Toggles</span>
														</span>
														<div class="et-fb-settings-option">
															<div class="et-fb-settings-option-container">
																<div class="et-core-control-toggle et-core-control-toggle--on">
																	<div class="et-core-control-toggle__label et-core-control-toggle__label--on">
																		<div class="et-core-control-toggle__text">On</div>
																		<div class="et-core-control-toggle__handle"></div>
																	</div>
																	<div class="et-core-control-toggle__label et-core-control-toggle__label--off">
																		<div class="et-core-control-toggle__text">Off</div>
																		<div class="et-core-control-toggle__handle"></div>
																	</div>
																	<input type="hidden" id="et-fb-undefined" value="on">
																</div>
															</div>
														</div>
													</div>
													<div class="et-fb-form__group">
														<span class="et-fb-form__label">
															<span class="et-fb-form__label-text">Add Placeholder Content To New Modules</span>
														</span>
														<div class="et-fb-settings-option">
															<div class="et-fb-settings-option-container">
																<div class="et-core-control-toggle et-core-control-toggle--on">
																	<div class="et-core-control-toggle__label et-core-control-toggle__label--on">
																		<div class="et-core-control-toggle__text">On</div>
																		<div class="et-core-control-toggle__handle"></div>
																	</div>
																	<div class="et-core-control-toggle__label et-core-control-toggle__label--off">
																		<div class="et-core-control-toggle__text">Off</div>
																		<div class="et-core-control-toggle__handle"></div>
																	</div>
																	<input type="hidden" id="et-fb-undefined" value="on">
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- warning modal -->
								<div id="agsdg-warning-modal" class="et-core-modal-overlay et-core-modal-two-buttons et-builder-exit-modal et-core-modal-disabled-scrollbar">
									<div class="et-core-modal" style="top: 40vh; left: 30vw;">
										<div class="et-core-modal-header">
											<h3 class="et-core-modal-title">You Have Unsaved Changes</h3>
											<a href="#" class="et-core-modal-close" data-et-core-modal="close"></a>
										</div>
										<div class="et-core-modal-content">
											<p>Your page contains changes that have not been saved. If you close the builder without saving, these changes will be lost. If you would like to leave the builder and save all changes, please select <strong>Save &amp; Exit</strong>. If you would like to discard all recent changes, choose <strong>Discard &amp; Exit</strong>.</p>
										</div>
										<div class="et_pb_prompt_buttons">
											<br>
											<span class="spinner"></span>
											<a href="#" class="et-core-modal-action et-core-modal-action-secondary">Discard &amp; Exit</a>
											<a href="#" class="et-core-modal-action">Save &amp; Exit</a>
										</div>
									</div>
								</div>
								<!-- insert modal -->
								<div id="agsdg-insert-modal" class="et-core-modal-overlay et-core-modal-two-buttons et-builder-exit-modal et-core-modal-disabled-scrollbar">
									<div id="et-fb-settings-column" class="et-fb-settings et-fb-tooltip-modal et-fb-tooltip-modal--add_module et-fb-modal-settings--container et-fb-modal-settings--modules_all et-fb-modal-add-module-container et_fb_add_row_modal" tabindex="0" style="top: 200px; left: 40%; width: 410px; height: 504px; position: absolute; opacity: 1; transform: scale(1); z-index: 100035">
										<div class="et-fb-tooltip-helper-container">
											<div class="et-fb-module-settings">
												<h3 class="et-fb-settings-heading">Insert Row
													<button type="button" class="et-fb-button et-fb-button--primary et-fb-settings-button--close">
														<div class="et-fb-icon et-fb-icon--close" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
															<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																<g><path d="M15.59 14l4.08-4.082a1.124 1.124 0 0 0-1.587-1.588L14 12.411 9.918 8.329A1.124 1.124 0 0 0 8.33 9.92L12.411 14l-4.082 4.082a1.124 1.124 0 0 0 1.59 1.589L14 15.589l4.082 4.082a1.124 1.124 0 0 0 1.589-1.59L15.589 14h.001z" fill-rule="evenodd"></path></g>
															</svg>
														</div>
														<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
													</button>
												</h3>
												<div class="et-fb-main-settings et-fb-main-settings--add_new_module">
													<ul class="et-fb-settings-tabs-nav">
														<li class="et-fb-settings-options_tab_modules_all et-fb-settings-tabs-nav-item et-fb-settings-tabs-nav-item--active"><a href="#" class="modules_all">New Row</a></li>
														<li class="et-fb-settings-options_tab_modules_library et-fb-settings-tabs-nav-item"><a href="#" class="modules_library">Add From Library</a></li>
													</ul>
													<div class="et-fb-settings-options-wrap" style="max-height: 404px;">
														<div class="et-fb-settings-options">
															<div class="et-fb-settings-options-tab et-fb-all-modules et-fb-modules-list et-fb-settings-options et-fb-settings-options-tab-modules_all et-fb-settings-options-tab--active">
																<ul class="et-fb-columns-layout"><li class="" data-layout="4_4"><span data-layout="4_4" class="column-block-wrap" style="width: 100%;"><span class="column-block" data-layout="4_4" data-section="regular">4/4</span></span></li><li class="" data-layout="1_2,1_2"><span data-layout="1_2,1_2" class="column-block-wrap" style="width: 50%;"><span class="column-block" data-layout="1_2,1_2" data-section="regular">1/2</span></span><span data-layout="1_2,1_2" class="column-block-wrap" style="width: 50%;"><span class="column-block" data-layout="1_2,1_2" data-section="regular">1/2</span></span></li><li class="" data-layout="1_3,1_3,1_3"><span data-layout="1_3,1_3,1_3" class="column-block-wrap" style="width: 33.3333%;"><span class="column-block" data-layout="1_3,1_3,1_3" data-section="regular">1/3</span></span><span data-layout="1_3,1_3,1_3" class="column-block-wrap" style="width: 33.3333%;"><span class="column-block" data-layout="1_3,1_3,1_3" data-section="regular">1/3</span></span><span data-layout="1_3,1_3,1_3" class="column-block-wrap" style="width: 33.3333%;"><span class="column-block" data-layout="1_3,1_3,1_3" data-section="regular">1/3</span></span></li><li class="" data-layout="1_4,1_4,1_4,1_4"><span data-layout="1_4,1_4,1_4,1_4" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="1_4,1_4,1_4,1_4" data-section="regular">1/4</span></span><span data-layout="1_4,1_4,1_4,1_4" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="1_4,1_4,1_4,1_4" data-section="regular">1/4</span></span><span data-layout="1_4,1_4,1_4,1_4" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="1_4,1_4,1_4,1_4" data-section="regular">1/4</span></span><span data-layout="1_4,1_4,1_4,1_4" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="1_4,1_4,1_4,1_4" data-section="regular">1/4</span></span></li><li class="" data-layout="1_5,1_5,1_5,1_5,1_5"><span data-layout="1_5,1_5,1_5,1_5,1_5" class="column-block-wrap" style="width: 20%;"><span class="column-block" data-layout="1_5,1_5,1_5,1_5,1_5" data-section="regular">1/5</span></span><span data-layout="1_5,1_5,1_5,1_5,1_5" class="column-block-wrap" style="width: 20%;"><span class="column-block" data-layout="1_5,1_5,1_5,1_5,1_5" data-section="regular">1/5</span></span><span data-layout="1_5,1_5,1_5,1_5,1_5" class="column-block-wrap" style="width: 20%;"><span class="column-block" data-layout="1_5,1_5,1_5,1_5,1_5" data-section="regular">1/5</span></span><span data-layout="1_5,1_5,1_5,1_5,1_5" class="column-block-wrap" style="width: 20%;"><span class="column-block" data-layout="1_5,1_5,1_5,1_5,1_5" data-section="regular">1/5</span></span><span data-layout="1_5,1_5,1_5,1_5,1_5" class="column-block-wrap" style="width: 20%;"><span class="column-block" data-layout="1_5,1_5,1_5,1_5,1_5" data-section="regular">1/5</span></span></li><li class="" data-layout="1_6,1_6,1_6,1_6,1_6,1_6"><span data-layout="1_6,1_6,1_6,1_6,1_6,1_6" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_6,1_6,1_6,1_6,1_6,1_6" data-section="regular">1/6</span></span><span data-layout="1_6,1_6,1_6,1_6,1_6,1_6" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_6,1_6,1_6,1_6,1_6,1_6" data-section="regular">1/6</span></span><span data-layout="1_6,1_6,1_6,1_6,1_6,1_6" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_6,1_6,1_6,1_6,1_6,1_6" data-section="regular">1/6</span></span><span data-layout="1_6,1_6,1_6,1_6,1_6,1_6" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_6,1_6,1_6,1_6,1_6,1_6" data-section="regular">1/6</span></span><span data-layout="1_6,1_6,1_6,1_6,1_6,1_6" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_6,1_6,1_6,1_6,1_6,1_6" data-section="regular">1/6</span></span><span data-layout="1_6,1_6,1_6,1_6,1_6,1_6" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_6,1_6,1_6,1_6,1_6,1_6" data-section="regular">1/6</span></span></li><li class="" data-layout="2_5,3_5"><span data-layout="2_5,3_5" class="column-block-wrap" style="width: 40%;"><span class="column-block" data-layout="2_5,3_5" data-section="regular">2/5</span></span><span data-layout="2_5,3_5" class="column-block-wrap" style="width: 60%;"><span class="column-block" data-layout="2_5,3_5" data-section="regular">3/5</span></span></li><li class="" data-layout="3_5,2_5"><span data-layout="3_5,2_5" class="column-block-wrap" style="width: 60%;"><span class="column-block" data-layout="3_5,2_5" data-section="regular">3/5</span></span><span data-layout="3_5,2_5" class="column-block-wrap" style="width: 40%;"><span class="column-block" data-layout="3_5,2_5" data-section="regular">2/5</span></span></li><li class="" data-layout="1_3,2_3"><span data-layout="1_3,2_3" class="column-block-wrap" style="width: 33.3333%;"><span class="column-block" data-layout="1_3,2_3" data-section="regular">1/3</span></span><span data-layout="1_3,2_3" class="column-block-wrap" style="width: 66.6667%;"><span class="column-block" data-layout="1_3,2_3" data-section="regular">2/3</span></span></li><li class="" data-layout="2_3,1_3"><span data-layout="2_3,1_3" class="column-block-wrap" style="width: 66.6667%;"><span class="column-block" data-layout="2_3,1_3" data-section="regular">2/3</span></span><span data-layout="2_3,1_3" class="column-block-wrap" style="width: 33.3333%;"><span class="column-block" data-layout="2_3,1_3" data-section="regular">1/3</span></span></li><li class="" data-layout="1_4,3_4"><span data-layout="1_4,3_4" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="1_4,3_4" data-section="regular">1/4</span></span><span data-layout="1_4,3_4" class="column-block-wrap" style="width: 75%;"><span class="column-block" data-layout="1_4,3_4" data-section="regular">3/4</span></span></li><li class="" data-layout="3_4,1_4"><span data-layout="3_4,1_4" class="column-block-wrap" style="width: 75%;"><span class="column-block" data-layout="3_4,1_4" data-section="regular">3/4</span></span><span data-layout="3_4,1_4" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="3_4,1_4" data-section="regular">1/4</span></span></li><li class="" data-layout="1_4,1_2,1_4"><span data-layout="1_4,1_2,1_4" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="1_4,1_2,1_4" data-section="regular">1/4</span></span><span data-layout="1_4,1_2,1_4" class="column-block-wrap" style="width: 50%;"><span class="column-block" data-layout="1_4,1_2,1_4" data-section="regular">1/2</span></span><span data-layout="1_4,1_2,1_4" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="1_4,1_2,1_4" data-section="regular">1/4</span></span></li><li class="" data-layout="1_5,3_5,1_5"><span data-layout="1_5,3_5,1_5" class="column-block-wrap" style="width: 20%;"><span class="column-block" data-layout="1_5,3_5,1_5" data-section="regular">1/5</span></span><span data-layout="1_5,3_5,1_5" class="column-block-wrap" style="width: 60%;"><span class="column-block" data-layout="1_5,3_5,1_5" data-section="regular">3/5</span></span><span data-layout="1_5,3_5,1_5" class="column-block-wrap" style="width: 20%;"><span class="column-block" data-layout="1_5,3_5,1_5" data-section="regular">1/5</span></span></li><li class="" data-layout="1_4,1_4,1_2"><span data-layout="1_4,1_4,1_2" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="1_4,1_4,1_2" data-section="regular">1/4</span></span><span data-layout="1_4,1_4,1_2" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="1_4,1_4,1_2" data-section="regular">1/4</span></span><span data-layout="1_4,1_4,1_2" class="column-block-wrap" style="width: 50%;"><span class="column-block" data-layout="1_4,1_4,1_2" data-section="regular">1/2</span></span></li><li class="" data-layout="1_2,1_4,1_4"><span data-layout="1_2,1_4,1_4" class="column-block-wrap" style="width: 50%;"><span class="column-block" data-layout="1_2,1_4,1_4" data-section="regular">1/2</span></span><span data-layout="1_2,1_4,1_4" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="1_2,1_4,1_4" data-section="regular">1/4</span></span><span data-layout="1_2,1_4,1_4" class="column-block-wrap" style="width: 25%;"><span class="column-block" data-layout="1_2,1_4,1_4" data-section="regular">1/4</span></span></li><li class="" data-layout="1_5,1_5,3_5"><span data-layout="1_5,1_5,3_5" class="column-block-wrap" style="width: 20%;"><span class="column-block" data-layout="1_5,1_5,3_5" data-section="regular">1/5</span></span><span data-layout="1_5,1_5,3_5" class="column-block-wrap" style="width: 20%;"><span class="column-block" data-layout="1_5,1_5,3_5" data-section="regular">1/5</span></span><span data-layout="1_5,1_5,3_5" class="column-block-wrap" style="width: 60%;"><span class="column-block" data-layout="1_5,1_5,3_5" data-section="regular">3/5</span></span></li><li class="" data-layout="3_5,1_5,1_5"><span data-layout="3_5,1_5,1_5" class="column-block-wrap" style="width: 60%;"><span class="column-block" data-layout="3_5,1_5,1_5" data-section="regular">3/5</span></span><span data-layout="3_5,1_5,1_5" class="column-block-wrap" style="width: 20%;"><span class="column-block" data-layout="3_5,1_5,1_5" data-section="regular">1/5</span></span><span data-layout="3_5,1_5,1_5" class="column-block-wrap" style="width: 20%;"><span class="column-block" data-layout="3_5,1_5,1_5" data-section="regular">1/5</span></span></li><li class="" data-layout="1_6,1_6,1_6,1_2"><span data-layout="1_6,1_6,1_6,1_2" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_6,1_6,1_6,1_2" data-section="regular">1/6</span></span><span data-layout="1_6,1_6,1_6,1_2" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_6,1_6,1_6,1_2" data-section="regular">1/6</span></span><span data-layout="1_6,1_6,1_6,1_2" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_6,1_6,1_6,1_2" data-section="regular">1/6</span></span><span data-layout="1_6,1_6,1_6,1_2" class="column-block-wrap" style="width: 50%;"><span class="column-block" data-layout="1_6,1_6,1_6,1_2" data-section="regular">1/2</span></span></li><li class="" data-layout="1_2,1_6,1_6,1_6"><span data-layout="1_2,1_6,1_6,1_6" class="column-block-wrap" style="width: 50%;"><span class="column-block" data-layout="1_2,1_6,1_6,1_6" data-section="regular">1/2</span></span><span data-layout="1_2,1_6,1_6,1_6" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_2,1_6,1_6,1_6" data-section="regular">1/6</span></span><span data-layout="1_2,1_6,1_6,1_6" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_2,1_6,1_6,1_6" data-section="regular">1/6</span></span><span data-layout="1_2,1_6,1_6,1_6" class="column-block-wrap" style="width: 16.6667%;"><span class="column-block" data-layout="1_2,1_6,1_6,1_6" data-section="regular">1/6</span></span></li></ul>
															</div>
															<div class="et-fb-settings-options-tab et-fb-all-modules et-fb-modules-list et-fb-settings-options et-fb-settings-options-tab-modules_library">
																<div class="et-fb-modules-filters">
																	<select class="et-core-control-select et-fb-settings-option-select" name="library_category" id="et-fb-library_category">
																		<option value="all">All Categories</option>
																	</select>
																	<input class="et-fb-settings-option-input et-fb-main-setting regular-text" type="text" name="filterByTitle" id="et-fb-filterByTitle" placeholder="Search..." value="">
																</div>
																<ul>You have not saved any items to your NV Web Dev Library yet. Once an item has been saved to your library, it will appear here for easy use.</ul>
																<div class="et-fb-preloader">
																	<button type="button" class="et-fb-button et-fb-help-button">
																		<div class="et-fb-icon et-fb-icon--help-circle" style="fill: rgb(43, 135, 218); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																			<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																				<g><path d="M14 22a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm0-3.6a.8.8 0 1 0 0-1.6.8.8 0 0 0 0 1.6zm-.8-3.2a.744.744 0 0 0 .8.8.744.744 0 0 0 .8-.8c.08-.343.305-.634.616-.8a2.976 2.976 0 0 0 1.784-2.8A2.944 2.944 0 0 0 14 8.8a3.112 3.112 0 0 0-3.2 2.4c-.096.48.264.8.8.8s.704-.32.8-.8c0-.04.216-.8 1.6-.8 1.24 0 1.6.8 1.6 1.2 0 .816-.536 1.088-1.128 1.456A2.536 2.536 0 0 0 13.2 15.2z"></path></g>
																			</svg>
																		</div>Help
																	</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- settings modal -->
								<div id="agsdg-settings-modal" class="et-core-modal-overlay et-core-modal-two-buttons et-builder-exit-modal et-core-modal-disabled-scrollbar">
									<div class="et-fb-modal et-fb-modal--draggable et-fb-modal--footer-buttons et-fb-modal__module-settings et-fb-modal--tabs-count-3" style="width: 50vw; opacity: 1; transform: scale(1); right: auto; bottom: auto; left: 20vw;">
										<div class="et-fb-modal__header">
											<span class="et-fb-modal__title">Page Settings</span>
											<div class="et-fb-button-group et-fb-button-group--block">
												<button type="button" data-tip="Expand Modal" class="et-fb-button et-fb-button--primary" currentitem="false">
													<div class="et-fb-icon et-fb-icon--expand" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
														<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
															<g>
																<path d="M17 16L17 12C17 11.5 16.5 11 16 11L12 11C11.5 11 11 11.5 11 12L11 16C11 16.5 11.5 17 12 17L16 17C16.5 17 17 16.5 17 16L17 16ZM15 15L13 15 13 13 15 13 15 15 15 15Z" fill-rule="evenodd"></path>
																<path d="M8.5 12C8.8 12 9 11.8 9 11.5L9 9 11.5 9C11.8 9 12 8.8 12 8.5 12 8.2 11.8 8 11.5 8L8.5 8C8.2 8 8 8.2 8 8.5L8 11.5C8 11.8 8.2 12 8.5 12L8.5 12Z" fill-rule="evenodd"></path>
																<path d="M19.5 8L16.5 8C16.2 8 16 8.2 16 8.5 16 8.8 16.2 9 16.5 9L19 9 19 11.5C19 11.8 19.2 12 19.5 12 19.8 12 20 11.8 20 11.5L20 8.5C20 8.2 19.8 8 19.5 8L19.5 8Z" fill-rule="evenodd"></path>
																<path d="M11.5 19L9 19 9 16.5C9 16.2 8.8 16 8.5 16 8.2 16 8 16.2 8 16.5L8 19.5C8 19.8 8.2 20 8.5 20L11.5 20C11.8 20 12 19.8 12 19.5 12 19.2 11.8 19 11.5 19L11.5 19Z" fill-rule="evenodd"></path>
																<path d="M19.5 16C19.2 16 19 16.2 19 16.5L19 19 16.5 19C16.2 19 16 19.2 16 19.5 16 19.8 16.2 20 16.5 20L19.5 20C19.8 20 20 19.8 20 19.5L20 16.5C20 16.2 19.8 16 19.5 16L19.5 16Z" fill-rule="evenodd"></path>
															</g>
														</svg>
													</div>
													<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
												</button>
												<button type="button" data-tip="Snap to Left" class="et-fb-button et-fb-button--primary" currentitem="false">
													<div class="et-fb-icon et-fb-icon--sidebar" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
														<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
															<g><path d="M19 8L9 8C8.5 8 8 8.5 8 9L8 19C8 19.5 8.5 20 9 20L19 20C19.5 20 20 19.5 20 19L20 9C20 8.5 19.5 8 19 8L19 8ZM10 10L12 10 12 12 10 12 10 10 10 10ZM10 13L12 13 12 15 10 15 10 13 10 13ZM10 18L10 16 12 16 12 18 10 18 10 18ZM18 18L14 18 14 10 18 10 18 18 18 18Z" fill-rule="evenodd"></path></g>
														</svg>
													</div>
													<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
												</button>
											</div>
										</div>
										<div class="et-fb-modal__content" style="height: 80vh; max-height: 900px;">
											<div class="et-fb-tabs">
												<header class="et-fb-tabs-outer">
													<div class="et-fb-tabs__list">
														<div class="et-fb-tabs__item et-fb-tabs__item--active" role="tab">Content</div>
														<div class="et-fb-tabs__item" role="tab">Design</div>
														<div class="et-fb-tabs__item" role="tab">Advanced</div>
													</div>
												</header>
												<div class="et-fb-tabs__panel et-fb-tabs__panel--active et-fb-tabs__panel--filter-dropdown et-fb-tabs__panel--null et-fb-tabs__panel--content" role="tabpanel">
													<div class="et-fb-options-filter et-fb-options-filter--has-dropdown">
														<div class="et-fb-options-filter-input-wrap">
															<input class="et-fb-settings-option-input et-fb-main-setting regular-text" type="text" name="filterOptions" id="et-fb-filterOptions" placeholder="Search Options" data-shortcuts-allowed="" value=""></div>
														<div class="et-fb-options-filter-button-wrap">
															<div class="et-fb-button-group et-fb-options-filter-button-group">
																<button type="button" class="et-fb-button">
																	<div class="et-fb-icon et-fb-icon--add" style="fill: rgb(92, 105, 120); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																		<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																			<g><path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path></g>
																		</svg>
																	</div>
																	<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
																</button>
																<button type="button" class="et-fb-button">Filter
																	<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
																</button>
															</div>
														</div>
													</div>
													<form class="et-fb-form">
														<input type="submit" class="et-fb-hide">
														<div class="et-fb-form__toggle et-fb-form__toggle-opened et-fb-form__toggle-enabled" data-order="1" data-name="main_content">
															<div class="et-fb-form__toggle-title">
																<h3>Page</h3>
																<div class="et-fb-icon et-fb-icon--next" style="fill: rgb(43, 135, 218); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M15.8 14L12.3 18C11.9 18.4 11.9 19 12.3 19.4 12.7 19.8 13.3 19.8 13.7 19.4L17.7 14.9C17.9 14.7 18 14.3 18 14 18.1 13.7 18 13.4 17.7 13.1L13.7 8.6C13.3 8.2 12.7 8.2 12.3 8.6 11.9 9 11.9 9.6 12.3 10L15.8 14 15.8 14Z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
															</div>
															<div class="et-fb-form__group">
																<span class="et-fb-form__label">
																	<span class="et-fb-form__label-text">Title</span>
																	<span class="et-fb-form__reset" style="margin-left: 20px; opacity: 0;">
																		<svg width="20" height="20" viewBox="-3 -3 20 20">
																			<path d="M7 1a5.29 5.29 0 0 0-3.8 1.44L1.92 1.15a.54.54 0 0 0-.92.39v3.93a.53.53 0 0 0 .53.53h3.84a.62.62 0 0 0 .44-1.07l-1.13-1.1A3.31 3.31 0 0 1 7 3a4 4 0 0 1 0 8 3.91 3.91 0 0 1-2.85-1.27 1 1 0 0 0-1.27-.21A1 1 0 0 0 2.62 11 6 6 0 1 0 7 1"></path>
																		</svg>
																	</span>
																</span>
																<div class="et-fb-settings-options et-fb-option--text">
																	<div class="et-fb-settings-option-container">
																		<input class="et-fb-settings-option-input et-fb-main-setting regular-text" type="text" name="et_pb_post_settings_title" id="et-fb-et_pb_post_settings_title" placeholder="" value="Test 3">
																	</div>
																</div>
															</div>
															<div class="et-fb-form__group">
																<span class="et-fb-form__label">
																	<span class="et-fb-form__label-text">Excerpt</span>
																</span>
																<div class="et-fb-settings-options et-fb-option--textarea">
																	<div class="et-fb-settings-option-container">
																		<textarea class="et-fb-settings-option-textarea et-fb-main-setting regular-text" name="et_pb_post_settings_excerpt" id="et-fb-et_pb_post_settings_excerpt"></textarea>
																	</div>
																</div>
															</div>
															<div class="et-fb-form__group">
																<span class="et-fb-form__label">
																	<span class="et-fb-form__label-text">Featured Image</span>
																</span>
																<div class="et-fb-settings-options et-fb-option--upload">
																	<div class="et-fb-settings-option-container">
																		<div class="et-fb-settings-option-inner et-fb-settings-option-inner-upload et-fb-settings-option-inner-upload--previewable">
																			<div class="et-fb-settings-option-preview et-fb-settings-option-upload-type-image et-fb-settings-option-preview--empty">
																				<div class="et-fb-item-addable-button">
																					<button type="button" data-tip="Add Image" class="et-fb-button et-fb-button--round et-fb-button et-fb-settings-option-add" currentitem="false">
																						<div class="et-fb-icon et-fb-icon--add" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																							<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																								<g><path d="M18 13h-3v-3a1 1 0 0 0-2 0v3h-3a1 1 0 0 0 0 2h3v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2z" fill-rule="evenodd"></path></g>
																							</svg>
																						</div>
																						<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
																					</button>
																					<label class="et-fb-form__label">Add Image</label>
																				</div>
																				<button class="et-fb-button et-fb-settings-option-remove" style="opacity: 1; top: 50%;">
																					<div class="et-fb-icon et-fb-icon--delete" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																						<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																							<g><path d="M19 9h-3V8a1 1 0 0 0-1-1h-2a1 1 0 0 0-1 1v1H9a1 1 0 1 0 0 2h10a1 1 0 0 0 .004-2H19zM9 20c.021.543.457.979 1 1h8c.55-.004.996-.45 1-1v-7H9v7zm2.02-4.985h2v4h-2v-4zm4 0h2v4h-2v-4z" fill-rule="evenodd"></path></g>
																						</svg>
																					</div>
																				</button>
																			</div>
																			<input class="et-fb-settings-option-input et-fb-settings-option-upload et-fb-main-setting regular-text" type="text" name="et_pb_post_settings_image" id="et-fb-et_pb_post_settings_image" value="">
																			<button class="et-fb-settings-option-upload-button">Upload</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="et-fb-form__toggle et-fb-form__toggle-enabled" data-order="2" data-name="background">
															<div class="et-fb-form__toggle-title">
																<h3>Background</h3>
																<div class="et-fb-icon et-fb-icon--next" style="fill: rgb(62, 80, 98); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M15.8 14L12.3 18C11.9 18.4 11.9 19 12.3 19.4 12.7 19.8 13.3 19.8 13.7 19.4L17.7 14.9C17.9 14.7 18 14.3 18 14 18.1 13.7 18 13.4 17.7 13.1L13.7 8.6C13.3 8.2 12.7 8.2 12.3 8.6 11.9 9 11.9 9.6 12.3 10L15.8 14 15.8 14Z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
															</div>
														</div>
														<div class="et-fb-form__toggle et-fb-form__toggle-enabled" data-order="3" data-name="ab_testing">
															<div class="et-fb-form__toggle-title">
																<h3>Split Testing</h3>
																<div class="et-fb-icon et-fb-icon--next" style="fill: rgb(62, 80, 98); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M15.8 14L12.3 18C11.9 18.4 11.9 19 12.3 19.4 12.7 19.8 13.3 19.8 13.7 19.4L17.7 14.9C17.9 14.7 18 14.3 18 14 18.1 13.7 18 13.4 17.7 13.1L13.7 8.6C13.3 8.2 12.7 8.2 12.3 8.6 11.9 9 11.9 9.6 12.3 10L15.8 14 15.8 14Z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
															</div>
														</div>
														<button type="button" class="et-fb-button et-fb-help-button">
															<div class="et-fb-icon et-fb-icon--help-circle" style="fill: rgb(43, 135, 218); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M14 22a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm0-3.6a.8.8 0 1 0 0-1.6.8.8 0 0 0 0 1.6zm-.8-3.2a.744.744 0 0 0 .8.8.744.744 0 0 0 .8-.8c.08-.343.305-.634.616-.8a2.976 2.976 0 0 0 1.784-2.8A2.944 2.944 0 0 0 14 8.8a3.112 3.112 0 0 0-3.2 2.4c-.096.48.264.8.8.8s.704-.32.8-.8c0-.04.216-.8 1.6-.8 1.24 0 1.6.8 1.6 1.2 0 .816-.536 1.088-1.128 1.456A2.536 2.536 0 0 0 13.2 15.2z"></path></g>
																</svg>
															</div>Help
														</button>
													</form>
												</div>
												<div class="et-fb-tabs__panel et-fb-tabs__panel--filter-dropdown et-fb-tabs__panel--null et-fb-tabs__panel--design" role="tabpanel">
													<form class="et-fb-form">
														<input type="submit" class="et-fb-hide">
														<div class="et-fb-form__toggle et-fb-form__toggle-enabled" data-order="1" data-name="color_palette">
															<div class="et-fb-form__toggle-title">
																<h3>Color Palette</h3>
																<div class="et-fb-icon et-fb-icon--next" style="fill: rgb(62, 80, 98); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M15.8 14L12.3 18C11.9 18.4 11.9 19 12.3 19.4 12.7 19.8 13.3 19.8 13.7 19.4L17.7 14.9C17.9 14.7 18 14.3 18 14 18.1 13.7 18 13.4 17.7 13.1L13.7 8.6C13.3 8.2 12.7 8.2 12.3 8.6 11.9 9 11.9 9.6 12.3 10L15.8 14 15.8 14Z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
															</div>
														</div>
														<div class="et-fb-form__toggle et-fb-form__toggle-enabled" data-order="2" data-name="spacing">
															<div class="et-fb-form__toggle-title">
																<h3>Spacing</h3>
																<div class="et-fb-icon et-fb-icon--next" style="fill: rgb(62, 80, 98); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M15.8 14L12.3 18C11.9 18.4 11.9 19 12.3 19.4 12.7 19.8 13.3 19.8 13.7 19.4L17.7 14.9C17.9 14.7 18 14.3 18 14 18.1 13.7 18 13.4 17.7 13.1L13.7 8.6C13.3 8.2 12.7 8.2 12.3 8.6 11.9 9 11.9 9.6 12.3 10L15.8 14 15.8 14Z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
															</div>
														</div>
														<div class="et-fb-form__toggle et-fb-form__toggle-enabled" data-order="3" data-name="text">
															<div class="et-fb-form__toggle-title">
																<h3>Text</h3>
																<div class="et-fb-icon et-fb-icon--next" style="fill: rgb(62, 80, 98); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M15.8 14L12.3 18C11.9 18.4 11.9 19 12.3 19.4 12.7 19.8 13.3 19.8 13.7 19.4L17.7 14.9C17.9 14.7 18 14.3 18 14 18.1 13.7 18 13.4 17.7 13.1L13.7 8.6C13.3 8.2 12.7 8.2 12.3 8.6 11.9 9 11.9 9.6 12.3 10L15.8 14 15.8 14Z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
															</div>
														</div>
														<button type="button" class="et-fb-button et-fb-help-button">
															<div class="et-fb-icon et-fb-icon--help-circle" style="fill: rgb(43, 135, 218); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M14 22a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm0-3.6a.8.8 0 1 0 0-1.6.8.8 0 0 0 0 1.6zm-.8-3.2a.744.744 0 0 0 .8.8.744.744 0 0 0 .8-.8c.08-.343.305-.634.616-.8a2.976 2.976 0 0 0 1.784-2.8A2.944 2.944 0 0 0 14 8.8a3.112 3.112 0 0 0-3.2 2.4c-.096.48.264.8.8.8s.704-.32.8-.8c0-.04.216-.8 1.6-.8 1.24 0 1.6.8 1.6 1.2 0 .816-.536 1.088-1.128 1.456A2.536 2.536 0 0 0 13.2 15.2z"></path></g>
																</svg>
															</div>Help
														</button>
													</form>
												</div>
												<div class="et-fb-tabs__panel et-fb-tabs__panel--filter-dropdown et-fb-tabs__panel--null et-fb-tabs__panel--advanced" role="tabpanel">
													<form class="et-fb-form">
														<input type="submit" class="et-fb-hide">
														<div class="et-fb-form__toggle et-fb-form__toggle-enabled" data-order="1" data-name="custom_css">
															<div class="et-fb-form__toggle-title">
																<h3>Custom CSS</h3>
																<div class="et-fb-icon et-fb-icon--next" style="fill: rgb(62, 80, 98); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M15.8 14L12.3 18C11.9 18.4 11.9 19 12.3 19.4 12.7 19.8 13.3 19.8 13.7 19.4L17.7 14.9C17.9 14.7 18 14.3 18 14 18.1 13.7 18 13.4 17.7 13.1L13.7 8.6C13.3 8.2 12.7 8.2 12.3 8.6 11.9 9 11.9 9.6 12.3 10L15.8 14 15.8 14Z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
															</div>
														</div>
														<div class="et-fb-form__toggle et-fb-form__toggle-enabled" data-order="2" data-name="performance">
															<div class="et-fb-form__toggle-title">
																<h3>Performance</h3>
																<div class="et-fb-icon et-fb-icon--next" style="fill: rgb(62, 80, 98); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																	<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																		<g><path d="M15.8 14L12.3 18C11.9 18.4 11.9 19 12.3 19.4 12.7 19.8 13.3 19.8 13.7 19.4L17.7 14.9C17.9 14.7 18 14.3 18 14 18.1 13.7 18 13.4 17.7 13.1L13.7 8.6C13.3 8.2 12.7 8.2 12.3 8.6 11.9 9 11.9 9.6 12.3 10L15.8 14 15.8 14Z" fill-rule="evenodd"></path></g>
																	</svg>
																</div>
															</div>
														</div>
														<button type="button" class="et-fb-button et-fb-help-button">
															<div class="et-fb-icon et-fb-icon--help-circle" style="fill: rgb(43, 135, 218); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
																<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
																	<g><path d="M14 22a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm0-3.6a.8.8 0 1 0 0-1.6.8.8 0 0 0 0 1.6zm-.8-3.2a.744.744 0 0 0 .8.8.744.744 0 0 0 .8-.8c.08-.343.305-.634.616-.8a2.976 2.976 0 0 0 1.784-2.8A2.944 2.944 0 0 0 14 8.8a3.112 3.112 0 0 0-3.2 2.4c-.096.48.264.8.8.8s.704-.32.8-.8c0-.04.216-.8 1.6-.8 1.24 0 1.6.8 1.6 1.2 0 .816-.536 1.088-1.128 1.456A2.536 2.536 0 0 0 13.2 15.2z"></path></g>
																</svg>
															</div>Help
														</button>
													</form>
												</div>
											</div>
										</div>
										<div class="et-fb-modal__footer">
											<div class="et-fb-button-group et-fb-button-group--block">
												<button type="button" data-tip="Discard All Changes" class="et-fb-button et-fb-button--block et-fb-button--danger" currentitem="false">
													<div class="et-fb-icon et-fb-icon--exit" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
														<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
															<g><path d="M19.71 16.857l-2.85-2.854 2.85-2.854c.39-.395.39-1.03 0-1.426l-1.43-1.427a1 1 0 0 0-1.42 0L14 11.15l-2.85-2.854a1.013 1.013 0 0 0-1.43 0L8.3 9.723a1 1 0 0 0 0 1.426l2.85 2.854-2.85 2.853a1 1 0 0 0 0 1.427l1.42 1.427a1.011 1.011 0 0 0 1.43 0L14 16.856l2.86 2.854a1 1 0 0 0 1.42 0l1.43-1.427c.39-.395.39-1.03 0-1.426z" fill-rule="evenodd"></path></g>
														</svg>
													</div>
													<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
												</button>
												<button type="button" data-tip="Undo" class="et-fb-button et-fb-button--block et-fb-button--primary et-fb-button--primary-alt" currentitem="false">
													<div class="et-fb-icon et-fb-icon--undo" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
														<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
															<g><path d="M14.355 7.253c-1.74.026-3.321.559-4.576 1.528L7.999 7A1 1 0 0 0 7 8v4.998c0 .552.447.999.999.999h4.995a1 1 0 0 0 .999-.999l-2.014-2.016a4.51 4.51 0 0 1 2.44-.733c2.235-.032 4.261 1.58 4.534 3.799.338 2.751-1.789 5.009-4.46 5.009-1.149 0-2.186-.462-2.978-1.182a.654.654 0 0 0-.902.026l-1.184 1.175a.674.674 0 0 0 .032.979A7.443 7.443 0 0 0 14.493 22c4.401 0 7.915-3.8 7.452-8.297-.395-3.826-3.745-6.507-7.59-6.45z" fill-rule="evenodd"></path></g>
														</svg>
													</div>
													<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
												</button>
												<button type="button" data-tip="Redo" class="et-fb-button et-fb-button--block et-fb-button--info" currentitem="false">
													<div class="et-fb-icon et-fb-icon--redo" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
														<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
															<g><path d="M20.986 7l-1.78 1.78c-1.255-.967-2.835-1.501-4.575-1.527-3.845-.057-7.195 2.624-7.59 6.45C6.577 18.2 10.092 22 14.493 22c1.94 0 3.701-.736 5.031-1.945a.674.674 0 0 0 .032-.979l-1.184-1.175a.655.655 0 0 0-.901-.026c-.791.72-1.83 1.182-2.978 1.182-2.671 0-4.798-2.258-4.46-5.008.273-2.22 2.299-3.831 4.534-3.8a4.51 4.51 0 0 1 2.44.734l-2.014 2.014c0 .552.447.999.999.999h4.994a.998.998 0 0 0 1-1V8a1 1 0 0 0-1-1z" fill-rule="evenodd"></path></g>
														</svg>
													</div>
													<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
												</button>
												<button type="button" data-tip="Save Changes" class="et-fb-button et-fb-button--block et-fb-button--success" currentitem="false">
													<div class="et-fb-icon et-fb-icon--check" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
														<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
															<g><path d="M19.203 9.21a.677.677 0 0 0-.98 0l-5.71 5.9-2.85-2.95a.675.675 0 0 0-.98 0l-1.48 1.523a.737.737 0 0 0 0 1.015l4.82 4.979a.677.677 0 0 0 .98 0l7.68-7.927a.737.737 0 0 0 0-1.015l-1.48-1.525z" fill-rule="evenodd"></path></g>
														</svg>
													</div>
													<canvas height="0" width="0" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
												</button>
											</div>
										</div>
										<button type="button" class="et-fb-button et-fb-button--inverse et-fb-button--round et-fb-button--small et-fb-modal__resize" style="opacity: 0; transform: translate(0%, 0%) scale(0.5);">
											<div class="et-fb-icon et-fb-icon--resize" style="fill: rgb(255, 255, 255); width: 28px; min-width: 28px; height: 28px; margin: -6px;">
												<svg viewBox="0 0 28 28" preserveAspectRatio="xMidYMid meet" shape-rendering="geometricPrecision">
													<g><path d="M11.715 12.858l-2.292-2.291a1.885 1.885 0 0 1-1.381 1.524A1.041 1.041 0 0 1 7 11.049V7.431C7 7.193 7.193 7 7.431 7h3.618c.575 0 1.041.467 1.042 1.042a1.884 1.884 0 0 1-1.523 1.38l2.292 2.291 5.728 5.728a1.886 1.886 0 0 1 1.37-1.532c.575 0 1.041.467 1.042 1.042v3.618a.431.431 0 0 1-.431.431h-3.618a1.043 1.043 0 0 1-1.042-1.042 1.887 1.887 0 0 1 1.533-1.371l-5.728-5.728z" fill-rule="evenodd"></path></g>
												</svg>
											</div>
											<canvas height="60" width="60" style="border-radius: inherit; height: 200%; left: -50%; position: absolute; top: -50%; width: 200%;"></canvas>
										</button>
									</div>
								</div>
							</div><!-- #modals -->
						</div><!-- #et-fb-app -->
					</div><!-- .et_builder_inner_content.et_pb_gutters3.et-fb-root-ancestor.et-fb-iframe-ancestor -->
				</div><!-- #et-boc -->
			</div><!-- .entry-content.et-fb-root-ancestor.et-fb-iframe-ancestor -->
		</article> <!-- .et_pb_post -->
	</div> <!-- #main-content -->

<?php

wp_footer();
