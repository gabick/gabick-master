(function ($) {
	wp.customize.bind('preview-ready', function () {

		wp.customize.preview.bind('agsdg-modal-customize-open', function (data) {
			if (true === data.expanded) {
				$(data.selector).addClass(data.class);
				if (data.selector == '#agsdg-builder-modal') {
					$('.et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar__column--left').addClass('agsdg-customize');
				}
			}
		});

		wp.customize.preview.bind('agsdg-modal-customize-close', function (data) {
			$(data.selector).removeClass(data.class);
			if (data.selector == '#agsdg-builder-modal') {
				$('.et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar__column--left').removeClass('agsdg-customize');
			}
		});

		wp.customize('agsdg_builder_options[layout_bg]', function (value) {
			value.bind(function (newBGColor) {
				$('#et_pb_layout .hndle, .et-db #et-boc #et_pb_layout .hndle .et-fb-button, .et-db #et-boc #et_pb_layout .hndle .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-page-settings-bar > .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-page-settings-bar > .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-page-settings-bar .et-fb-page-settings-bar__column--main .et-fb-button, #et_pb_layout_controls, #et_pb_layout_controls .et-pb-layout-buttons, #et_pb_layout_controls .et-pb-layout-buttons:hover, .et-db #et-boc .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options-tab-modules_all li.et_fb_fullwidth').css("cssText", "background: " + newBGColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[layout_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon svg, .et-db #et-boc .et-fb-page-settings-bar__toggle-button ~ .et-fb-button-group .et-fb-icon svg, .et-db #et-boc #et_pb_layout .et-fb-page-settings-bar .et-fb-button .et-fb-icon svg').css("fill", newColor);
				$('#et_pb_layout .hndle').css("color", newColor);
				$('.et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon__child, .et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon__child:before, .et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon__child:after').css("background", newColor);
			});
		});

		wp.customize('agsdg_builder_options[section_bg]', function (value) {
			value.bind(function (newBGColor) {
				$('.et-db #et-boc .et-fb-skeleton--section > .et-fb-skeleton__header, #et-boc #et-fb-app .et-fb-skeleton--section > .et-fb-skeleton__header .et-fb-button, #et-boc #et-fb-app .et_pb_section .et-fb-section-button-wrap--add .et-fb-button, #et-boc #et-fb-app .et-fb-component-settings--section .et-fb-button, #et-boc #et-fb-app .et-fb-component-settings--section .et-fb-button:hover, #et-boc #et-fb-app .et-fb-component-settings--section .et-fb-button-group, .et-db #et-boc .et-fb-outline--section, .et-pb-controls, .et-db #et-boc .et_pb_section>.et-pb-draggable-spacing__sizing, .et-db #et-boc .et_pb_section>.et-pb-draggable-spacing__spacing, .et-db #et-boc .et_pb_section>.et-pb-draggable-spacing__spacing .et-pb-draggable-spacing__hint, .et-db #et-boc .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options-tab-modules_all li, .et-db #et-boc .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options-tab-modules_all li:after, .et-db #et-boc .et-core-control-toggle--on, .et-db #et-boc .et-fb-settings-custom-select-wrapper.et-fb-settings-option-select-active .select-option-item-hovered, .et_pb_yes_no_button.et_pb_on_state').css("cssText", "background: " + newBGColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[section_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-section-button-wrap--add .et-fb-icon svg, .et-db #et-boc .et-fb-skeleton--section > .et-fb-skeleton__header .et-fb-icon svg, .et-db #et-boc .et-fb-component-settings--section .et-fb-button-group .et-fb-icon svg').css("fill", newColor);
				$('.et-db #et-boc .et-fb-skeleton--section > .et-fb-skeleton__header').css("color", newColor);
			});
		});
		wp.customize('agsdg_builder_options[row_bg]', function (value) {
			value.bind(function (newBGColor) {
				$('.et-db #et-boc .et-fb-skeleton--row > .et-fb-skeleton__header, .et-db #et-boc .et-fb-outline--row, .et-db #et-boc #et-fb-app .et-fb-row-button-wrap--add .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-component-settings--row .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-component-settings--row .et-fb-button-group, .et_pb_row .et-pb-controls, .et-db #et-boc .et_pb_row>.et-pb-draggable-spacing__sizing, .et-db #et-boc .et_pb_row>.et-pb-draggable-spacing__spacing, .et-db #et-boc .et_pb_row>.et-pb-draggable-spacing__spacing .et-pb-draggable-spacing__hint, .et-db #et-boc .et-fb-columns-layout li .column-block-wrap .column-block, .et_pb_layout_column, .et-pb-column-layouts li:hover .et_pb_layout_column').css("cssText", "background: " + newBGColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[row_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-skeleton--row > .et-fb-skeleton__header .et-fb-icon svg, .et-db #et-boc .et-fb-row-button-wrap--add .et-fb-icon svg, .et-db #et-boc #et-fb-app .et-fb-component-settings--row > .et-fb-button-group .et-fb-icon svg').css("fill", newColor);
				$('.et-db #et-boc .et-fb-skeleton--row > .et-fb-skeleton__header').css("color", newColor);
			});
		});
		wp.customize('agsdg_builder_options[module_bg]', function (value) {
			value.bind(function (newBGColor) {
				$('.et-db #et-boc .et-fb-skeleton--module > .et-fb-skeleton__header, .et-db #et-boc #et-fb-app .et-fb-component-settings--module .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-component-settings--module .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-module-button-wrap--add .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-skeleton--row .et_pb_column_empty .et-fb-button, .et-db #et-boc #et-fb-app .et_pb_row > .et_pb_column_empty .et-fb-button, .et_pb_module_block, .et-db #et-boc .et-pb-draggable-spacing__sizing').css("background", newBGColor);
			});
		});
		wp.customize('agsdg_builder_options[module_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-skeleton--module > .et-fb-skeleton__header .et-fb-icon svg, .et-db #et-boc .et_pb_module .et-fb-icon svg, .et-db #et-boc .et_pb_row > .et_pb_column_empty .et-fb-icon svg, .et-db #et-boc .et-fb-skeleton--row .et_pb_column_empty .et-fb-icon svg').css("fill", newColor);
				$('.et-db #et-boc .et-fb-skeleton--module > .et-fb-skeleton__header').css("color", newColor);
			});
		});
		wp.customize('agsdg_builder_options[builder_settings_bg]', function (value) {
			value.bind(function (newBGColor) {
				$('.et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header, .et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header .et-fb-button, .et-db #et-boc .et-fb-button-group--responsive-mode, .et-db #et-boc .et-fb-button-group--builder-mode, .et-db #et-boc #et-fb-app .et-fb-button-group--responsive-mode .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-button-group--builder-mode .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-button-group--responsive-mode .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-button-group--builder-mode .et-fb-button:hover').css("cssText", "background: " + newBGColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[builder_settings_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header .et-fb-button svg, .et-db #et-boc .et-fb-button-group--responsive-mode .et-fb-icon svg, .et-db #et-boc .et-fb-button-group--builder-mode .et-fb-icon svg, .et-db #et-boc .et-fb-button-group--responsive-mode .et-fb-button--app-modal path').css("fill", newColor);
				$('.et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header').css("color", newColor);
			});
		});
		wp.customize('agsdg_builder_options[builder_settings_active_icon]', function (value) {
			value.bind(function (newColor) {
				$('#et-boc #et-fb-app .et-fb-page-settings-bar__column--left .et-fb-icon--desktop svg').css("fill", newColor);
				/* may re-add: #et-boc #et-fb-app .et-fb-page-settings-bar__column--left .et-fb-icon--hover svg */
			});
		});
		wp.customize('agsdg_builder_options[settings_header_bg]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-modal .et-fb-modal__header, .et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__header .et-fb-button, .et-core-modal-header, .et-pb-settings-heading, .et_pb_prompt_modal h3').css("background", newColor);
			});
		});
		wp.customize('agsdg_builder_options[settings_header_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-modal .et-fb-modal__header').css("color", newColor);
				$('.et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__header .et-fb-button svg').css("fill", newColor);
			});
		});
		wp.customize('agsdg_builder_options[settings_tab_bg]', function (value) {
			value.bind(function (newColor) {
                var colorText = $('.et-db #et-boc .et-fb-modal .et-fb-tabs__list').css("color");
                $('.et-db #et-boc .et-fb-modal .et-fb-tabs__list, .et-db #et-boc .et-fb-modal .et-fb-tabs__list:hover, .et-db #et-boc .et-l .et-fb-tabs__item:hover, .et-core-tabs, .et-core-tabs.ui-widget-header').css("cssText", "color: " + colorText + " !important; background: " + newColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[settings_tab_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-modal .et-fb-modal__content .et-fb-tabs__item').css("color", newColor);
			});
		});
		wp.customize('agsdg_builder_options[settings_active_tab_bg]', function (value) {
			value.bind(function (newColor) {
                var colorText = $('.et-db #et-boc .et-fb-modal .et-fb-modal__content .et-fb-tabs__item').css("color");
//				$('.et-db #et-boc .et-fb-modal .et-fb-tabs__item--active, .et-db #et-boc .et-fb-modal .et-fb-tabs__item--active:hover, .et-db #et-boc .et-l .et-fb-tabs__item:hover, .et-core-tabs li.ui-state-active a, .et-core-tabs a:hover, .et-pb-options-tabs-links li.et-pb-options-tabs-links-active a, .et-pb-options-tabs-links li a:hover, .et-db #et-boc .et-fb-modal .et-fb-tabs__item--active:hover').css("background", newColor);
				$('.et-db #et-boc .et-fb-modal .et-fb-tabs__item--active, .et-db #et-boc .et-fb-modal .et-fb-tabs__item--active:hover, .et-db #et-boc .et-l .et-fb-tabs__item--active:hover, .et-core-tabs li.ui-state-active a, .et-core-tabs a:hover, .et-pb-options-tabs-links li.et-pb-options-tabs-links-active a, .et-pb-options-tabs-links li a:hover, .et-db #et-boc .et-fb-modal .et-fb-tabs__item--active:hover').css("cssText", "color: " + colorText + " !important; background: " + newColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[settings_active_tab_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-modal .et-fb-tabs__item--active').css("color", newColor);
			});
		});
		/*    */
		wp.customize('agsdg_builder_options[settings_discard_button_bg]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--danger').css("background", newColor);
			});
		});
		wp.customize('agsdg_builder_options[settings_undo_button_bg]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--primary-alt, a.et-pb-modal-save-template, a.et-pb-modal-save-template:hover').css("cssText", "background: " + newColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[settings_redo_button_bg]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--info, a.et-pb-modal-preview-template, a.et-pb-modal-preview-template:hover').css("cssText", "background: " + newColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[settings_save_button_bg]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--block.et-fb-button--success, .et-db #et-boc .et_fb_save_module_modal a.et-fb-save-library-button, a.et-pb-modal-save, a.et-pb-modal-save:hover, .et-core-modal-action, .et_pb_prompt_modal .et_pb_prompt_buttons input.et_pb_prompt_proceed, .et-db #et-boc #et-fb-app .et-fb-button--block.et-fb-button--success').css("cssText", "background: " + newColor + " !important;");
			});
		});
		/*    */
		wp.customize('agsdg_builder_options[settings_bottom_buttons_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-modal .et-fb-modal__footer .et-fb-icon svg').css("fill", newColor);
			});
		});

		/*    */
		wp.customize('agsdg_builder_options[insert_header_bg]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-heading, .et-db #et-boc #et-fb-app .et-fb-modal-add-module-container .et-fb-settings-heading .et-fb-button, .et-db #et-boc .et-fb-modal-add-module-container.et-fb-modal-settings--inversed:after').css("cssText", "background: " + newColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[insert_header_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-heading').css("color", newColor);
				$('.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-heading .et-fb-icon svg').css("fill", newColor);
			});
		});
		wp.customize('agsdg_builder_options[insert_tab_bg]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav').css("background", newColor);
			});
		});
		wp.customize('agsdg_builder_options[insert_tab_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav-item a').css("color", newColor);
			});
		});
		wp.customize('agsdg_builder_options[insert_active_tab_bg]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav-item--active a, .et-db #et-boc .et-fb-settings-tabs-nav-item:hover a').css("background", newColor);
			});
		});
		wp.customize('agsdg_builder_options[insert_active_tab_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav .et-fb-settings-tabs-nav-item--active a').css("color", newColor);
			});
		});
		/*  */
		wp.customize('agsdg_builder_options[warning_header_bg]', function (value) {
			value.bind(function (newColor) {
				$('.et-builder-exit-modal .et-core-modal-header').css("background", newColor);
			});
		});
		wp.customize('agsdg_builder_options[warning_header_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-builder-exit-modal .et-core-modal-title, .et-builder-exit-modal .et-core-modal-close, .et-builder-exit-modal .et-core-modal-close:hover, .et-builder-exit-modal .et-core-modal-close:before').css("color", newColor);
			});
		});
		wp.customize('agsdg_builder_options[warning_discard_button_bg]', function (value) {
			value.bind(function (newColor) {
				$('.et-builder-exit-modal .et_pb_prompt_buttons .et-core-modal-action-secondary').css("background", newColor);
			});
		});
		wp.customize('agsdg_builder_options[warning_save_button_bg]', function (value) {
			value.bind(function (newColor) {
				$('.et-builder-exit-modal .et_pb_prompt_buttons .et-core-modal-action:not(.et-core-modal-action-secondary)').css("background", newColor);
			});
		});
		wp.customize('agsdg_builder_options[warning_bottom_buttons_font]', function (value) {
			value.bind(function (newColor) {
				$('.et-builder-exit-modal .et_pb_prompt_buttons .et-core-modal-action').css("cssText", "color: " + newColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[save_draft_button_bg]', function (value) {
			value.bind(function (newColor) {
                var colorText = $('.et-db #et-boc .et-fb-button-group .et-fb-button--save-draft').css("color");
//				$('.et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--info.et-fb-button--save-draft, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--save-draft, .et-db #et-boc .et-fb-button-group .et-fb-button--save-draft').css("background", newColor);
				$('.et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--info.et-fb-button--save-draft, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--save-draft, .et-db #et-boc .et-fb-button-group .et-fb-button--save-draft').css("cssText", "color: " + colorText + " !important; background: " + newColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[save_draft_button_font]', function (value) {
			value.bind(function (newColor) {
                var colorBG = $('.et-db #et-boc .et-fb-button-group .et-fb-button--save-draft').css("background-color");
//				$('.et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--info.et-fb-button--save-draft, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--save-draft, .et-db #et-boc .et-fb-button-group .et-fb-button--save-draft').css("cssText", "color: " + newColor + " !important;");
                var colorBG = $('.et-db #et-boc .et-fb-button-group .et-fb-button--save-draft').css("background-color");
				$('.et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--info.et-fb-button--save-draft, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--save-draft, .et-db #et-boc .et-fb-button-group .et-fb-button--save-draft').css("cssText", "color: " + newColor + " !important; background: " + colorBG + " !important;");

			});
		});
		wp.customize('agsdg_builder_options[publish_button_bg]', function (value) {
			value.bind(function (newColor) {
                var colorText = $('.et-db #et-boc .et-fb-button-group .et-fb-button--publish').css("color");
//				$('.et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish').css("background", newColor);
				$('.et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish').css("cssText", "color: " + colorText + " !important; background: " + newColor + " !important;");
			});
		});
		wp.customize('agsdg_builder_options[publish_button_font]', function (value) {
			value.bind(function (newColor) {
                var colorBG = $('.et-db #et-boc .et-fb-button-group .et-fb-button--publish').css("background-color");
//				$('.et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish').css("cssText", "color: " + newColor + " !important;");
				$('.et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish').css("cssText", "color: " + newColor + " !important; background: " + colorBG + " !important;");
			});
		});
	});
})(jQuery);
