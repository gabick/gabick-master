var $j = jQuery.noConflict();

$j(document).ready(function() {
		mobileMenuSlide();

	function mobileMenuSlide() {
		$j( "<div class='sub-menu-toggle'></div>" ).insertBefore( "#main-header #mobile_menu.et_mobile_menu .menu-item-has-children > a" );
		$j( "#main-header #mobile_menu.et_mobile_menu .sub-menu-toggle" ).click(function () {
			$(this).toggleClass("popped");
		});
	}

	$j(window).load(function() {
		setup_collapsible_submenus();
	});
});
