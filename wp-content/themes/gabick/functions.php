<?php
require_once( 'code/services-shortcode.php' );
require_once( 'code/added-social-icons.php' );
require_once( 'includes/admin-login/admin-login.php' );
function my_enqueue_assets() {
    wp_enqueue_style( 'parent-main', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'gabick-main', '/pub/build/styles/main.css' );
    wp_enqueue_style( 'gabick-copyright', '/pub/build/styles/copyright.css' );
    wp_enqueue_style( 'gabick-mobile', '/pub/build/styles/better-mobile-menu.css' );
//    wp_enqueue_style( 'mobile-slide-menu', get_stylesheet_directory_uri().'/includes/mobile-menu-slide./css/mobile-menu-slide.css' );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue_assets' );

function my_scripts_method()
{
    wp_enqueue_script(
        'nwayo-dependencies',
        '/pub/build/scripts/dependencies-head-sync.js',
        array('jquery'),
        true
    );
    wp_enqueue_script(
        'nwayo-dependencies-sync',
        '/pub/build/scripts/dependencies.js',
        array('jquery'),
        true
    );
    wp_enqueue_script(
        'nwayo-main',
        '/pub/build/scripts/main.js',
        array('jquery'),
        true
    );
//    wp_enqueue_script(
//        'mobile-slide-menu',
//        get_stylesheet_directory_uri().'/includes/mobile-menu-slide./js/mobile-menu-slide.js',
//        array('jquery'),
//        true
//    );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_method', 15 );

?>
