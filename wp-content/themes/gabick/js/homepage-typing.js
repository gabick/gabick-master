var $j = jQuery.noConflict();
const URL = location.host;
const CATEGORY_SLUG = 'category'
$j(document).ready(function(){
	homePageTyping();
	
	function homePageTyping() {
		new Typed('h1 span.dwd-typing',{
			strings:["de Marques","de Site Web‎s","de Publicitées","de Boutiques"],
			startDelay: 1750,
			typeSpeed: 80,
			backDelay: 500,
			backSpeed: 40,
			showCursor: true,
			loop: false,
			onComplete: function(self) {
				subtitleTyping();
			}
		});
	}
	
	function subtitleTyping() {
		new Typed('h1',{
			strings:["L'avenir est web", "L'avenir est web"],
			typeSpeed: 80,
			ackDelay: 500,
			backSpeed: 40,
			showCursor: false,
			onComplete: function(self) {
				new Typed('p.easy',{
					strings:[" C'est aussi simple que ça !", " C'est aussi simple que ça !"],
					startDelay: 100,
					typeSpeed: 45,
					showCursor: false,
					loop: false,
				});
			}
		});
		
	}
})
